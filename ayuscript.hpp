#define Common_h__
#define AYUL_MAJOR() 0
#define AYUL_MINOR() 1
static unsigned AYUL_REVISION() {
	const char* revision = strchr("$Rev\t$", '\t');
	return revision ? atoi(revision) : 1;
}
static const char* AYUL_VERSION() {
	static char buffer[20];
	if(!buffer[0])
		sprintf(buffer, "%i.%i.%i", AYUL_MAJOR, AYUL_MINOR, AYUL_REVISION());
	return buffer;
}
using std::string;
using std::list;
using std::vector;
using std::set;
using boost::intrusive_ptr;
class BaseObject {
protected:
	virtual ~BaseObject() {
	}
};
template<typename Type>
class BaseRefObject : public BaseObject {
	int Count;
	bool Scoped;
public:
	virtual ~BaseRefObject() {
		assert(Count == 0);
	}
	void scoped() {
		Scoped = true;
	}
protected:
	BaseRefObject() {
		Count = 0;
		Scoped = false;
	}
	BaseRefObject(const BaseRefObject& object) {
		Count = 0;
		Scoped = false;
	}
	friend void intrusive_ptr_add_ref(BaseRefObject<Type>* object) {
		++object->Count;
	}
	friend void intrusive_ptr_release(BaseRefObject<Type>* object) {
		if(--object->Count <= 0 && !object->Scoped)
			delete object;
	}
};
class RefString : public BaseRefObject<RefString>, public std::string {
public:
	RefString() {
	}
	RefString(const std::string& str) : string(str) {
	}
	RefString(const char* str) : string(str) {
	}
};
template<typename Type>
bool eraseFromVector(const vector<Type>& array, const Type& value) {
	vector<Type>::iterator itor = std::find(array.begin(), array.end(), value);
	if(itor != array.end()) {
		array.erase(itor);
		return true;
	}
	return false;
}
template<typename Type>
bool erasePtrFromVector(vector<intrusive_ptr<Type> >& array, Type* value) {
	vector<intrusive_ptr<Type> >::iterator itor = std::find(array.begin(), array.end(), value);
	if(itor != array.end()) {
		array.erase(itor);
		return true;
	}
	return false;
}
template<typename Type1, typename Type2>
void operator += (vector<Type1>& right, const vector<Type2>& left) {
	right.insert(right.end(), left.begin(), left.end());
}
enum SyntaxAssignemnt {
	saNormal,
	saModulus,
	saBXor,
	saBOr,
	saBAnd,
	saMult,
	saDiv,
	saAdd,
	saSub
};
enum SyntaxVisiblity {
	svNone,
	svPublic,
	svPrivate,
	svProtected
};
enum SyntaxOperator {
	soModulus,
	soBXor,
	soBOr,
	soBAnd,
	soMult,
	soDiv,
	soAdd,
	soSub
};
enum SyntaxCompare {
	scEqual,
	scNotEqual,
	scLessEqual,
	scLess,
	scGreaterEqual,
	scGreater,
	scAnd,
	scOr
};
template<typename T>
class singleton_ptr {
	mutable T* Data;
private:
	void check() const {
		if(Data)
			return;
		Data = (T*)malloc(sizeof(T));
		memset(Data, 0, sizeof(T));
		new(Data) T();
		Data->scoped();
	}
	singleton_ptr(const singleton_ptr&);
	singleton_ptr& operator = (const singleton_ptr&);
public:
	singleton_ptr() {
		Data = NULL;
	}
	~singleton_ptr() {
		release();
	}
	T* get() { 
		check();
		return Data; 
	}
	T* get() const { 
		check();
		return Data; 
	}
	void release() {
		if(!Data)
			return;
		T* ptr = Data;
		Data = NULL;
		delete Data;
	}
	operator T* () { 
		return get(); 
	}
	operator T* () const { 
		return get();
	}
};
static string join(char delim, const char* v, ...) {
	const int MaxLength = 4000;
	static char buffer[MaxLength];
	va_list list;
	va_start(list, v);
	char delims[2] = {delim, 0};
	buffer[0] = 0;
restart:
	while(v != NULL) {
		switch(v[0]) {
			case 0:
				break;
			case 1:
				strcat(buffer, v+1);
				break;
			case 2:
				{
					const char* vv = v;
					v = va_arg(list, const char*);
					if(v == NULL)
						goto restart;
					if(v[0]) {
						if(buffer[0] != 0)
							strcat(buffer, delims);
						strcat(buffer, vv+1);
						goto restart;
					}
				}
				break;
			default:
				if(buffer[0] != 0)
					strcat(buffer, delims);
				strcat(buffer, v);
				break;
		}
		v = va_arg(list, const char*);
	}
	return buffer;
}
static string trim(const string& v) {
	unsigned right = v.size();
	unsigned left = 0;
	for(; left < v.size() && isalpha(v[left]); ++left);
	for(; right > left && isalpha(v[right-1]); --right);
	return v.substr(left, right-left);
}
#define Script_h__
class ScriptModuleObject;
class ScriptClass;
class ScriptInterface;
class ScriptValueType;
class ScriptModule;
class ScriptCode;
#define ScriptObject_h__
class ScriptObject : public BaseRefObject<ScriptObject> {
};
class ScriptNamedObject : public ScriptObject {
public:
	static bool GenerateCppString;
protected:
	ScriptModuleObject* Parent;
	unsigned Index;
	ScriptNamedObject();
public:
	~ScriptNamedObject();
	virtual const string& name() const = 0;
	unsigned index() const { return Index; }
	virtual string toString() const { return name(); }
	virtual string toFullString(unsigned level = 0) const;
	virtual SyntaxVisiblity visibility() const { return svNone; }
	virtual bool equals(const ScriptNamedObject& otherObject) const { return name() == otherObject.name(); }
	friend class ScriptModuleObject;
};
// ScriptObject_h__
#define ScriptType_h__
enum ScriptTypeType {
	sttVoid,
	sttObject,
	sttDelegate,
	sttEvent
};
template<typename Type>
class ScriptTypeT {
};
template<typename Type> static ScriptTypeT<Type&> makeScriptTypeFromPtr(Type*) {
	return ScriptTypeT<Type&>();
}
struct ScriptType {
	ScriptTypeType Type;
	intrusive_ptr<ScriptModuleObject> Object;
	int Array;
	bool IsRef;
	bool IsConst;
	string toString(bool cppString = false) const;
	ScriptType();
	ScriptType(ScriptTypeType type, bool isRef = false, bool isConst = false);
	ScriptType(const intrusive_ptr<ScriptModuleObject>& object_, ScriptTypeType type = sttObject, bool isRef = false, bool isConst = false);
	ScriptType(ScriptModuleObject* object_, ScriptTypeType type = sttObject, bool isRef = false, bool isConst = false);
	bool isValid() const;
private:
	template<typename Type> static ScriptType fromScriptType3T(const ScriptTypeT<const Type>&) {
		ScriptType type = deduceSimpleType<Type>();
		type.IsConst = true;
		return type;
	}
	template<typename Type> static ScriptType fromScriptType3T(const ScriptTypeT<Type>&) {
		ScriptType type = deduceSimpleType<Type>();
		return type;
	}
	template<typename Type> static ScriptType fromScriptType2T(const ScriptTypeT<Type>&) {
		ScriptType type = fromScriptType3T(ScriptTypeT<Type>());
		return type;
	}
	template<typename Type> static ScriptType fromScriptType2T(const ScriptTypeT<Type&>&) {
		ScriptType type = fromScriptType3T(ScriptTypeT<Type>());
		type.IsRef = true;
		return type;
	}
	template<typename Type> static ScriptType fromScriptType2T(const ScriptTypeT<Type*>&) {
		ScriptType type = fromScriptType3T(ScriptTypeT<Type>());
		type.IsRef = true;
		return type;
	}
public:
	template<typename Type> static ScriptType fromPtrT(Type*) {
		return fromScriptTypeT(ScriptTypeT<Type>());
	}
	template<typename Type> static ScriptType fromScriptTypeT(const ScriptTypeT<Type>& x) {
		return fromScriptType2T(x);
	}
private:
	template<typename Type>	static ScriptType deduceSimpleType();
	template<> static ScriptType deduceSimpleType<void>();
	template<> static ScriptType deduceSimpleType<int>();
	template<> static ScriptType deduceSimpleType<float>();
	template<> static ScriptType deduceSimpleType<bool>();
	template<> static ScriptType deduceSimpleType<string>();
	friend bool operator == (const ScriptType& left, const ScriptType& right) {
		return memcmp(&left, &right, sizeof(ScriptType)) == 0;
	}
	friend bool operator != (const ScriptType& left, const ScriptType& right) {
		return memcmp(&left, &right, sizeof(ScriptType)) != 0;
	}
};
// ScriptType_h__
#define ScriptValue_h__
struct ScriptValue {
	enum EnumType {
		svtNone,
		svtString,
		svtInt,
		svtFloat,
		svtBoolean,
		svtNewObject,
		svtNull
	};
	EnumType Type;
	string StringValue;
	int IntValue;
	float FloatValue;
	bool BooleanValue;
	intrusive_ptr<ScriptClass> ObjectType;
	ScriptValue();
	string toString() const;
};
// ScriptValue_h__
#define ScriptVisibility_h__
struct ScriptVisibility {
	bool IsStatic;
	SyntaxVisiblity Visiblity;
	ScriptVisibility();
	string toString(bool cppString = false, bool valueType = false) const;
};
// ScriptVisibility_h__
#define ScriptVariable_h__
struct ScriptVariable {
	ScriptType Type;
	string Name;
	ScriptValue Value;
	ScriptVariable();
	ScriptVariable(const ScriptType& type, const string& name = string(), const ScriptValue& value = ScriptValue());
	string toString(bool cppString = false) const;
};
struct ScriptLocalVariable : ScriptVariable {
	int NextVariable;
	unsigned Offset;
	unsigned Level; // 0 - arguments
	ScriptLocalVariable() : NextVariable(-1), Level(0), Offset(0) {}
	ScriptLocalVariable(const ScriptVariable& variable) : ScriptVariable(variable), NextVariable(-1), Level(0), Offset(0) {}
};
// ScriptVariable_h__
#define ScriptFunction_h__
enum ScriptFunctionType {
	sftNone,
	sftImport,
	sftNative,
	sftScript
};
struct ScriptFunction {
	ScriptFunctionType Type;
	void* Native;
	intrusive_ptr<ScriptCode> Script;
	ScriptFunction();
	ScriptFunction(const intrusive_ptr<ScriptCode>& script);
	bool defined() const;
	string toString(bool cppString) const;
};
// ScriptFunction_h__
#define ScriptModuleObject_h__
class ScriptModuleObject : public ScriptNamedObject {
protected:
	typedef vector<intrusive_ptr<ScriptNamedObject> > ObjectPtrList;
	string Name;
	ObjectPtrList Objects;
	unsigned ObjectIndex;
public:
	const string& name() const {
		return Name;
	}
	unsigned objectCount() const {
		return Objects.size();
	}
	const ObjectPtrList::value_type& object(unsigned i) const {
		return Objects[i];
	}
	ObjectPtrList::value_type& object(unsigned i) {
		return Objects[i];
	}
	virtual string toFullString(unsigned level = 0) const;
	virtual bool isValueType() const {
		return false;
	}
	ScriptModuleObject(const string& objectName);
	void addObject(const intrusive_ptr<ScriptNamedObject>& object);
	template<typename Type>
	void addClass() {
		addObject((ScriptModuleObject*)Type::class$);
	}
	virtual intrusive_ptr<ScriptNamedObject> findObject(const string& objectName, bool up = false);
	template<typename Type>
	intrusive_ptr<Type> findObject(const string& objectName, bool up = false, bool throw_ = true) {
		intrusive_ptr<ScriptNamedObject> object = findObject(objectName, up);
		if(object) {
			intrusive_ptr<Type> objectType = boost::dynamic_pointer_cast<Type>(findObject(objectName, up));
			if(objectType)
				return objectType;
			if(throw_)
				throw std::runtime_error("object of specified type not found");
		}
		return NULL;
	}
	friend class ScriptNamedObject;
};
// ScriptModuleObject_h__
#define ScriptClassVariable_h__
class ScriptClassVariable : public ScriptNamedObject, public ScriptVariable, public ScriptVisibility {
public:
	unsigned Offset;
public:
	ScriptClassVariable(const string& methodName);
	~ScriptClassVariable();
	string toString() const;
	const string& name() const {
		return Name;
	}
	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}
	friend class ScriptClass;
};
// ScriptClassVariable_h__
#define ScriptClassMethod_h__
class ScriptClassMethod : public ScriptNamedObject, public ScriptVisibility {
	ScriptType ReturnType;
	vector<ScriptVariable> Arguments;
	ScriptFunction Function;
	string Name;
public:
	ScriptClassMethod(const string& methodName);
	~ScriptClassMethod();
	string toString() const;
	const string& name() const;
	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}
	bool equals(const ScriptNamedObject& otherObject) const;
	template<typename RetType>
	void setMethod(RetType (*function)()) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Function.Type = sftNative;
		Function.Native = (void*)function;
	}
	template<typename RetType, typename Arg1>
	void setMethod(RetType (*function)(Arg1)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Function.Type = sftNative;
		Function.Native = (void*)function;
	}
	template<typename RetType, typename Arg1, typename Arg2>
	void setMethod(RetType (*function)(Arg1, Arg2)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Function.Type = sftNative;
		Function.Native = (void*)function;
	}
	template<typename RetType, typename Arg1, typename Arg2, typename Arg3>
	void setMethod(RetType (*function)(Arg1, Arg2, Arg3)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Function.Type = sftNative;
		Function.Native = (void*)function;
	}
	template<typename RetType, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
	void setMethod(RetType (*function)(Arg1, Arg2, Arg3, Arg4)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg4>()));
		Function.Type = sftNative;
		Function.Native = (void*)function;
	}
	template<typename RetType, typename ThisClass>
	void setMethod(RetType (ThisClass::*function)()) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass>
	void setMethod(RetType (ThisClass::*function)() const) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1>
	void setMethod(RetType (ThisClass::*function)(Arg1)) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1>
	void setMethod(RetType (ThisClass::*function)(Arg1) const) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2>
	void setMethod(RetType (ThisClass::*function)(Arg1, Arg2)) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2>
	void setMethod(RetType (ThisClass::*function)(Arg1, Arg2) const) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3>
	void setMethod(RetType (ThisClass::*function)(Arg1, Arg2, Arg3)) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3>
	void setMethod(RetType (ThisClass::*function)(Arg1, Arg2, Arg3) const) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
	void setMethod(RetType (ThisClass::*function)(Arg1, Arg2, Arg3, Arg4)) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg4>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
	void setMethod(RetType (ThisClass::*function)(Arg1, Arg2, Arg3, Arg4) const) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg4>()));
		Function.Type = sftNative;
		Function.Native = *(void**)&function;
	}
	friend class ScriptClass;
	friend struct SyntaxClassMethod;
};
// ScriptClassMethod_h__
#define ScriptClassProperty_h__
class ScriptClassProperty : public ScriptNamedObject, public ScriptVariable, public ScriptVisibility {
	ScriptFunction GetFunction, SetFunction;
public:
	ScriptClassProperty(const string& propertyName);
	~ScriptClassProperty();
	string toString() const;
	const string& name() const {
		return Name;
	}
	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}
	template<typename RetType, typename ThisClass>
	void setFunction(RetType (*function)()) {
		IsStatic = true;
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		GetFunction.Type = sftNative;
		GetFunction.Native = (void*)function;
	}
	template<typename RetType>
	void setSetFunction(void (*function)(RetType)) {
		IsStatic = true;
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		SetFunction.Type = sftNative;
		SetFunction.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass>
	void setFunction(RetType (ThisClass::*function)()) {
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		GetFunction.Type = sftNative;
		GetFunction.Native = *(void**)&function;
	}
	template<typename RetType, typename ThisClass>
	void setSetFunction(void (ThisClass::*function)(RetType)) {
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		SetFunction.Type = sftNative;
		SetFunction.Native = *(void**)&function;
	}
	friend class ScriptClass;
	friend struct SyntaxClassProperty;
};
// ScriptClassProperty_h__
#define ScriptClass_h__
class ScriptClass : public ScriptModuleObject {
	intrusive_ptr<ScriptClass> Extends;
	vector<intrusive_ptr<ScriptInterface> > Implements;
public:
	ScriptClass(const string& className = string());
	~ScriptClass();
	string toString() const;
	intrusive_ptr<ScriptNamedObject> findObject(const string& objectName, bool up = false);
	friend class ScriptModule;
	friend class ScriptClassVariable;
	friend class ScriptClassMethod;
	friend class ScriptClassProperty;
	friend struct SyntaxClass;
};
class BaseNativeScriptClass : public ScriptClass {
public:
	BaseNativeScriptClass(const string& className);
};
template<typename Type>
class NativeScriptClass : public BaseNativeScriptClass {
public:
	static intrusive_ptr<ScriptClass> Class;
	typedef Type ThisClass;
	NativeScriptClass();
};
// ScriptClassMethod_h__
// ScriptClass_h__
#define ScriptValueType_h__
class ScriptValueType : public ScriptModuleObject {
public:
	ScriptValueType(const string& className = string());
	~ScriptValueType();
	string toString() const;
	bool isValueType() const;
	friend class ScriptModule;
	friend struct SyntaxClass;
};
class BaseNativeScriptValueType : public ScriptValueType {
public:
	BaseNativeScriptValueType(const string& className) : ScriptValueType(className) {}
};
template<typename Type>
class NativeScriptValueType : public BaseNativeScriptValueType {
public:
	static intrusive_ptr<ScriptValueType> Class;
	typedef Type ThisClass;
	NativeScriptValueType();
};
// ScriptValueType_h__
#define ScriptInterface_h__
class ScriptInterface : public ScriptModuleObject {
	vector<intrusive_ptr<ScriptInterface> > Implements;
public:
	ScriptInterface(const string& interfaceName = string());
	~ScriptInterface();
	string toString() const;
	intrusive_ptr<ScriptNamedObject> findObject(const string& objectName, bool up = false);
	friend class ScriptModule;
	friend struct SyntaxInterface;
};
// ScriptInterface_h__
#define ScriptModule_h__
class ScriptModule : public ScriptModuleObject {
public:
	string Name;
	ScriptModule(const string& name);
	string toString() const;
	friend class ScriptClass;
};
// ScriptModule_h__
//=====================================================================
#define DECLARE_CLASS(ClassName) \
	public: static singleton_ptr<NativeScriptClass<ClassName> > class$;
#define DECLARE_VALUE_TYPE(ClassName) \
	static singleton_ptr<NativeScriptValueType<ClassName> > class$;
#define BEGIN_CLASS(ClassName) \
	singleton_ptr<NativeScriptClass<ClassName> > ClassName::class$; \
	template<> NativeScriptClass<ClassName>::NativeScriptClass() : BaseNativeScriptClass(#ClassName)
#define BEGIN_VALUE_TYPE(ClassName) \
	singleton_ptr<NativeScriptValueType<ClassName> > ClassName::class$; \
	template<> NativeScriptValueType<ClassName>::NativeScriptValueType() : BaseNativeScriptValueType(#ClassName)
#define PTR_OF(Class, Name) (((Class*)0)->Name)
#define OFFSET_OF(Class, Name) ((unsigned)&PTR_OF(Class, Name))
#define CLASS_VARIABLE(VarName) \
	{ \
		intrusive_ptr<ScriptClassVariable> variable(new ScriptClassVariable(#VarName)); \
		variable->Offset = OFFSET_OF(ThisClass, VarName); \
		variable->Type = ScriptType::fromPtrT(&PTR_OF(ThisClass, VarName)); \
		addObject(variable); \
	}
#define CLASS_METHOD(MethodName) \
	{ \
		intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(#MethodName)); \
		method->setMethod(&ThisClass::MethodName); \
		addObject(method); \
	}
#define CLASS_CONVERSION(To) \
	{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod("cast#" #To)); \
		struct F { static To X(ThisClass x) { return (To)x; } }; \
		method->setMethod(F::X); \
		addObject(method); \
	}
#define CLASS_OPERATOR1(Op, RetType, Left) \
	{ \
		intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod("operator " #Op)); \
		struct F { static RetType X(Left x) {	return Op x; } }; \
		method->setMethod(F::X); \
		addObject(method); \
	}
#define CLASS_OPERATOR2(Op, RetType, Left, Right) \
	{ \
		intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod("operator " #Op)); \
		struct F { static RetType X(Left x, Right y) { return x Op y; } }; \
		method->setMethod(F::X); \
		addObject(method); \
	}
#define CLASS_PROPERTY(PropertyName, GetProperty, SetProperty) \
	{ \
		intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
		property_->setFunction(&ThisClass::GetProperty); \
		property_->setSetFunction(&ThisClass::SetProperty); \
		addObject(property_); \
	}
#define CLASS_GET_PROPERTY(PropertyName, GetProperty) \
	{ \
		intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
		property_->setFunction(&ThisClass::GetProperty); \
		addObject(property_); \
	}
#define CLASS_SET_PROPERTY(PropertyName, SetProperty) \
	{ \
		intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
		property_->setSetFunction(&ThisClass::SetProperty); \
		addObject(property_); \
	}
#define TYPE_OF(ClassName) (ScriptType::DeduceType<ClassName>().Class)
// Script_h__
#define OpCode_h__
#define OP_CODE0(index) (index)
#define OP_CODE1(index,arg1) ((index)|(0x1<<8)|((arg1&0x1)<<10))
#define OP_CODE2(index,arg1,arg2) ((index)|(0x2<<8)|((arg1&0x1)<<10)|((arg2&0x1)<<10))
#define OP_CODE3(index,arg1,arg2,arg3) ((index)|(0x3<<8)|((arg1&0x1)<<10)|((arg2&0x1)<<10)|((arg3&0x1)<<10))
#define OP_BEGIN_ARGS(count)		((count&0x3)<<6)
#define OP_NUM_ARGS(code)				(((code)>>6)&0x3)
struct OpCode {
	enum OpType {
		Arg0 = (( 0 &0x3)<<6) ,
		Nop,
		Arg1 = (( 1 &0x3)<<6) ,
		Jmp, // O
	
		Arg2 = (( 2 &0x3)<<6) ,
		Call, // FL B
		Push, // TL B
		Pop, // TL B
		JCmp, // C O
		Init, // FL VL
		Arg3 = (( 3 &0x3)<<6) ,
		New, // TL FL B
	};
	OpType Type;
	unsigned Args[3];
	OpCode(OpType type, unsigned arg0 = 0, unsigned arg1 = 0, unsigned arg2 = 0) : Type(type) {
		Args[0] = arg0;
		Args[1] = arg1;
		Args[2] = arg2;
	}
};
class ScriptCode : public ScriptObject {
	vector<ScriptLocalVariable> VariableList;
	vector<ScriptFunction*> FunctionList;
	vector<ScriptType> TypeList;
	vector<OpCode> OpList;
	friend struct SyntaxClassMethod;
	friend struct SyntaxClassProperty;
};
// OpCode_h__
#define Primitive_h__
struct Int32 {
	static singleton_ptr<NativeScriptValueType< Int32 > > class$; ;
	int Value;
	Int32() : Value(0) {
	}
	Int32(int value) : Value(value) {
	}
	string toString() const {
		char buffer[12];
		return _itoa(Value, buffer, 10);
	}
	operator int () const { 
		return Value; 
	}
	operator string () const {
		return toString();
	}
	friend Int32 operator + (Int32 a, Int32 b) {
		return a.Value + b.Value;	
	}
};
struct Float32 {
	static singleton_ptr<NativeScriptValueType< Float32 > > class$; ;
	float Value;
	Float32() : Value(0) {
	}
	Float32(float value) : Value(value) {
	}
	string toString() const {
		char buffer[30];
		sprintf(buffer, "%f", Value);
		return buffer;
	}
	operator float () const { 
		return Value; 
	}
	operator string () const {
		return toString();
	}
	friend Float32 operator + (Float32 a, Float32 b) {
		return a.Value + b.Value;	
	}
};
struct Boolean {
	static singleton_ptr<NativeScriptValueType< Boolean > > class$; ;
	bool Value;
	Boolean() : Value(0) {
	}
	Boolean(bool value) : Value(value) {
	}
	string toString() const {
		return Value ? "true" : "false";
	}
	operator string () const {
		return toString();
	}
	operator bool () const { 
		return Value; 
	}
};
struct String {
	static singleton_ptr<NativeScriptValueType< String > > class$; ;
	string Value;
	String() : Value(0) {
	}
	String(const string& value) : Value(value) {
	}
	String(const char* value) : Value(value) {
	}
	string toString() const {
		return Value;
	}
	operator string () const {
		return Value;
	}
	operator Int32 () const {
		return atoi(Value.c_str());
	}
	operator Float32 () const {
		return (float)atof(Value.c_str());
	}
};
// Primitive_h__
// Script_h__
// Common_h__
