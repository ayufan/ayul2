#ifndef ScriptBase_h__
#define ScriptBase_h__


template<typename Type>
bool eraseFromVector(const vector<Type>& array, const Type& value) {
	vector<Type>::iterator itor = std::find(array.begin(), array.end(), value);
	if(itor != array.end()) {
		array.erase(itor);
		return true;
	}
	return false;
}

template<typename Type>
bool erasePtrFromVector(vector<intrusive_ptr<Type> >& array, Type* value) {
	vector<intrusive_ptr<Type> >::iterator itor = std::find(array.begin(), array.end(), value);
	if(itor != array.end()) {
		array.erase(itor);
		return true;
	}
	return false;
}

template<typename Type1, typename Type2>
void operator += (vector<Type1>& right, const vector<Type2>& left) {
	right.insert(right.end(), left.begin(), left.end());
}

enum SyntaxAssignemnt {
	saModulus,
	saBXor,
	saBOr,
	saBAnd,
	saMult,
	saDiv,
	saAdd,
	saSub,

	saNormal
};

enum SyntaxVisiblity {
	svNone,
	svPublic,
	svPrivate,
	svProtected
};

enum SyntaxOperator {
	soModulus,
	soBXor,
	soBOr,
	soBAnd,
	soMult,
	soDiv,
	soAdd,
	soSub,
	scEqual,
	scNotEqual,
	scLessEqual,
	scLess,
	scGreaterEqual,
	scGreater
};

static string join(char delim, const char* v, ...) {
	const int MaxLength = 4000;
	static char buffer[MaxLength];
	va_list list;
	va_start(list, v);
	char delims[2] = {delim, 0};

	buffer[0] = 0;

restart:
	while(v != NULL) {
		switch(v[0]) {
			case 0:
				break;

			case 1:
				strcat(buffer, v+1);
				break;

			case 2:
				{
					const char* vv = v;
					v = va_arg(list, const char*);
					if(v == NULL)
						goto restart;
					if(v[0]) {
						if(buffer[0] != 0)
							strcat(buffer, delims);
						strcat(buffer, vv+1);
						goto restart;
					}
				}
				break;

			default:
				if(buffer[0] != 0)
					strcat(buffer, delims);
				strcat(buffer, v);
				break;
		}
		v = va_arg(list, const char*);
	}

	return buffer;
}

static string trim(const string& v) {
	unsigned right = v.size();
	unsigned left = 0;

	for(; left < v.size() && isalpha(v[left]); ++left);
	for(; right > left && isalpha(v[right-1]); --right);

	return v.substr(left, right-left);
}

#endif // ScriptBase_h__