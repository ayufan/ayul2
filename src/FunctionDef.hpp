#ifndef FunctionDef_h__
#define FunctionDef_h__

class ScriptClass;
class ScriptModule;
class ScriptValueType;

//=====================================================================

#define OPERATOR_CAST "cast "
#define OPERATOR1_PREFIX "operator "
#define OPERATOR2_PREFIX "operator "
#define CALLER_NAME "caller "
#define CTOR_NAME "constructor "

#define DECLARE_CLASS(ClassName) \
	public: static ScriptClass* id$(); virtual ScriptObject* id();

#define DECLARE_VALUE_TYPE(ClassName) \
	typedef ClassName ThisClass; \
	static ScriptValueType* id$(); ScriptObject* id();

#define DECLARE_MODULE(ModuleName) \
	struct ModuleName { static ScriptModule* id$(); }

#define BEGIN_MODULE(ModuleName) \
	ScriptModule* ModuleName::id$() { \
	static NativeScriptModule<ModuleName> id$; \
	return &id$; } \
	template<> NativeScriptModule<ModuleName>::NativeScriptModule() : BaseNativeScriptModule(#ModuleName)

#define BEGIN_CLASS(ClassName, BaseClass) \
	ScriptObject* ClassName::id() { return id$(); } \
	ScriptClass* ClassName::id$() { static NativeScriptClass<ClassName> id$; return &id$; } \
	template<> NativeScriptClass<ClassName>::NativeScriptClass() : BaseNativeScriptClass(#ClassName, BaseClass::id$(), sizeof(ClassName))

#define BEGIN_STATIC_CLASS(ClassName) \
	ScriptObject* ClassName::id() { return id$(); } \
	ScriptClass* ClassName::id$() { static NativeScriptClass<ClassName> id$; return &id$; } \
	template<> NativeScriptClass<ClassName>::NativeScriptClass() : BaseNativeScriptClass(#ClassName, NULL, sizeof(ClassName))

#define BEGIN_VALUE_TYPE(ClassName) \
	ScriptObject* ClassName::id() { return id$(); } \
	ScriptValueType* ClassName::id$() { static NativeScriptValueType<ClassName> id$; return &id$; } \
	template<> NativeScriptValueType<ClassName>::NativeScriptValueType() : BaseNativeScriptValueType(#ClassName, sizeof(ClassName))

#define BEGIN_PRIMITIVE_TYPE(ClassName) \
	ScriptObject* ClassName::id() { return id$(); } \
	ScriptValueType* ClassName::id$() { \
	static NativeScriptPrimitiveType<ClassName> id$; \
	return &id$; } \
	template<> NativeScriptPrimitiveType<ClassName>::NativeScriptPrimitiveType() : BaseNativeScriptValueType(#ClassName, sizeof(ClassName))

#define PTR_OF(Class, Name) (((Class*)0)->Name)
#define OFFSET_OF(Class, Name) ((unsigned)&PTR_OF(Class, Name))

#define CLASS_VARIABLE(VarName) \
{ \
	intrusive_ptr<ScriptClassVariable> variable(new ScriptClassVariable(#VarName)); \
	variable->Offset = OFFSET_OF(ThisClass, VarName); \
	variable->Type = ScriptType::fromPtrT(&PTR_OF(ThisClass, VarName)); \
	addObject(variable); \
}

#define CLASS_METHOD(MethodName) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(#MethodName)); \
	method->setMethod(&ThisClass::MethodName); \
	addObject(method); \
}

#define CLASS_VIRTUAL_METHOD(MethodName) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(#MethodName)); \
	method->setMethod(&ThisClass::MethodName, true); \
	addObject(method); \
}

#define MODULE_FUNCTION(FunctionName, FunctionAddr) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(#FunctionName)); \
	method->setMethod(&FunctionAddr); \
	addObject(method); \
}

#define CLASS_CONSTRUCTOR(...) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(CTOR_NAME)); \
	method->setMethod(&FunctionType<void (__VA_ARGS__)>::createClass<ThisClass>); \
	addObject(method); \
}

#define CLASS_CUSTOM_FACTORY(X) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(CTOR_NAME)); \
	method->setMethod(&ThisClass::X); \
	addObject(method); \
}

#define CLASS_CAST(To) \
{ \
	intrusive_ptr<ScriptClassCast> cast(new ScriptClassCast()); \
struct F { static To __cdecl X(ThisClass x) { return (To)x; } }; \
	cast->setCast(F::X); \
	addObject(cast); \
}

#define CLASS_OPERATOR1(Op, ReturnType, Left) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(OPERATOR1_PREFIX #Op)); \
struct F { static ReturnType __cdecl X(Left x) {	return Op x; } }; \
	method->setMethod(F::X); \
	addObject(method); \
}

#define CLASS_OPERATOR2(Op, ReturnType, Left, Right) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(OPERATOR2_PREFIX #Op)); \
struct F { static ReturnType __cdecl X(Left x, Right y) { return x Op y; } }; \
	method->setMethod(F::X); \
	addObject(method); \
}

#define CLASS_ASSIGNMENT(Op, Type) \
{ \
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(OPERATOR2_PREFIX #Op)); \
struct F { static ThisClass& __cdecl X(ThisClass& x, Type y) { x Op y; return x; } }; \
	method->setMethod(F::X); \
	addObject(method); \
}

#define CLASS_PROPERTY(PropertyName, GetProperty, SetProperty) \
{ \
	intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
	property_->setFunction(&ThisClass::GetProperty); \
	property_->setSetFunction(&ThisClass::SetProperty); \
	addObject(property_); \
}

#define CLASS_GET_PROPERTY(PropertyName, GetProperty) \
{ \
	intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
	property_->setFunction(&ThisClass::GetProperty); \
	addObject(property_); \
}

#define CLASS_SET_PROPERTY(PropertyName, SetProperty) \
{ \
	intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
	property_->setSetFunction(&ThisClass::SetProperty); \
	addObject(property_); \
}

#define MODULE_PROPERTY(PropertyName, GetProperty, SetProperty) \
{ \
	intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
	property_->setFunction(&GetProperty); \
	property_->setSetFunction(&SetProperty); \
	addObject(property_); \
}

#define MODULE_GET_PROPERTY(PropertyName, GetProperty) \
{ \
	intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
	property_->setFunction(&GetProperty); \
	addObject(property_); \
}

#define MODULE_SET_PROPERTY(PropertyName, SetProperty) \
{ \
	intrusive_ptr<ScriptClassProperty> property_(new ScriptClassProperty(#PropertyName)); \
	property_->setSetFunction(&SetProperty); \
	addObject(property_); \
}

#define CLASS_OBJECT(ClassName) \
{ \
	addObject<ClassName>(); \
}

#define TYPE_OF(ClassName) (ScriptType::DeduceType<ClassName>().Class)

#endif // FunctionDef_h__