#ifndef FunctionType_h__
#define FunctionType_h__

template <typename Signature>
struct FunctionType;

template<typename RetType>
struct FunctionType<RetType ()> { 
	typedef RetType (__cdecl *Type)();

	template<typename ThisClass>
	void __thiscall createClass() {
		new(this) ThisClass();
	}

	FunctionType(ScriptFunction* function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}
	FunctionType(const intrusive_ptr<ScriptFunction> &function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}

	Type Function;
	RetType operator () () {
		return Function();
	}
};

template<typename RetType, typename Arg0>
struct FunctionType<RetType (Arg0)> {
	typedef RetType (__cdecl *Type)(Arg0);

	template<typename ThisClass>
	void __thiscall createClass(Arg0 arg0) {
		new(this) ThisClass(arg0);
	}

	FunctionType(ScriptFunction* function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}
	FunctionType(const intrusive_ptr<ScriptFunction> &function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}

	Type Function;
	RetType operator () (Arg0 arg0) {
		return Function(arg0);
	}
};

template<typename RetType, typename Arg0, typename Arg1>
struct FunctionType<RetType (Arg0, Arg1)> {
	typedef RetType (__cdecl *Type)(Arg0, Arg1);

	template<typename ThisClass>
	void __thiscall createClass(Arg0 arg0, Arg1 arg1) {
		new(this) ThisClass(arg0, arg1);
	}

	FunctionType(ScriptFunction* function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}
	FunctionType(const intrusive_ptr<ScriptFunction> &function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}

	Type Function;
	RetType operator () (Arg0 arg0, Arg1 arg1) {
		return Function(arg0, arg1);
	}
};

template<typename RetType, typename Arg0, typename Arg1, typename Arg2>
struct FunctionType<RetType (Arg0, Arg1, Arg2)> {
	typedef RetType (__cdecl *Type)(Arg0, Arg1, Arg2);

	template<typename ThisClass>
	void __thiscall createClass(Arg0 arg0, Arg1 arg1, Arg2 arg2) {
		new(this) ThisClass(arg0, arg1, arg2);
	}

	FunctionType(ScriptFunction* function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}
	FunctionType(const intrusive_ptr<ScriptFunction> &function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}

	Type Function;
	RetType operator () (Arg0 arg0, Arg1 arg1, Arg2 arg2) {
		return Function(arg0, arg1, arg2);
	}
};

template<typename RetType, typename Arg0, typename Arg1, typename Arg2, typename Arg3>
struct FunctionType<RetType (Arg0, Arg1, Arg2, Arg3)> {
	typedef RetType (__cdecl *Type)(Arg0, Arg1, Arg2, Arg3);

	template<typename ThisClass>
	void __thiscall createClass(Arg0 arg0, Arg1 arg1, Arg2 arg2, Arg3 arg3) {
		new(this) ThisClass(arg0, arg1, arg2, arg3);
	}

	FunctionType(ScriptFunction* function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}
	FunctionType(const intrusive_ptr<ScriptFunction> &function) {
		if(!function)
			throw std::invalid_argument("function");
		Function = (Type)function->nativePtr();
	}

	Type Function;
	RetType operator () (Arg0 arg0, Arg1 arg1, Arg2 arg2, Arg3 arg3) {
		return Function(arg0, arg1, arg2, arg3);
	}
};

#endif // FunctionType_h__