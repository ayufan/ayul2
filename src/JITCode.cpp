#include "StdAfx.hpp"

static bool isbyte(long value)
{
	return char(value) == value;
}

const char *RegisterNames[] = {"ax", "cx", "dx", "bx", "ESP", "EBP", "si", "di", "ptr"};

MemType::MemType()
{
	Reg1 = NONE;
	Reg2 = NONE;
	Scale = SCALE1;
	Disp = 0;
}

MemType::MemType( RegisterType reg, long disp /*= 0*/ )
{
	Reg1 = reg;
	Reg2 = NONE;
	Scale = SCALE1;
	Disp = disp;
}

MemType::MemType( RegisterType reg, ScaleType scale, long disp /*= 0*/ )
{
	Reg1 = NONE;
	Reg2 = reg;
	Scale = scale;
	Disp = disp;
}

MemType::MemType( RegisterType reg1, RegisterType reg2, ScaleType scale /*= SCALE1*/, long disp /*= 0*/ )
{
	Reg1 = reg1;
	Reg2 = reg2;
	Scale = scale;
	Disp = disp;
}

MemType::MemType( void *ptr )
{
	Reg1 = PTR;
	Reg2 = NONE;
	Scale = SCALE1;
	Ptr = ptr;
}

void JITCode::clear()
{
	Code.clear();
	Code.reserve(2048);
}

//////////////////////////////////////////////////////////////////////
// Emit Offset
//////////////////////////////////////////////////////////////////////

void JITCode::emitByte(long value)
{
	Code.push_back(value);
}

void JITCode::emitShort(long value)
{
	emitByte(value & 0xFF);
	emitByte((value>>8) & 0xFF);
}
void JITCode::emitLong(long value)
{
	emitByte(value & 0xFF);
	emitByte((value>>8) & 0xFF);
	emitByte((value>>16) & 0xFF);
	emitByte((value>>24) & 0xFF);
}
void JITCode::emitLong(void* value$)
{
	long& value = (long&)value$;
	emitByte(value & 0xFF);
	emitByte((value>>8) & 0xFF);
	emitByte((value>>16) & 0xFF);
	emitByte((value>>24) & 0xFF);
}
void JITCode::emitChar(int value)
{
	emitByte(value);
}

void JITCode::emitMem(MemType mem, byte sub)
{
	if(mem.Reg2 == ESP || mem.Reg2 == PTR)
		return;
	if(mem.Reg1 == NONE && mem.Reg2 == NONE)
		return;

	sub &= 0x7;

	if(mem.Reg2 == NONE)
	{
		if(mem.Reg1 == PTR)
		{
			emitByte(EBP | sub<<3);
			emitLong(mem.Disp);
			return;
		}

		int disp = (mem.Disp == 0 && mem.Reg1 != EBP ? 0x0 : char(mem.Disp) == mem.Disp ? 0x40 : 0x80);

		emitByte(disp | mem.Reg1 | sub<<3);

		if(mem.Reg1 == ESP)
		{
			emitByte(0x24);
		}
		if(disp == 0x40)
		{
			emitByte(mem.Disp);
		}
		else if(disp == 0x80)
		{
			emitLong(mem.Disp);
		}
		return;
	}

	if(mem.Reg1 == PTR)
	{
		return;
	}

	if(mem.Reg1 == NONE)
	{
		emitByte(0x4 | sub << 3);
		emitByte(mem.Scale | mem.Reg2 << 3 | 0x5);
		emitLong(mem.Disp);
		return;
	}

	int disp = (mem.Disp == 0 ? 0x0 : char(mem.Disp) == mem.Disp ? 0x40 : 0x80);

	emitByte(disp | 0x4 | sub<<3);
	emitByte(mem.Scale | (mem.Reg2<<3) | mem.Reg1);

	if(disp == 0x40)
	{
		emitByte(mem.Disp);
	}
	else if(disp == 0x80)
	{
		emitLong(mem.Disp);
	}
}
void JITCode::emitOffset(byte r1, byte r2)
{
	emitByte(0xC0 | r1 | r2<<3);
}

//////////////////////////////////////////////////////////////////////
// Nop
//////////////////////////////////////////////////////////////////////

void JITCode::nop()
{
	emitByte(0x90);
}

//////////////////////////////////////////////////////////////////////
// Push
//////////////////////////////////////////////////////////////////////

void JITCode::push(RegisterType reg)
{
	emitByte(0x50 | long(reg));
}

void JITCode::push(MemType mem)
{
	emitByte(0xFF);
	emitMem(mem, 6);
}

// push value
void JITCode::push(long value)
{
	if(isbyte(value))
	{
		emitByte(0x6A);
		emitByte(value);
	}
	else
	{
		emitByte(0x68);
		emitLong(value);
	}
}

void JITCode::push(LabelType label)
{
	emitByte(0x68);
	emitLabel(label);
}

void JITCode::push( void* value )
{
	emitByte(0x68);
	emitLong(value);
}

//////////////////////////////////////////////////////////////////////
// Pop
//////////////////////////////////////////////////////////////////////

void JITCode::pop(RegisterType reg)
{
	emitByte(0x58 | long(reg));
}

void JITCode::pop(MemType mem)
{
	emitByte(0x8F);
	emitMem(mem, 0);
}

//////////////////////////////////////////////////////////////////////
// Mov
//////////////////////////////////////////////////////////////////////

void JITCode::mov(RegisterType src, RegisterType dest)
{
	emitByte(0x8B);
	emitOffset(src, dest);
}

void JITCode::mov(MemType src, RegisterType dest)
{
	emitByte(0x8B);
	emitMem(src, dest);
}

void JITCode::mov(RegisterType src, MemType dest)
{
	emitByte(0x89);
	emitMem(dest, src);
}

void JITCode::mov(RegisterType reg, long value)
{
	emitByte(0xB8 | long(reg));
	emitLong(value);
}

void JITCode::mov(MemType mem, long value)
{
	emitByte(0xC7);
	emitMem(mem, 0);
	emitLong(value);
}

void JITCode::mov( RegisterType reg, void* value )
{
	emitByte(0xB8 | long(reg));
	emitLong(value);
}

void JITCode::mov( MemType mem, void* value )
{
	emitByte(0xC7);
	emitMem(mem, 0);
	emitLong(value);
}
//////////////////////////////////////////////////////////////////////
// Cmp
//////////////////////////////////////////////////////////////////////

void JITCode::cmp(RegisterType r1, RegisterType r2)
{
	emitByte(0x3B);
	emitOffset(r1, r2);
}

void JITCode::cmp(MemType m1, RegisterType r2)
{
	emitByte(0x39);
	emitMem(m1, r2);
}

void JITCode::cmp(RegisterType r1, MemType m2)
{
	emitByte(0x3B);
	emitMem(m2, r1);
}

void JITCode::cmp(RegisterType reg, long value)
{
	if(isbyte(value))
	{
		emitByte(0x83);
		emitByte(0xF8 | long(reg));
		emitByte(value);
	}
	else
	{
		emitByte(0x81);
		emitByte(0xF8 | long(reg));
		emitLong(value);
	}
}

void JITCode::cmp(MemType mem, long value)
{
	if(isbyte(value))
	{
		emitByte(0x83);
		emitMem(mem, 7);
		emitByte(value);
	}
	else
	{
		emitByte(0x81);
		emitMem(mem, 7);
		emitLong(value);
	}
}

//////////////////////////////////////////////////////////////////////
// Jmp
//////////////////////////////////////////////////////////////////////

void JITCode::jmp(RegisterType reg)
{
	emitByte(0xFF);
	emitByte(0xE0 | long(reg));
}

void JITCode::jmp(long rel, JmpType j)
{
	if(char(rel) == rel)
	{
		static byte codes[] = {0xEB, 0x74, 0x75, 0x7F, 0x7D, 0x7C, 0x7E};

		emitByte(codes[j]);
		emitByte(rel);
	}
	else
	{
		static byte codes[] = {0x0, 0x84, 0x85, 0x8F, 0x8D, 0x8C, 0x8E};

		if(j == JMP)
		{
			emitByte(0xE9);
		}
		else
		{
			emitByte(0x0F);
			emitByte(codes[j]);
		}
		emitLong(rel);
	}
}

//////////////////////////////////////////////////////////////////////
// Call
//////////////////////////////////////////////////////////////////////

void JITCode::call(RegisterType reg)
{
	emitByte(0xFF);
	emitByte(0xD0 | long(reg));
}

void JITCode::call(MemType mem)
{
	emitByte(0xFF);
	emitMem(mem, 2);
}

void JITCode::call(long rel)
{
	emitByte(0xE8);
	emitLong(rel);
}

void JITCode::call( LabelType label )
{
	emitByte(0xE8);
	emitLabel(label);
}

//////////////////////////////////////////////////////////////////////
// Math
//////////////////////////////////////////////////////////////////////

void JITCode::math(MathType math, RegisterType src, RegisterType dest)
{
	static byte codes[] = {0x03, 0x2B, 0x23, 0x0B, 0x33};

	emitByte(codes[math]);
	emitOffset(src, dest);
}

void JITCode::math(MathType math, MemType src, RegisterType dest)
{
	static byte codes[] = {0x03, 0x2B, 0x23, 0x0B, 0x33};

	emitByte(codes[math]);
	emitMem(src, dest);
}

void JITCode::math(MathType math, RegisterType src, MemType dest)
{
	static byte codes[] = {0x01, 0x29, 0x21, 0x09, 0x31};

	emitByte(codes[math]);
	emitMem(dest, src);
}

void JITCode::math(MathType math, RegisterType reg, long value)
{
	static byte codes[] = {0xC0, 0xE8, 0xE0, 0xC8, 0xF0};

	if(isbyte(value))
	{
		emitByte(0x83);
		emitByte(codes[math] | long(reg));
		emitByte(value);
	}
	else
	{
		emitByte(0x81);
		emitByte(codes[math] | long(reg));
		emitLong(value);
	}
}

void JITCode::math(MathType math, MemType mem, long value)
{
	static byte codes[] = {0x0, 0x5, 0x4, 0x1, 0x6};

	if(isbyte(value))
	{
		emitByte(0x83);
		emitMem(mem, codes[math]);
		emitByte(value);
	}
	else
	{
		emitByte(0x81);
		emitMem(mem, codes[math]);
		emitLong(value);
	}
}

//////////////////////////////////////////////////////////////////////
// Inc
//////////////////////////////////////////////////////////////////////

void JITCode::inc(RegisterType reg)
{
	emitByte(0x40 | long(reg));
}

void JITCode::inc(MemType mem)
{
	emitByte(0xFF);
	emitMem(mem, 0);
}

//////////////////////////////////////////////////////////////////////
// Dec
//////////////////////////////////////////////////////////////////////

void JITCode::dec(RegisterType reg)
{
	emitByte(0x48 | long(reg));
}

void JITCode::dec(MemType mem)
{
	emitByte(0xFF);
	emitMem(mem, 1);
}

//////////////////////////////////////////////////////////////////////
// Mul
//////////////////////////////////////////////////////////////////////

void JITCode::mul(RegisterType src, RegisterType dest)
{
	emitByte(0x0F);
	emitByte(0xAF);
	emitOffset(src, dest);
}

void JITCode::mul(MemType src, RegisterType dest)
{
	emitByte(0x0F);
	emitByte(0xAF);
	emitMem(src, dest);
}

void JITCode::mul(RegisterType src, long value)
{
	if(isbyte(value))
	{
		emitByte(0x6B);
		emitOffset(src, src);
		emitByte(value);
	}
	else
	{
		emitByte(0x69);
		emitOffset(src, src);
		emitLong(value);
	}
}

void JITCode::mul(RegisterType src, long value, RegisterType dest)
{
	if(isbyte(value))
	{
		emitByte(0x6B);
		emitOffset(src, dest);
		emitByte(value);
	}
	else
	{
		emitByte(0x69);
		emitOffset(src, dest);
		emitLong(value);
	}
}

void JITCode::mul(MemType src, long value, RegisterType dest)
{
	if(isbyte(value))
	{
		emitByte(0x6B);
		emitMem(src, dest);
		emitByte(value);
	}
	else
	{
		emitByte(0x69);
		emitMem(src, dest);
		emitLong(value);
	}
}

//////////////////////////////////////////////////////////////////////
// Div - edx:eax/reg => eax
//////////////////////////////////////////////////////////////////////

void JITCode::div(RegisterType reg)
{
	emitByte(0xF7);
	emitOffset(reg, 7);
}

void JITCode::div(MemType mem)
{
	emitByte(0xF7);
	emitMem(mem, 7);
}

//////////////////////////////////////////////////////////////////////
// Not - inverts the bits 
//////////////////////////////////////////////////////////////////////

void JITCode::not(RegisterType dest)
{
	emitByte(0xF7);
	emitOffset(dest, 2);
}

void JITCode::not(MemType dest)
{
	emitByte(0xF7);
	emitMem(dest, 2);
}

//////////////////////////////////////////////////////////////////////
// Neg -
//////////////////////////////////////////////////////////////////////

void JITCode::neg(RegisterType dest)
{
	emitByte(0xF7);
	emitOffset(dest, 3);
}

void JITCode::neg(MemType dest)
{
	emitByte(0xF7);
	emitMem(dest, 3);
}

//////////////////////////////////////////////////////////////////////
// Shl
//////////////////////////////////////////////////////////////////////

void JITCode::shl(RegisterType dest, byte count)
{
	emitByte(0xC1);
	emitOffset(dest, 4);
	emitByte(count);
}

void JITCode::shl(MemType dest, byte count)
{
	emitByte(0xC1);
	emitMem(dest, 4);
	emitByte(count);
}

void JITCode::shl(RegisterType dest)
{
	emitByte(0xD3);
	emitOffset(dest, 4);
}

void JITCode::shl(MemType mem)
{
	emitByte(0xD3);
	emitMem(mem, 4);
}

//////////////////////////////////////////////////////////////////////
// Shr
//////////////////////////////////////////////////////////////////////

void JITCode::shr(RegisterType dest, byte count)
{
	emitByte(0xC1);
	emitOffset(dest, 5);
	emitByte(count);
}

void JITCode::shr(MemType dest, byte count)
{
	emitByte(0xC1);
	emitMem(dest, 5);
	emitByte(count);
}

void JITCode::shr(RegisterType dest)
{
	emitByte(0xD3);
	emitOffset(dest, 5);
}

void JITCode::shr(MemType dest)
{
	emitByte(0xD3);
	emitMem(dest, 5);
}

//////////////////////////////////////////////////////////////////////
// Cdq
//////////////////////////////////////////////////////////////////////

void JITCode::cdq()
{
	emitByte(0x99);
}

//////////////////////////////////////////////////////////////////////
// CLD/STD
//////////////////////////////////////////////////////////////////////

void JITCode::cld()
{
	emitByte(0xFC);
}

void JITCode::std()
{
	emitByte(0xFD);
}

//////////////////////////////////////////////////////////////////////
// Rep
//////////////////////////////////////////////////////////////////////

void JITCode::rep_stos()
{
	emitByte(0xF3);
	emitByte(0xAB);
}

void JITCode::rep_movs()
{
	emitByte(0xF3);
	emitByte(0xA5);
}

//////////////////////////////////////////////////////////////////////
// Lea
//////////////////////////////////////////////////////////////////////

void JITCode::lea(MemType src, RegisterType dest)
{
	emitByte(0x8D);
	emitMem(src, dest);
}

//////////////////////////////////////////////////////////////////////
// OR
//////////////////////////////////////////////////////////////////////

void JITCode::or(RegisterType src, RegisterType dest)
{
	emitByte(0x0B);
	emitOffset(src, dest);
}

void JITCode::or(MemType src, RegisterType dest)
{
	emitByte(0x0B);
	emitMem(src, dest);
}

void JITCode::or(RegisterType src, MemType dest)
{
	emitByte(0x09);
	emitMem(dest, src);
}

//////////////////////////////////////////////////////////////////////
// XOR
//////////////////////////////////////////////////////////////////////

void JITCode::xor(RegisterType src, RegisterType dest)
{
	emitByte(0x33);
	emitOffset(src, dest);
}

void JITCode::xor(MemType src, RegisterType dest)
{
	emitByte(0x33);
	emitMem(src, dest);
}

void JITCode::xor(RegisterType src, MemType dest)
{
	emitByte(0x31);
	emitMem(dest, src);
}

//////////////////////////////////////////////////////////////////////
// AND
//////////////////////////////////////////////////////////////////////

void JITCode::and(RegisterType src, RegisterType dest)
{
	emitByte(0x23);
	emitOffset(src, dest);
}

void JITCode::and(MemType src, RegisterType dest)
{
	emitByte(0x23);
	emitMem(src, dest);
}

void JITCode::and(RegisterType src, MemType dest)
{
	emitByte(0x21);
	emitMem(dest, src);
}

//////////////////////////////////////////////////////////////////////
// Ret
//////////////////////////////////////////////////////////////////////

// ret
void JITCode::ret()
{
	emitByte(0xC3);
}

void JITCode::ret(long size)
{
	if(!size)
	{
		ret();
		return;
	}
	emitByte(0xC2);
	emitShort(size);
}

//////////////////////////////////////////////////////////////////////
// FLD
//////////////////////////////////////////////////////////////////////

void JITCode::fld(FpuType fpu)
{
	emitByte(0xD9);
	emitOffset(fpu, 0);
}

void JITCode::fld(MemType mem)
{
	emitByte(0xD9);
	emitMem(mem, 0);
}

//////////////////////////////////////////////////////////////////////
// FILD
//////////////////////////////////////////////////////////////////////

void JITCode::fild(MemType mem)
{
	emitByte(0xDB);
	emitMem(mem, 0);
}

//////////////////////////////////////////////////////////////////////
// FST
//////////////////////////////////////////////////////////////////////

void JITCode::fst(FpuType fpu)
{
	emitByte(0xDD);
	emitOffset(fpu, 2);
}

void JITCode::fst(MemType mem)
{
	emitByte(0xD9);
	emitMem(mem, 2);
}

//////////////////////////////////////////////////////////////////////
// FSTP
//////////////////////////////////////////////////////////////////////

void JITCode::fstp(FpuType fpu)
{
	emitByte(0xDD);
	emitOffset(fpu, 3);
}

void JITCode::fstp(MemType mem)
{
	emitByte(0xD9);
	emitMem(mem, 3);
}

//////////////////////////////////////////////////////////////////////
// FCOMI
//////////////////////////////////////////////////////////////////////

void JITCode::fcomi(FpuType with)
{
	emitByte(0xDB);
	emitByte(0xF0 | with);
}

//////////////////////////////////////////////////////////////////////
// FIST
//////////////////////////////////////////////////////////////////////

void JITCode::fist(MemType mem)
{
	emitByte(0xDB);
	emitMem(mem, 2);
}

//////////////////////////////////////////////////////////////////////
// FISTP
//////////////////////////////////////////////////////////////////////

void JITCode::fistp(MemType mem)
{
	emitByte(0xDB);
	emitMem(mem, 3);
}

//////////////////////////////////////////////////////////////////////
// FLD*
//////////////////////////////////////////////////////////////////////

void JITCode::fld1()
{
	emitByte(0xD9);
	emitByte(0xE8);
}

void JITCode::fldz()
{
	emitByte(0xD9);
	emitByte(0xEE);
}

void JITCode::fldpi()
{
	emitByte(0xD9);
	emitByte(0xEB);
}

//////////////////////////////////////////////////////////////////////
// fmath
//////////////////////////////////////////////////////////////////////

void JITCode::math(FpuMathType math, FpuType dest)
{
	static byte codes[] = {0xC0, 0xE8, 0xC8, 0xF8};

	emitByte(0xDE);
	emitByte(codes[math] | long(dest));
}

void JITCode::math(FpuMathType math, MemType mem)
{
	static byte codes[] = {0x0, 0x4, 0x1, 0x6};

	emitByte(0xD8);
	emitMem(mem, codes[math]);
}

void JITCode::math(FpuMathExtType math)
{
	static byte codes[] = {0xE1, 0xE0, 0xFA, 0xFE, 0xFF, 0xFB};

	emitByte(0xD9);
	emitByte(codes[math]);
}

JITFunction::JITFunction(unsigned size) : Code(NULL), Size(0)
{
	Size = (size + 4095) & 4096;
	Code = VirtualAlloc(NULL, Size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	assert(Code != NULL);
}

JITFunction::JITFunction(const void* code, unsigned size) : Code(NULL), Size(0)
{
	Size = (size + 4095) & ~4095;
	Code = VirtualAlloc(NULL, Size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	assert(Code != NULL);
	memcpy(Code, code, size);
	//bool results = VirtualProtect(Code, Size, PAGE_EXECUTE_READ, NULL) != 0;
	//assert(results);
}

void JITFunction::set(const JITCode& code)
{
	if(code.size() > Size)
		throw std::overflow_error("not enought place to put code");
	memcpy(Code, code.code(), code.size());
}

JITFunction::~JITFunction() {
	if(Code) {
		VirtualFree(Code, 4096, MEM_RELEASE);
	}
}

void JITFunction::resolveImportTable( const ImportList& importList ) const
{
	for( unsigned i = 0; i < importList.size(); ++i )
	{
		const ImportEntry& e = importList[i];
		unsigned offset = e.first & (JITResolveAsAddress-1);
		assert(offset + sizeof(void*) <= Size);
		int* p = (int*)code(offset);
		assert(*p == JITFunctionImport);

		if(e.first & JITResolveAsAddress)
			*(void**)p = e.second;
		else
			*p = (char*)e.second - (char*)p - sizeof(void*);
	}
}

std::auto_ptr<JITFunction> JITCode::function() const {
	if(LabelList.size())
		throw std::runtime_error("has not closed labels");

	return std::auto_ptr<JITFunction>(new JITFunction(Code.c_str(), Code.size()));
}

void JITCode::jmpLabel( LabelType label, JmpType j /*= JMP*/ )
{
	static byte codes[] = {0x0, 0x84, 0x85, 0x8F, 0x8D, 0x8C, 0x8E};

	if(j == JMP)
	{
		emitByte(0xE9);
	}
	else
	{
		emitByte(0x0F);
		emitByte(codes[j]);
	}

	emitLabel(label);
}

void JITCode::label( LabelType label )
{
	LabelOffset[label] = Code.size();

	if(LabelList.find(label) != LabelList.end())
	{
		vector<unsigned>& labelList = LabelList[label];

		for(unsigned i = 0; i < labelList.size(); ++i)
		{
			unsigned* offset = (unsigned*)&Code[labelList[i]];
			assert(*offset == JITFunctionImport);
			*offset = Code.size() - labelList[i] - sizeof(unsigned);
		}

		LabelList.erase(label);
	}
}

void JITCode::align( unsigned block, byte code /*= 0x90*/ )
{
	Code.resize((Code.size() + block - 1) & ~(block-1), code);
}

void JITCode::emitLabel( LabelType label )
{
	if(LabelOffset.find(label) == LabelOffset.end())
	{
		LabelList[label].push_back(Code.size());
		emitLong(JITFunctionImport);
	}
	else
	{
		emitLong(LabelOffset[label] - Code.size() - sizeof(unsigned));
	}
}

bool JITCode::has( LabelType label ) const
{
	return LabelOffset.find(label) != LabelOffset.end();
}

void JITCode::addCustomFunction( void* function )
{
	CustomFunctions.insert(function);
}

vector<unsigned> JITCode::needs( LabelType label )
{
	LabelListMap::iterator itor = LabelList.find(label);
	vector<unsigned> list;

	if(itor != LabelList.end())
	{
		list.swap(itor->second);
		LabelList.erase(itor);
	}
	return list;
}
