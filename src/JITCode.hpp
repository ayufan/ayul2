#pragma once

class JITCode;

typedef std::pair<unsigned, void*> ImportEntry;
typedef std::vector<ImportEntry> ImportList;

const unsigned JITFunctionImport = 0x65756545;
const unsigned JITResolveAsAddress = 1<<31;

class JITFunction
{
	void* Code;
	unsigned Size;

public:
	void* code() const {
		return Code;
	}
	void* code(unsigned offset) const {
		return (char*)Code + offset;
	}
	unsigned size() const {
		return Size;
	}

	void resolveImportTable(const ImportList& importList) const;

	void set(const JITCode& code);

	JITFunction(unsigned size = 1);
	JITFunction(const void* code, unsigned size);
	~JITFunction();
};

enum RegisterType
{
	NONE = 0xFF,
	EAX = 0x0,
	ECX = 0x1,
	EDX = 0x2,
	EBX = 0x3,
	ESP = 0x4,
	EBP = 0x5,
	ESI = 0x6,
	EDI = 0x7,
	PTR = 0x8
};

enum FpuType
{
	ST0 = 0x0,
	ST1 = 0x1,
	ST2 = 0x2,
	ST3 = 0x3,
	ST4 = 0x4,
	ST5 = 0x5,
	ST6 = 0x6,
	ST7 = 0x7,
	STCount
};

enum FpuMathType
{
	FADD = 0x0,
	FSUB = 0x1,
	FMUL = 0x2,
	FDIV = 0x3,
};

enum FpuMathExtType
{
	FABS = 0x0,
	FCHS = 0x1,
	FSQRT = 0x2,
	FSIN = 0x3,
	FCOS = 0x4,
	FSINCOS = 0x5,
};

enum JmpType
{
	JMP = 0x0,
	JE = 0x1,
	JNE = 0x2,
	JG = 0x3,
	JGE = 0x4,
	JL = 0x5,
	JLE = 0x6,
};

enum MathType
{
	ADD = 0x0,
	SUB = 0x1,
	AND = 0x2,
	OR = 0x3,
	XOR = 0x4,
};

enum ScaleType
{
	SCALE1 = 0x0,
	SCALE2 = 0x40,
	SCALE4 = 0x80,
	SCALE8 = 0xC0
};

struct MemType
{
	RegisterType Reg1;
	RegisterType Reg2;
	ScaleType Scale;
	union {
		long Disp;
		void* Ptr;
	};

	MemType();
	explicit MemType(RegisterType reg, long disp = 0);
	explicit MemType(RegisterType reg, ScaleType scale, long disp = 0);
	explicit MemType(RegisterType reg1, RegisterType reg2, ScaleType scale = SCALE1, long disp = 0);
	explicit MemType(void *ptr);
};

struct LabelType
{
	const void* This;
	unsigned Label;

	LabelType() : This(NULL), Label(0)
	{}

	LabelType(const void* this$, unsigned label) : This(this$), Label(label)
	{}

	friend bool operator < (const LabelType& a, const LabelType& b)
	{
		return a.This < b.This || (a.This == b.This && a.Label < b.Label);
	}
};

class JITCode
{
public:
	static const char *RegisterNames[];

	typedef std::map<LabelType, vector<unsigned> > LabelListMap;

private:
	string Code;
	std::map<LabelType, unsigned> LabelOffset;
	std::set<void*> CustomFunctions;
	LabelListMap LabelList;

	void emitByte(long value);
	void emitShort(long value);
	void emitLong(long value);
	void emitLong(void* value$);
	void emitChar(int value);
	void emitMem(MemType mem, byte sub);
	void emitOffset(byte r1, byte r2);
	void emitLabel(LabelType label);

public:
	const void* code() const { return Code.c_str(); }
	unsigned size() const { return Code.size(); }

	std::auto_ptr<JITFunction> function() const;
	bool has(LabelType label) const;
	vector<unsigned> needs(LabelType label);
	void addCustomFunction(void* function);
	const std::set<void*>& customFunctions() const { return CustomFunctions; }

	void clear();
	void align(unsigned block, byte code = 0x90);
	void nop();
	void push(RegisterType reg);
	void push(MemType mem);
	void push(long value);
	void push(void* value);
	void push(LabelType label);
	void pop(RegisterType reg);
	void pop(MemType mem);
	void mov(RegisterType src, RegisterType dest);
	void mov(MemType src, RegisterType dest);
	void mov(RegisterType src, MemType dest);
	void mov(RegisterType reg, long value);
	void mov(RegisterType reg, void* value);
	void mov(MemType mem, long value);
	void mov(MemType mem, void* value);
	void cmp(RegisterType r1, RegisterType r2);
	void cmp(MemType m1, RegisterType r2);
	void cmp(RegisterType r1, MemType m2);
	void cmp(RegisterType reg, long value);
	void cmp(MemType mem, long value);
	void jmp(RegisterType reg);
	void jmp(long rel, JmpType j = JMP);
	void jmpLabel(LabelType label, JmpType j = JMP);
	void label(LabelType label);
	void call(RegisterType reg);
	void call(MemType mem);
	void call(long rel);
	void call(LabelType label);
	void math(MathType math, RegisterType src, RegisterType dest);
	void math(MathType math, MemType src, RegisterType dest);
	void math(MathType math, RegisterType src, MemType dest);
	void math(MathType math, RegisterType reg, long value);
	void math(MathType math, MemType mem, long value);
	void inc(RegisterType reg);
	void inc(MemType mem);
	void dec(RegisterType reg);
	void dec(MemType mem);
	void mul(RegisterType src, RegisterType dest);
	void mul(MemType src, RegisterType dest);
	void mul(RegisterType src, long value);
	void mul(RegisterType src, long value, RegisterType dest);
	void mul(MemType src, long value, RegisterType dest);
	void div(RegisterType reg);
	void div(MemType mem);
	void not(RegisterType dest);
	void not(MemType dest);
	void neg(RegisterType dest);
	void neg(MemType dest);
	void shl(RegisterType dest, byte count);
	void shl(MemType dest, byte count);
	void shl(RegisterType dest);
	void shl(MemType mem);
	void shr(RegisterType dest, byte count);
	void shr(MemType dest, byte count);
	void shr(RegisterType dest);
	void shr(MemType dest);
	void cdq();
	void cld();
	void std();
	void rep_stos();
	void rep_movs();
	void lea(MemType src, RegisterType dest);
	void or(RegisterType src, RegisterType dest);
	void or(MemType src, RegisterType dest);
	void or(RegisterType src, MemType dest);
	void xor(RegisterType src, RegisterType dest);
	void xor(MemType src, RegisterType dest);
	void xor(RegisterType src, MemType dest);
	void and(RegisterType src, RegisterType dest);
	void and(MemType src, RegisterType dest);
	void and(RegisterType src, MemType dest);
	void ret();
	void ret(long size);
	void fld(FpuType fpu);
	void fld(MemType mem);
	void fild(MemType mem);
	void fst(FpuType fpu);
	void fst(MemType mem);
	void fstp(FpuType fpu);
	void fstp(MemType mem);
	void fcomi(FpuType with = ST1);
	void fist(MemType mem);
	void fistp(MemType mem);
	void fld1();
	void fldz();
	void fldpi();
	void math(FpuMathType math, FpuType dest = ST1);
	void math(FpuMathType math, MemType mem);
	void math(FpuMathExtType math);
};