%{

//------------------------------------//
//
// syntax.cxx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-3-31
//
//------------------------------------//

#include "StdAfx.hpp"
#include "Lexer.hpp"
#include "Parser.hpp"

#ifdef _MSC_VER
// Treat some previous errors as warnings
#pragma warning(disable : 4244 4267)
#endif

#define YY_NEVER_INTERACTIVE 1
#define YY_SKIP_YYWRAP 1
	
void yyerror(const char *message) {
	asCurrentLine.error(message);
}

int yywrap(void) {
	return 1;
}

%}

Identifier	[a-zA-Z_][a-zA-Z0-9_$]*
Integer			[0-9]+
Float				[0-9]*"."[0-9]+
String			\"[^\"\n]*\"
Space				[ \t]+

%%

"module"			{ yylval.Line.update(); return T_MODULE; }
"class"				{ yylval.Line.update(); return T_CLASS; }
"struct" { yylval.Line.update(); return T_STRUCT; }
"if"				{	yylval.Line.update(); return T_IF;				}
"else"			{	yylval.Line.update(); return T_ELSE;			}
"while"			{	yylval.Line.update(); return T_WHILE;			}
"do"				{	yylval.Line.update(); return T_DO;				}
"for"				{	yylval.Line.update(); return T_FOR;				}
"return"		{	yylval.Line.update(); return T_RETURN;		}
"continue"	{	yylval.Line.update(); return T_CONTINUE;	}
"break"			{	yylval.Line.update(); return T_BREAK;			}
"null"			{ yylval.Line.update(); return T_NULL;			}
"true"			{ yylval.Line.update(); yylval.Boolean = true; return T_BOOLEAN_VALUE;	}
"false"			{ yylval.Line.update(); yylval.Boolean = false; return T_BOOLEAN_VALUE;	}
"foreach"		{ yylval.Line.update(); return T_FOREACH;		}
"is"				{ yylval.Line.update(); return T_IS;				}
"as"				{ yylval.Line.update(); return T_AS;				}
"this"			{ yylval.Line.update(); return T_THIS;			}
"public" { yylval.Line.update(); return T_PUBLIC; }
"private" { yylval.Line.update(); return T_PRIVATE; }
"protected" { yylval.Line.update(); return T_PROTECTED; }
"static" { yylval.Line.update(); return T_STATIC; }
"get" { yylval.Line.update(); return T_GET; }
"set" { yylval.Line.update(); return T_SET; }
"interface" { yylval.Line.update(); return T_INTERFACE; }
"implements" { yylval.Line.update(); return T_IMPLEMENTS; }
"extends" { yylval.Line.update(); return T_EXTENDS; }
"void" { yylval.Line.update(); return T_VOID; }
"delegate" { yylval.Line.update(); return T_DELEGATE; }
"event" { yylval.Line.update(); return T_EVENT; }
"typedef" { yylval.Line.update(); return T_TYPEDEF; }
"ref" { yylval.Line.update(); return T_REF; }
"new" { yylval.Line.update(); return T_NEW; }
"try" { yylval.Line.update(); return T_TRY; }
"catch" { yylval.Line.update(); return T_CATCH; }
"finally" { yylval.Line.update(); return T_FINALLY; }
"throw" { yylval.Line.update(); return T_THROW; }
"switch" { yylval.Line.update(); return T_SWITCH; }
"case" { yylval.Line.update(); return T_CASE; }
"default" { yylval.Line.update(); return T_DEFAULT; }

"{"					{	yylval.Line.update(); return T_BEGIN;			}
"}"					{	yylval.Line.update(); return T_END;				}
"("					{	yylval.Line.update(); return T_OPEN;			}
")"					{	yylval.Line.update(); return T_CLOSE;			}
"["					{	yylval.Line.update(); return T_S_OPEN;		}
"]"					{	yylval.Line.update(); return T_S_CLOSE;		}
"?"					{ yylval.Line.update(); return T_TEST;			}
":"					{ yylval.Line.update(); return T_OTHERWISE;	}
"::"					{ yylval.Line.update(); return T_SCOPE;	}
";"					{	yylval.Line.update(); return T_STATEMENT;	}

","					{	yylval.Line.update(); return T_SEPARATOR;	}
"."					{	yylval.Line.update(); return T_ACCESSOR;	}

"=="				{ yylval.Line.update(); return T_EQUAL;					}
"!="				{	yylval.Line.update(); return T_NOT_EQUAL;			}
"<="				{	yylval.Line.update(); return T_LESS_EQUAL;		}
">="				{	yylval.Line.update(); return T_GREATER_EQUAL;	}
"<"					{	yylval.Line.update(); return T_LESS;					}
">"					{	yylval.Line.update(); return T_GREATER;				}
"!"					{	yylval.Line.update(); return T_NEGATION;			}

"="					{	yylval.Line.update(); return T_ASSIGNMENT;							}
"+="				{	yylval.Line.update(); return T_ADDITIVE_ASSIGNMENT;			}
"-="				{	yylval.Line.update(); return T_SUBTRACTIVE_ASSIGNMENT;	}
"*="				{	yylval.Line.update(); return T_MULTIPLY_ASSIGNMENT;			}
"/="				{	yylval.Line.update(); return T_DIVIDE_ASSIGNMENT;				}
"%="				{	yylval.Line.update(); return T_MODULUS_ASSIGNMENT;			}
"^="				{	yylval.Line.update(); return T_BXOR_ASSIGNMENT;				}
"&="				{	yylval.Line.update(); return T_BAND_ASSIGNMENT;				}
"|="				{	yylval.Line.update(); return T_BOR_ASSIGNMENT;				}

"+"					{	yylval.Line.update(); return T_ADDITION;				}
"-"					{	yylval.Line.update(); return T_SUBTRACTION;			}
"*"					{	yylval.Line.update(); return T_MULTIPLICATION;	}
"/"					{	yylval.Line.update(); return T_DIVIDE;					}
"%"					{	yylval.Line.update(); return T_MODULUS;					}
"^"					{	yylval.Line.update(); return T_BXOR;						}
"&"					{	yylval.Line.update(); return T_BAND;						}
"|"					{	yylval.Line.update(); return T_BOR;						}

"&&"				{	yylval.Line.update(); return T_AND;							}
"||"				{	yylval.Line.update(); return T_OR;							}

"++"				{	yylval.Line.update(); return T_INCREMENTATION;	}
"--"				{	yylval.Line.update(); return T_DECREMENTATION;	}

{Identifier} {	
	yylval.String = yytext;
	yylval.Line.update();
	return T_IDENTIFIER;		
}
{Integer} {	
	yylval.Integer = atoi(yytext);
	yylval.Line.update();
	return T_INTEGER_VALUE;	
}
{Float} {	
	yylval.Float = (float)atof(yytext);
	yylval.Line.update();
	return T_FLOAT_VALUE;					
}
{String} {	
	static char escapeSeq[256];
	static int escapeSeqSet = 0;

	if(escapeSeqSet == 0) {
		memset(escapeSeq, 0, sizeof(escapeSeq));
		escapeSeq['a'] = '\a';
		escapeSeq['b'] = '\b';
		escapeSeq['f'] = '\f';
		escapeSeq['n'] = '\n';
		escapeSeq['r'] = '\r';
		escapeSeq['t'] = '\t';
		escapeSeq['v'] = '\v';
		escapeSeq['\''] = '\'';
		escapeSeq['\"'] = '\"';
		escapeSeq['\\'] = '\\';
		escapeSeq['?'] = '\?';
		escapeSeq['0'] = '\0';
		escapeSeqSet = 1;
	}	
	
	yylval.String.assign(yytext+1, yytext+strlen(yytext)-1);
	yylval.Line.update();

	for(string::iterator i = yylval.String.begin(), p = yylval.String.begin(); i != yylval.String.end(); ++i, ++p) {
		if(*i == '\n')
			asCurrentLine.Line++;

		if(*i == '\\' && i+1 != yylval.String.end() && i[1] < sizeof(escapeSeq) && escapeSeq[i[1]] != 0) {
			*p = escapeSeq[*++i];
		}
		else if(i != p) {
			*p = *i;
		}
	}

 	return T_STRING_VALUE;				
}
"//" {
	char c;

  while((c = yyinput()) != '\n' && c != 0);
	asCurrentLine.Line++;
}
"/*" {
	char	c1, c2;

	c1 = yyinput();
	c2 = yyinput();

	if(c1 == '\n')
		asCurrentLine.Line++;

	while(c1 != 0 && c2 != 0) {
		if(c1 == '*' && c2 == '/')
			break;

		if(c1 == '\n')
			asCurrentLine.Line++;

		c1 = c2;
		c2 = yyinput();
	}
}
\r						{}
\n						{ asCurrentLine.Line++; }
{Space}				{}

%%
