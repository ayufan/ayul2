#ifndef Lexer_h__
#define Lexer_h__

#define YYSTYPE_IS_DECLARED
struct YYSTYPE {
	union {
		SyntaxVisiblity Visibility;
		int	Integer;
		bool Boolean;
		float	Float;
	};
	SyntaxLine Line;
	std::string	String;
	intrusive_ptr<SyntaxObject> Object;
	vector<intrusive_ptr<SyntaxObject> > ObjectList;
	vector<std::string> StringList;

	// RefCount Methods
	intrusive_ptr<SyntaxType> &Type() { return (intrusive_ptr<SyntaxType>&)Object; }
	intrusive_ptr<SyntaxValue> &Value() { return (intrusive_ptr<SyntaxValue>&)Object; }
	intrusive_ptr<SyntaxExpression> &Expression() { return (intrusive_ptr<SyntaxExpression>&)Object; }
	intrusive_ptr<SyntaxStatement> &Statement() { return (intrusive_ptr<SyntaxStatement>&)Object; }
	intrusive_ptr<SyntaxCatchStatement> &CatchStatement() { return (intrusive_ptr<SyntaxCatchStatement>&)Object; }
	intrusive_ptr<SyntaxVariable> &Variable() { return (intrusive_ptr<SyntaxVariable>&)Object; }
	intrusive_ptr<SyntaxNamedObject> &NamedObject() { return (intrusive_ptr<SyntaxNamedObject>&)Object; }
	intrusive_ptr<SyntaxModuleObject> &ModuleObject() { return (intrusive_ptr<SyntaxModuleObject>&)Object; }
	intrusive_ptr<SyntaxCaseStatement> &CaseStatement() { return (intrusive_ptr<SyntaxCaseStatement>&)Object; }
	intrusive_ptr<SyntaxContext> &Context() { return (intrusive_ptr<SyntaxContext>&)Object; }

	// RefCount Array Methods
	vector<intrusive_ptr<SyntaxNamedObject> > &NamedObjectList() { return (vector<intrusive_ptr<SyntaxNamedObject> >&)ObjectList; }
	vector<intrusive_ptr<SyntaxModuleObject> > &ModuleObjectList() { return (vector<intrusive_ptr<SyntaxModuleObject> >&)ObjectList; }
	vector<intrusive_ptr<SyntaxVariable> > &VariableList() { return (vector<intrusive_ptr<SyntaxVariable> >&)ObjectList; }
	vector<intrusive_ptr<SyntaxExpression> > &ExpressionList() { return (vector<intrusive_ptr<SyntaxExpression> >&)ObjectList; }
	vector<intrusive_ptr<SyntaxStatement> > &StatementList() { return (vector<intrusive_ptr<SyntaxStatement> >&)ObjectList; }
	vector<intrusive_ptr<SyntaxCatchStatement> > &CatchStatementList() { return (vector<intrusive_ptr<SyntaxCatchStatement> >&)ObjectList; }
	vector<intrusive_ptr<SyntaxCaseStatement> > &CaseStatementList() { return (vector<intrusive_ptr<SyntaxCaseStatement> >&)ObjectList; }
};

#endif // Lexer_h__
