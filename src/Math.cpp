#include "StdAfx.hpp"

BEGIN_VALUE_TYPE(Vec2) {
	CLASS_VARIABLE(X);
	CLASS_VARIABLE(Y);

	CLASS_CONSTRUCTOR();
	CLASS_CONSTRUCTOR(Float32, Float32);

	CLASS_METHOD(normalize);

	CLASS_OPERATOR2(+, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(+, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(+, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(^, Float32, ThisClass, ThisClass);

	CLASS_ASSIGNMENT(=, ThisClass);
	CLASS_ASSIGNMENT(+=, ThisClass);
	CLASS_ASSIGNMENT(+=, Float32);
	CLASS_ASSIGNMENT(-=, ThisClass);
	CLASS_ASSIGNMENT(-=, Float32);
	CLASS_ASSIGNMENT(*=, ThisClass);
	CLASS_ASSIGNMENT(*=, Float32);
	CLASS_ASSIGNMENT(/=, ThisClass);
	CLASS_ASSIGNMENT(/=, Float32);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<, Boolean, ThisClass, ThisClass);
}

BEGIN_VALUE_TYPE(Vec3) {
	CLASS_VARIABLE(X);
	CLASS_VARIABLE(Y);
	CLASS_VARIABLE(Z);

	CLASS_CONSTRUCTOR();
	CLASS_CONSTRUCTOR(Float32, Float32, Float32);

	CLASS_METHOD(normalize);

	CLASS_OPERATOR2(+, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(+, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(+, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(^, Float32, ThisClass, ThisClass);
	CLASS_OPERATOR2(%, ThisClass, ThisClass, ThisClass);

	CLASS_ASSIGNMENT(=, ThisClass);
	CLASS_ASSIGNMENT(+=, ThisClass);
	CLASS_ASSIGNMENT(+=, Float32);
	CLASS_ASSIGNMENT(-=, ThisClass);
	CLASS_ASSIGNMENT(-=, Float32);
	CLASS_ASSIGNMENT(*=, ThisClass);
	CLASS_ASSIGNMENT(*=, Float32);
	CLASS_ASSIGNMENT(/=, ThisClass);
	CLASS_ASSIGNMENT(/=, Float32);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<, Boolean, ThisClass, ThisClass);
}

BEGIN_VALUE_TYPE(Vec4) {
	CLASS_VARIABLE(X);
	CLASS_VARIABLE(Y);
	CLASS_VARIABLE(Z);
	CLASS_VARIABLE(W);

	CLASS_CONSTRUCTOR();
	CLASS_CONSTRUCTOR(Float32, Float32, Float32);
	CLASS_CONSTRUCTOR(Float32, Float32, Float32, Float32);

	CLASS_METHOD(normalize);

	CLASS_OPERATOR2(+, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(+, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(+, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, Float32, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, Float32);
	CLASS_OPERATOR2(^, Float32, ThisClass&, ThisClass&);

	CLASS_ASSIGNMENT(=, ThisClass);
	CLASS_ASSIGNMENT(+=, ThisClass);
	CLASS_ASSIGNMENT(+=, Float32);
	CLASS_ASSIGNMENT(-=, ThisClass);
	CLASS_ASSIGNMENT(-=, Float32);
	CLASS_ASSIGNMENT(*=, ThisClass);
	CLASS_ASSIGNMENT(*=, Float32);
	CLASS_ASSIGNMENT(/=, ThisClass);
	CLASS_ASSIGNMENT(/=, Float32);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<, Boolean, ThisClass, ThisClass);
}

BEGIN_STATIC_CLASS(MathLib) {
	CLASS_METHOD(Sin);
	CLASS_METHOD(Cos);
	CLASS_METHOD(Tan);
	CLASS_GET_PROPERTY(PI, getPI);
}
