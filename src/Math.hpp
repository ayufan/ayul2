#ifndef Math_h__
#define Math_h__

const float Epsf = 1e-30;

struct Vec2 {
	DECLARE_VALUE_TYPE(Vec2);

	Float32 X, Y;

	Vec2() {
		X = 0;
		Y = 0;
	}
	Vec2(Float32 _x, Float32 _y) {
		X = _x;
		Y = _y;
	}

	Float32 lengthSq() const {
		return X * X + Y * Y;
	}
	Float32 length() const {
		return sqrtf(X * X + Y * Y);
	}
	bool empty() const {
		return X == 0.0f && Y == 0.0f;
	}
	Vec2 normalize() const {
		return *this / length();
	}

	// Friends
	friend Vec2 &operator += (Vec2 &c, Vec2 v) {
		c.X.Value += v.X.Value;
		c.Y.Value += v.Y.Value;
		return c;
	}
	friend Vec2 &operator += (Vec2 &c, Float32 v)	{
		c.X.Value += v.Value;
		c.Y.Value += v.Value;
		return c;
	}
	friend Vec2 &operator -= (Vec2 &c, Vec2 v) {
		c.X.Value -= v.X.Value;
		c.Y.Value -= v.Y.Value;
		return c;
	}
	friend Vec2 &operator -= (Vec2 &c, Float32 v)	{
		c.X.Value -= v.Value;
		c.Y.Value -= v.Value;
		return c;
	}
	friend Vec2 &operator *= (Vec2 &c, Vec2 v)	{
		c.X.Value *= v.X.Value;
		c.Y.Value *= v.Y.Value;
		return c;
	}
	friend Vec2 &operator *= (Vec2 &c, Float32 v)	{
		c.X.Value *= v.Value;
		c.Y.Value *= v.Value;
		return c;
	}
	friend Vec2 &operator /= (Vec2 &c, Vec2 v) {
		c.X.Value /= v.X.Value;
		c.Y.Value /= v.Y.Value;
		return c;
	}
	friend Vec2 &operator /= (Vec2 &c, Float32 v)	{
		c.X.Value /= v.Value;
		c.Y.Value /= v.Value;
		return c;
	}
	friend Vec2 operator + (Vec2 a, Vec2 b) {
		return Vec2(a.X + b.X, a.Y + b.Y);
	}
	friend Vec2 operator + (Float32 a, Vec2 b) {
		return Vec2(a + b.X, a + b.Y);
	}
	friend Vec2 operator + (Vec2 a, Float32 b) {
		return Vec2(a.X + b, a.Y + b);
	}
	friend Vec2 operator - (Float32 a, Vec2 b) {
		return Vec2(a - b.X, a - b.Y);
	}
	friend Vec2 operator - (Vec2 a, Vec2 b) {
		return Vec2(a.X - b.X, a.Y - b.Y);
	}
	friend Vec2 operator - (Vec2 a, Float32 b) {
		return Vec2(a.X - b, a.Y - b);
	}
	friend Vec2 operator * (Vec2 a, Vec2 b) {
		return Vec2(a.X * b.X, a.Y * b.Y);
	}
	friend Vec2 operator * (Vec2 a, Float32 b) {
		return Vec2(a.X * b, a.Y * b);
	}
	friend Vec2 operator * (Float32 a, Vec2 b) {
		return Vec2(a * b.X, a * b.Y);
	}
	friend Vec2 operator / (Vec2 a, Vec2 b) {
		return Vec2(a.X / b.X, a.Y / b.Y);
	}
	friend Vec2 operator / (Vec2 a, Float32 b) {
		return Vec2(a.X / b, a.Y / b);
	}
	friend Vec2 operator / (Float32 a, Vec2 b) {
		return Vec2(a / b.X, a / b.Y);
	}
	friend Float32 operator ^ (Vec2 a, Vec2 b) {
		return a.X * b.X + a.Y * b.Y;
	}
	friend Vec2 operator + (Vec2 v) {
		return Vec2(+v.X, +v.Y);
	}
	friend Vec2 operator - (Vec2 v) {
		return Vec2(-v.X, -v.Y);
	}
	friend bool operator == (Vec2 a, Vec2 b) {
		return fabs(a.X - b.X) < Epsf && fabs(a.Y - b.Y) < Epsf;
	}
	friend bool operator != (Vec2 a, Vec2 b) {
		return a.X != b.X || a.Y != b.Y;
	}
	friend bool operator < (Vec2 a, Vec2 b) {
		return a.X - b.X < -Epsf && a.Y - b.Y < -Epsf;
	}
};

struct Vec3 {
	DECLARE_VALUE_TYPE(Vec3);

	Float32 X, Y, Z;

	Vec3() {
		X = 0;
		Y = 0;
		Z = 0;
	}
	Vec3(Float32 _x, Float32 _y, Float32 _z) {
		X = _x;
		Y = _y;
		Z = _z;
	}

	Float32 lengthSq() const {
		return X * X + Y * Y + Z * Z;
	}
	Float32 length() const {
		return sqrtf(X * X + Y * Y + Z * Z);
	}
	bool empty() const {
		return X == 0.0f && Y == 0.0f && Z == 0.0f;
	}
	Vec3 normalize() const {
		return *this / length();
	}

	// Friends
	friend Vec3 &operator += (Vec3 &c, Vec3 v) {
		c.X.Value += v.X.Value;
		c.Y.Value += v.Y.Value;
		c.Z.Value += v.Z.Value;
		return c;
	}
	friend Vec3 &operator += (Vec3 &c, Float32 v)	{
		c.X.Value += v.Value;
		c.Y.Value += v.Value;
		c.Z.Value += v.Value;
		return c;
	}
	friend Vec3 &operator -= (Vec3 &c, Vec3 v) {
		c.X.Value -= v.X.Value;
		c.Y.Value -= v.Y.Value;
		c.Z.Value -= v.Z.Value;
		return c;
	}
	friend Vec3 &operator -= (Vec3 &c, Float32 v)	{
		c.X.Value -= v.Value;
		c.Y.Value -= v.Value;
		c.Z.Value -= v.Value;
		return c;
	}
	friend Vec3 &operator *= (Vec3 &c, Vec3 v)	{
		c.X.Value *= v.X.Value;
		c.Y.Value *= v.Y.Value;
		c.Z.Value *= v.Z.Value;
		return c;
	}
	friend Vec3 &operator *= (Vec3 &c, Float32 v)	{
		c.X.Value *= v.Value;
		c.Y.Value *= v.Value;
		c.Z.Value *= v.Value;
		return c;
	}
	friend Vec3 &operator /= (Vec3 &c, Vec3 v) {
		c.X.Value /= v.X.Value;
		c.Y.Value /= v.Y.Value;
		c.Z.Value /= v.Z.Value;
		return c;
	}
	friend Vec3 &operator /= (Vec3 &c, Float32 v)	{
		c.X.Value /= v.Value;
		c.Y.Value /= v.Value;
		c.Z.Value /= v.Value;
		return c;
	}
	friend Vec3 operator + (Vec3 a, Vec3 b) {
		return Vec3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
	}
	friend Vec3 operator + (Float32 a, Vec3 b) {
		return Vec3(a + b.X, a + b.Y, a + b.Z);
	}
	friend Vec3 operator + (Vec3 a, Float32 b) {
		return Vec3(a.X + b, a.Y + b, a.Z + b);
	}
	friend Vec3 operator - (Float32 a, Vec3 b) {
		return Vec3(a - b.X, a - b.Y, a - b.Z);
	}
	friend Vec3 operator - (Vec3 a, Vec3 b) {
		return Vec3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
	}
	friend Vec3 operator - (Vec3 a, Float32 b) {
		return Vec3(a.X - b, a.Y - b, a.Z - b);
	}
	friend Vec3 operator * (Vec3 a, Vec3 b) {
		return Vec3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
	}
	friend Vec3 operator * (Vec3 a, Float32 b) {
		return Vec3(a.X * b, a.Y * b, a.Z * b);
	}
	friend Vec3 operator * (Float32 a, Vec3 b) {
		return Vec3(a * b.X, a * b.Y, a * b.Z);
	}
	friend Vec3 operator / (Vec3 a, Vec3 b) {
		return Vec3(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
	}
	friend Vec3 operator / (Vec3 a, Float32 b) {
		return Vec3(a.X / b, a.Y / b, a.Z / b);
	}
	friend Vec3 operator / (Float32 a, Vec3 b) {
		return Vec3(a / b.X, a / b.Y, a / b.Z);
	}
	friend Float32 operator ^ (Vec3 a, Vec3 b) {
		return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
	}
	friend Vec3 operator % (Vec3 a, Vec3 b) {
		return Vec3(a.Y * b.Z - b.Y * a.Z, a.Z * b.X - b.Z * a.X, a.X * b.Y - b.X * a.Y);
	}
	friend Vec3 operator + (Vec3 v) {
		return Vec3(+v.X, +v.Y, +v.Z);
	}
	friend Vec3 operator - (Vec3 v) {
		return Vec3(-v.X, -v.Y, -v.Z);
	}
	friend bool operator == (Vec3 a, Vec3 b) {
		return fabs(a.X - b.X) < Epsf && fabs(a.Y - b.Y) < Epsf && fabs(a.Z - b.Z) < Epsf;
	}
	friend bool operator != (Vec3 a, Vec3 b) {
		return a.X != b.X || a.Y != b.Y || a.Z != b.Z;
	}
	friend bool operator < (Vec3 a, Vec3 b) {
		return a.X - b.X < -Epsf && a.Y - b.Y < -Epsf && a.Z - b.Z < -Epsf;
	}
};

struct Vec4 {
	DECLARE_VALUE_TYPE(Vec4);

	Float32 X, Y, Z, W;

	Vec4() {
		X = 0;
		Y = 0;
		Z = 0;
		W = 0;
	}
	Vec4(Float32 _x, Float32 _y, Float32 _z, Float32 _w = 0) {
		X = _x;
		Y = _y;
		Z = _z;
		W = _w;
	}

	Float32 lengthSq() const {
		return X * X + Y * Y + Z * Z + W * W;
	}
	Float32 length() const {
		return sqrtf(X * X + Y * Y + Z * Z + W * W);
	}
	bool empty() const {
		return X == 0.0f && Y == 0.0f && Z == 0.0f && W == 0.0f;
	}
	Vec4 normalize() const {
		return *this / length();
	}

	// Friends
	friend Vec4 &operator += (Vec4 &c, Vec4 v) {
		c.X.Value += v.X.Value;
		c.Y.Value += v.Y.Value;
		c.Z.Value += v.Z.Value;
		c.W.Value += v.W.Value;
		return c;
	}
	friend Vec4 &operator += (Vec4 &c, Float32 v)	{
		c.X.Value += v.Value;
		c.Y.Value += v.Value;
		c.Z.Value += v.Value;
		c.W.Value += v.Value;
		return c;
	}
	friend Vec4 &operator -= (Vec4 &c, Vec4 v) {
		c.X.Value -= v.X.Value;
		c.Y.Value -= v.Y.Value;
		c.Z.Value -= v.Z.Value;
		c.W.Value -= v.W.Value;
		return c;
	}
	friend Vec4 &operator -= (Vec4 &c, Float32 v)	{
		c.X.Value -= v.Value;
		c.Y.Value -= v.Value;
		c.Z.Value -= v.Value;
		c.W.Value -= v.Value;
		return c;
	}
	friend Vec4 &operator *= (Vec4 &c, Vec4 v)	{
		c.X.Value *= v.X.Value;
		c.Y.Value *= v.Y.Value;
		c.Z.Value *= v.Z.Value;
		c.W.Value *= v.W.Value;
		return c;
	}
	friend Vec4 &operator *= (Vec4 &c, Float32 v)	{
		c.X.Value *= v.Value;
		c.Y.Value *= v.Value;
		c.Z.Value *= v.Value;
		c.W.Value *= v.Value;
		return c;
	}
	friend Vec4 &operator /= (Vec4 &c, Vec4 v) {
		c.X.Value /= v.X.Value;
		c.Y.Value /= v.Y.Value;
		c.Z.Value /= v.Z.Value;
		c.W.Value /= v.W.Value;
		return c;
	}
	friend Vec4 &operator /= (Vec4 &c, Float32 v)	{
		c.X.Value /= v.Value;
		c.Y.Value /= v.Value;
		c.Z.Value /= v.Value;
		c.W.Value /= v.Value;
		return c;
	}
	friend Vec4 operator + (Vec4 a, Vec4 b) {
		return Vec4(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W);
	}
	friend Vec4 operator + (Float32 a, Vec4 b) {
		return Vec4(a + b.X, a + b.Y, a + b.Z, a + b.W);
	}
	friend Vec4 operator + (Vec4 a, Float32 b) {
		return Vec4(a.X + b, a.Y + b, a.Z + b, a.W + b);
	}
	friend Vec4 operator - (Float32 a, Vec4 b) {
		return Vec4(a - b.X, a - b.Y, a - b.Z, a - b.W);
	}
	friend Vec4 operator - (Vec4 a, Vec4 b) {
		return Vec4(a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.W - b.W);
	}
	friend Vec4 operator - (Vec4 a, Float32 b) {
		return Vec4(a.X - b, a.Y - b, a.Z - b, a.W - b);
	}
	friend Vec4 operator * (Vec4 a, Vec4 b) {
		return Vec4(a.X * b.X, a.Y * b.Y, a.Z * b.Z, a.W * b.W);
	}
	friend Vec4 operator * (Vec4 a, Float32 b) {
		return Vec4(a.X * b, a.Y * b, a.Z * b, a.W * b);
	}
	friend Vec4 operator * (Float32 a, Vec4 b) {
		return Vec4(a * b.X, a * b.Y, a * b.Z, a * b.W);
	}
	friend Vec4 operator / (Vec4 a, Vec4 b) {
		return Vec4(a.X / b.X, a.Y / b.Y, a.Z / b.Z, a.W / b.W);
	}
	friend Vec4 operator / (Vec4 a, Float32 b) {
		return Vec4(a.X / b, a.Y / b, a.Z / b, a.W / b);
	}
	friend Vec4 operator / (Float32 a, Vec4 b) {
		return Vec4(a / b.X, a / b.Y, a / b.Z, a / b.W);
	}
	friend Float32 operator ^ (Vec4& a, Vec4& b) {
		return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;
	}
	friend Vec4 operator + (Vec4 v) {
		return Vec4(+v.X, +v.Y, +v.Z, +v.W);
	}
	friend Vec4 operator - (Vec4 v) {
		return Vec4(-v.X, -v.Y, -v.Z, -v.W);
	}
	friend bool operator == (Vec4 a, Vec4 b) {
		return fabs(a.X - b.X) < Epsf && fabs(a.Y - b.Y) < Epsf && fabs(a.Z - b.Z) < Epsf && fabs(a.W - b.W) < Epsf;
	}
	friend bool operator != (Vec4 a, Vec4 b) {
		return a.X != b.X || a.Y != b.Y || a.Z != b.Z || a.W != b.W;
	}
	friend bool operator < (Vec4 a, Vec4 b) {
		return a.X - b.X < -Epsf && a.Y - b.Y < -Epsf && a.Z - b.Z < -Epsf || a.W != b.W;
	}
};

class MathLib {
	DECLARE_CLASS(MathLib)
	static Float32 Sin(Float32 x) { return sinf(x); }
	static Float32 Cos(Float32 x) { return cosf(x); }
	static Float32 Tan(Float32 x) { return tanf(x); }
	static Float32 getPI() { return 3.14159265f; }
};

#endif // Math_h__