#include "StdAfx.hpp"

// V - variable index
// T - type
// F - function
// S - string
// X - ignore
//   - integer value
// C - class variable
// G - global index
// L - label index

string OpCode::toString(SyntaxCompilerContext& context) const {
	string buffer;
	const char* args = "   ";

	if(Type < OpCount) {
		buffer += OpList[Type].Name;
		args = OpList[Type].Args;
	}
	else {
		buffer += Int32(Type).toString2();
	}
	buffer += " ";

	for(unsigned i = 0; args[i]; ++i) {
		switch(args[i]) {
			case 'X':
				break;

			case 'V':
				{
					if(Args[i] == ~0)
						break;

					const ScriptLocalVariable& variable = context.VariableList.at(Args[i]);
					buffer += "[variable=" + variable.toString() + "] ";
				}
				break;

			case 'T':
				{
					const ScriptType& type = context.Module.TypeList[Args[i]];
					buffer += "[type=" + type.toString() + "] ";
				}
				break;

			case 'F':
				{
					const ScriptFunction& function = *context.Module.FunctionList[Args[i]];
					buffer += "[function=" + function.toRefString() + "] ";
				}
				break;

			case 'S':
				{
					const string &data = context.Module.DataList[Args[i]];
					buffer += "\"" + data + "\" ";
				}
				break;

			case ' ':
				{
					buffer += "[" + Int32(Args[i]).toString2() + "] ";
				}
				break;

			case 'C':
				{
					const ScriptClassVariable& variable = *context.Module.VariableList[Args[i]];
					buffer += "[classVariable=" + variable.toRefString() + "] ";
				}
				break;
		
			case 'G':
				{
					const ScriptClassVariable& variable = *context.Module.VariableList[Args[i]];
					buffer += "[globalVariable=" + variable.toRefString() + "] ";
				}
				break;

			case 'L':
				{
					buffer += "[label" + Int32(Args[i]).toString2() + "] ";
				}
				break;

			default:
				throw std::runtime_error("unsupported value in opcode description");
		}
	}

	return buffer;
}
