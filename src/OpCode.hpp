#ifndef OpCode_h__
#define OpCode_h__

typedef int (*DummyFunction)();

struct SyntaxCompilerContext;
struct ScriptScriptFunction;
class JITCode;

#define EMIT_FUNC(Name)			void op##Name(OpCode& in, JITCode& out, const ScriptScriptFunction& context)

struct OpCode {
	enum OpType {
		Label, // no operation <Label>
		Mark, // mark next statement
		ZeroLocal, // <VariableIndex> | <Offset> <Size>
		Pop, // discard object from stack : <Type>
		Return, // return object : <Type>
		DeRef, // dereference object - gets pointer from stack and pushes content : <Type>
		Call, // call function : <Func>
		CallMethod, // call this->function : <Func>
		PushFunction, // pushes function address : <Func>
		PushString, // pushes string value : <Offset> <Size>
		PushValueType, // pushes value type : <Type> <Offset> <Size>
		PushTrue, // pushes true value
		PushFalse, // pushes false value
		PushNull, // pushes null value
		PushLocal, // pushes local variable : <VariableIndex>
		PushArgument, // pushes local variable : <VariableIndex>
		PushOffset, // gets this and pushes variable at offset : <ClassVariable>
		PushThis, // pushes this pointer,
		PushStatic, // pushes static variable : <GlobalIndex>
		PopLocal, // pops value and saves to local : <VariableIndex>
		PopArgument, // pops value and saves to local : <VariableIndex>
		PopStatic, // pops value and saves to static variable : <GlobalIndex>
		PopOffset, // pops value and saves to class variable : <ClassVariable>
		Jump, // jump to specified label : <Label>
		JumpIfTrue, // jump to specified label : <Label>
		JumpIfFalse, // jump to specified label : <Label>
		CallConstructor, // call constructor : <Func> <Type>
		OpCount
	};

	struct OpDesc {
		const char* Name;
		const char* Args;
		void (*EmitFunc)(OpCode& op, JITCode& code, const ScriptScriptFunction& context);
	};
	static OpDesc OpList[OpCount]; 

	OpType Type;
	union {
		unsigned Args[3];
		int ArgInt[3];
		void* ArgPtr[3];
	};

	OpCode(OpType type = Label, unsigned arg0 = 0, unsigned arg1 = 0, unsigned arg2 = 0) : Type(type) {
		Args[0] = arg0;
		Args[1] = arg1;
		Args[2] = arg2;
	}

	string toString(SyntaxCompilerContext& context) const;

	void finalize(const OpCode& code, const ScriptScriptFunction& function, const vector<unsigned>& labelList);
};

#endif // OpCode_h__
