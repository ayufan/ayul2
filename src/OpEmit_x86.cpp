#include "StdAfx.hpp"

//#define BEGIN_FUNC
//#define END_FUNC
#define DEBUG_BREAK
#define ALIGN 1

const unsigned EndLabel = ~0;
const unsigned PtrSize = sizeof(void*);

#define LVAR(x,i)		const ScriptLocalVariable* x = in.Args[i] != ~0 ? &context.VariableList.at(in.Args[i]) : NULL;
#define TVAR(x,i)		const ScriptType* x = &context.Module->TypeList.at(in.Args[i]);
#define FVAR(x,i)		const ScriptFunction* x = context.Module->FunctionList.at(in.Args[i]).get();
#define DVAR(x,i)		const string& x = context.Module->DataList.at(in.Args[i]);
#define CVAR(x,i)		const ScriptClassVariable* x = context.Module->VariableList.at(in.Args[i]).get();

static MemType local(const ScriptScriptFunction& context, unsigned offset)
{
	return MemType(EBP, offset - context.VariableStackUsage - 2 * PtrSize);
}

static MemType self(const ScriptScriptFunction& context)
{
	assert(!context.isStaticFunction());
#ifdef _MSC_VER
	return MemType(EBP, -16);
#else
	return MemType(EBP, 8);
#endif
}

static MemType retVar(const ScriptScriptFunction& context)
{
	assert(!context.returnType().isPOD());

#ifdef _MSC_VER
	return MemType(EBP, 8);
#else
	if(!context.isStaticFunction())
		return MemType(EBP, 8);
	else
		return MemType(EBP, 12);
#endif
}

static MemType args(const ScriptScriptFunction& context, unsigned offset)
{
#ifndef _MSC_VER
	if(!context.isStaticFunction())
		offset += 4;
#endif
	if(!context.returnType().isPOD())
		offset += 4;
	return MemType(EBP, 4+offset);
}

#define LOCAL(offset)			local(context, offset)
#define SELF()						self(context)
#define RETVAR()					retVar(context)
#define ARGS(offset)			args(context, offset)

EMIT_FUNC(Label) 
{
	assert(in.ArgInt[0] > 0);
	out.label(LabelType(&context, in.Args[0]));
}

static void __stdcall InvalidESP()
{
	printf("!! invalid esp !!\n");
	DebugBreak();
}

EMIT_FUNC(Mark)
{
	static unsigned nextLabel = ~0;

	out.addCustomFunction((void*)InvalidESP);

	out.cmp(EBX, ESP);
	out.jmpLabel(LabelType(&context, --nextLabel), JE);
	out.call(LabelType((void*)InvalidESP, 0));
	out.label(LabelType(&context, nextLabel));
}

EMIT_FUNC(ZeroLocal)
{
	LVAR(variable, 0);

	if(variable->isArgument())
		throw std::runtime_error("not expected");

	unsigned offset = variable->Offset;
	unsigned end = variable->Type.sizeOfAligned();

	if((end-offset)&(PtrSize-1))
		throw std::invalid_argument("bytes not supported");

	while(offset < end)
	{
		switch(end-offset)
		{
		case PtrSize:
		case 2*PtrSize:
		case 3*PtrSize:
			out.mov(LOCAL(offset), 0L);
			offset += PtrSize;
			break;

		default:
			out.mov(EAX, 0L);
			out.mov(ECX, end-offset);
			out.rep_stos();
			offset = end;
			break;
		}
	}
}

EMIT_FUNC(Pop)
{
	TVAR(type, 0);

	unsigned size = type->IsRef ? PtrSize : type->sizeOfAligned();
	out.math(ADD, ESP, size);
}

EMIT_FUNC(Return)
{
	TVAR(type, 0);

	unsigned sizeOf = type->sizeOfAligned();

	if(sizeOf == 0)
		return;

	if(type->isPOD())
	{
		switch(sizeOf)
		{
		case 2*PtrSize: // int64
			out.pop(EDX);
		case PtrSize: // int
			out.pop(EAX);
		case 0: // void
			break;

		default:
			assert(0);
		}
	}
	else if(sizeOf <= 2 * PtrSize)
	{
		out.mov(RETVAR(), ECX);

		switch(sizeOf)
		{
		case 2*PtrSize: // int64
			out.pop(MemType(ECX, PtrSize));
		case PtrSize: // int
			out.pop(MemType(ECX));
		case 0: // void
			out.mov(ECX, EAX);
			break;

		default:
			assert(0);
		}
	}
	else
	{
		out.mov(ECX, sizeOf/4);
		out.mov(ESP, ESI);
		out.mov(RETVAR(), EDI);
		out.rep_movs();
		out.math(ADD, ESP, sizeOf);
		out.lea(RETVAR(), EAX);
	}
	out.jmpLabel(LabelType(&context, EndLabel));
}

EMIT_FUNC(DeRef)
{
	TVAR(type, 0);

	unsigned sizeOf = type->sizeOfAligned();

	if(sizeOf <= 3*PtrSize)
	{
		out.pop(EAX);

		switch(sizeOf)
		{
		case 3*PtrSize:
			out.push(MemType(EAX,8));
		case 2*PtrSize: // int64
			out.push(MemType(EAX,4));
		case PtrSize: // int
			out.push(MemType(EAX,0));
		case 0:
			break;
	
		default:
			assert(0);
		}
	}
	else
	{
		out.mov(ECX, sizeOf/4);
		out.pop(ESI);
		out.math(SUB, ESP, sizeOf);
		out.mov(ESP, EDI);
		out.rep_movs();
	}
}

EMIT_FUNC(PushFunction)
{
	FVAR(function, 0);
	out.push(LabelType(function, JITResolveAsAddress));
}

static void* __stdcall allocString(const char* text, unsigned length)
{
	return String::allocString(text, length);
}

EMIT_FUNC(PushString)
{
	DVAR(data, 0);

	out.addCustomFunction(allocString);

	out.push(data.size());
	out.push((void*)data.c_str());
	out.call(LabelType(allocString, 0));
	out.push(EAX);
}

EMIT_FUNC(PushValueType)
{
	TVAR(type, 0);
	DVAR(data, 1);

	if(data.size() != in.Args[2] || type->sizeOfAligned() != in.Args[2])
		throw std::runtime_error("internal error");

	unsigned sizeOf = data.size();
	unsigned* datap = (unsigned*)data.c_str();

	assert(~sizeOf&3);

	switch(sizeOf)
	{
	case 3*PtrSize:
		out.push(datap[2]);
	case 2*PtrSize:
		out.push(datap[1]);
	case 1*PtrSize:
		out.push(datap[0]);
	case 0:
		break;

	default:
		out.mov(ECX, sizeOf/4);
		out.mov(ESI, datap);
		out.math(SUB, ESP, sizeOf);
		out.mov(ESP, EDI);
		out.rep_movs();
		break;
	}
}

EMIT_FUNC(PushTrue)
{
	out.push(1L);
}

EMIT_FUNC(PushFalse)
{
	out.push(0L);
}

EMIT_FUNC(PushNull)
{
	out.push(0L);
}

EMIT_FUNC(PushThis)
{
	out.push(SELF());
}

EMIT_FUNC(Jump)
{
	assert(in.ArgInt[0] > 0);
	out.jmpLabel(LabelType(&context, in.Args[0]));
}

EMIT_FUNC(JumpIfTrue)
{
	assert(in.ArgInt[0] > 0);
	out.pop(EAX);
	out.cmp(EAX, 0);
	out.jmpLabel(LabelType(&context, in.Args[0]), JNE);
}

EMIT_FUNC(JumpIfFalse)
{
	assert(in.ArgInt[0] > 0);
	out.pop(EAX);
	out.cmp(EAX, 0);
	out.jmpLabel(LabelType(&context, in.Args[0]), JE);
}

EMIT_FUNC(PushLocal)
{
	LVAR(variable, 0);

	if(variable->isArgument())
		throw std::runtime_error("internal error");

	unsigned offset = variable->Offset;
	unsigned size = variable->Type.sizeOfAligned();
	bool isRef = variable->Type.IsRef;
	bool isValue = variable->Type.isValue();

	// get address of variable
	if(isRef)
		out.mov(LOCAL(offset), EAX);
	else
		out.lea(LOCAL(offset), EAX);

	// push address of variable
	if(isValue)
		out.push(EAX);
	else
		out.push(MemType(EAX));
}

EMIT_FUNC(PopLocal)
{
	LVAR(variable, 0);

	if(variable->isArgument())
		throw std::runtime_error("internal error");

	unsigned offset = variable->Offset;
	unsigned size = variable->Type.sizeOfAligned();
	bool isRef = variable->Type.IsRef;

	// get address of variable
	if(isRef)
		out.mov(LOCAL(offset), EDI);
	else
		out.lea(LOCAL(offset), EDI);

	// copy data
	out.mov(ESP, ESI);
	out.math(ADD, ESP, size);
	out.mov(ECX, size/4);
	out.rep_movs();
}

EMIT_FUNC(PushArgument)
{
	LVAR(variable, 0);

	if(!variable->isArgument())
		throw std::runtime_error("internal error");

	unsigned offset = variable->Offset;
	unsigned size = variable->Type.sizeOfAligned();
	bool isRef = variable->Type.IsRef;
	bool isValue = variable->Type.isValue();

	// get address of variable
	if(isRef)
		out.mov(ARGS(-offset), EAX);
	else
		out.lea(ARGS(-offset), EAX);

	// push address of variable
	if(isValue)
		out.push(EAX);
	else
		out.push(MemType(EAX));
}

EMIT_FUNC(PopArgument)
{
	LVAR(variable, 0);

	if(!variable->isArgument())
		throw std::runtime_error("internal error");

	unsigned offset = variable->Offset;
	unsigned size = variable->Type.sizeOfAligned();
	bool isRef = variable->Type.IsRef;

	// get address of variable
	if(isRef)
		out.mov(ARGS(-offset), EDI);
	else
		out.lea(ARGS(-offset), EDI);

	// copy data
	out.mov(ESP, ESI);
	out.math(ADD, ESP, size);
	out.mov(ECX, size/4);
	out.rep_movs();
}

EMIT_FUNC(PushOffset)
{
	CVAR(variable, 0);
	TVAR(type, 1);

	if(variable->isStatic())
		throw std::runtime_error("internal error");
	variable->parentSizeOf(); // force to recalculate Offsets

	//assert(type->IsRef);

	unsigned offset = variable->Offset;
	unsigned size = variable->sizeOfAligned();

	out.math(ADD, MemType(ESP), offset);
}

EMIT_FUNC(PopOffset)
{
	CVAR(variable, 0);
	TVAR(type, 1);

	if(variable->isStatic())
		throw std::runtime_error("internal error");
	variable->parentSizeOf(); // force to recalculate Offsets

	//assert(type->IsRef);

	unsigned offset = variable->Offset;
	unsigned size = variable->sizeOf();

	out.pop(EDI);
	out.math(ADD, EDI, offset);
	out.mov(ESP, ESI);
	out.math(ADD, ESP, size);
	out.mov(ECX, size/4);
	out.rep_movs();
}

static void* __stdcall zeroMemory(void* data, unsigned size)
{
	memset(data, 0, size);
	return data;
}

static void* __stdcall allocMemory(unsigned size)
{
	void* data = new char[size];
	memset(data, 0, size);
	new(data) Object();
	return data;
}

EMIT_FUNC(CallConstructor)
{
	FVAR(function, 0);
	TVAR(type, 1);
	LVAR(variable, 2);

	if(function->isStaticFunction())
		throw std::runtime_error("construct can't be static");

	unsigned size = type->sizeOfAligned(true);

	if(type->isValue())
	{
		if(!variable)
			throw std::runtime_error("no local variable to hold data");

		out.addCustomFunction(zeroMemory);

		// zero memory
		out.push(size);
		out.lea(LOCAL(variable->Offset), EDI);
		out.push(EDI);
		out.call(LabelType(zeroMemory, 0));
	}
	else if(type->isObject())
	{
		out.addCustomFunction(allocMemory);

		// alloc memory
		out.push(size);
		out.call(LabelType(allocMemory, 0));
		out.mov(EAX, EDI);
	}
	else
	{
		throw std::runtime_error("invalid object type");
	}

	// Call constructor function
#ifdef _MSC_VER
	out.mov(EDI, ECX);
#else
	out.push(EDI);
#endif

	out.call(LabelType(function, 0));

#ifndef _MSC_VER
	out.math(ADD, ESP, function->argumentsSize() + 4);
#endif

	out.push(EDI);
}

EMIT_FUNC(Call)
{
	FVAR(function, 0);
	LVAR(variable, 1);

	unsigned argSize = function->argumentsSize();
	unsigned returnSize = function->returnSize();

	// pop this pointer
	if(!function->isStaticFunction())
		out.pop(ECX);

	// function returns Primitive data
	if(function->returnType().isPOD())
	{
		out.call(LabelType(function, 0));

		if(function->isStaticFunction())
			out.math(ADD, ESP, argSize);
#ifndef _MSC_VER
		else
			out.math(ADD, ESP, argSize + PtrSize);
#endif

		// return in value
		if(function->returnType().isValue())
		{		
			if(!variable)
				throw std::runtime_error("no local variable to hold data");

			switch(returnSize)
			{
			case 2*PtrSize:
				out.mov(EDX, LOCAL(variable->Offset+PtrSize));
			case PtrSize:
				out.mov(EAX, LOCAL(variable->Offset));
				out.lea(LOCAL(variable->Offset), EAX);
				out.push(EAX);
			case 0:
				break;

			default:
				assert(0);
			}
		}

		// return on stack
		else
		{
			switch(returnSize)
			{
			case 2*PtrSize:
				out.push(EDX);
			case PtrSize:
				out.push(EAX);
			case 0:
				break;

			default:
				assert(0);
			}
		}
	}
	// Pass hidden pointer to value
	else
	{
		if(!variable)
			throw std::runtime_error("no local variable to hold data");

		out.lea(LOCAL(variable->Offset), EAX);

		// push hidden pointer
		if(!function->isStaticFunction())
		{
			out.push(MemType(ESP));
			out.mov(EAX, MemType(ESP, PtrSize));
		}
		else
		{
			out.push(EAX);
		}

		// call function
		out.call(LabelType(function, 0));

		if(function->isStaticFunction())
			out.math(ADD, ESP, argSize + PtrSize);
#ifndef _MSC_VER
		else
			out.math(ADD, ESP, argSize + 2*PtrSize);
#endif

		// push pointer once again
		out.lea(LOCAL(variable->Offset), EAX);
		out.push(EAX);
	}
}

EMIT_FUNC(CallMethod)
{
	opCall(in, out, context);
}

static void __stdcall debugBreak(const char* name, const char** ebx)
{
	if(!strcmp(name, "Mark - EndFunc"))
	{
		name = name;
	}
	else if(strstr(name, "CallMethod") || strstr(name, "PushFunction"))
	{
		name = name;
	}
	else if(strstr(name, "CallConstructor"))
	{
		name = name;
	}
	const char **stack = &name+2;
	printf("... %s\n...  [esp*=%p] [ebx*=%p] [ret=%p] [offset=%i] ", 
		name, stack, ebx, stack[-2], 4*(ebx - stack));
	for(unsigned i = 0; i < 16; ++i)
		printf("[esp%i=%p] ", i, stack[i]);
	printf("\n\n");
}

void ScriptScriptFunction::jitEmit(JITCode& code)
{
	// prolog
	code.push(EBP);
	code.mov(ESP, EBP);
	code.push(EBX);
	code.push(ESI);
	code.push(EDI);

	// push this
#ifdef _MSC_VER
	if(!isStaticFunction())
		code.push(ECX); // [EBP-16]
#endif

	if(VariableStackUsage)
		code.math(SUB, ESP, VariableStackUsage);

	code.mov(ESP, EBX);

	code.mov(ESP, EDI);
	code.mov(EAX, 01L);
	code.mov(ECX, VariableStackUsage / 4);
	code.rep_stos();
#ifdef BEGIN_FUNC
	code.push(EBX);
	code.push((void*)"Mark - BeginFunc");
	code.mov(EAX, debugBreak);
	code.call(EAX);
	opMark(OpCode(), code, *this);
#endif
	code.align(ALIGN);

	for(unsigned i = 0; i < OpList.size(); ++i)
	{
#ifdef DEBUG_BREAK
		code.addCustomFunction(debugBreak);
		code.push(EBX);
		code.push((void*)OpDesc[i].c_str());
		code.call(LabelType(debugBreak, 0));
#endif

		OpCode& in = OpList[i];
		assert(in.Type < OpCode::OpCount);

		OpCode::OpDesc& desc = OpCode::OpList[in.Type];
		assert(desc.EmitFunc);

		code.align(ALIGN);
		desc.EmitFunc(in, code, *this);
		code.align(ALIGN);
	}

	// epilog
	code.label(LabelType(this, EndLabel));

#ifdef END_FUNC
	code.push(EBX);
	code.push((void*)"Mark - EndFunc");
	code.mov(EAX, debugBreak);
	code.call(EAX);

	opMark(OpCode(), code, *this);
#endif

	if(VariableStackUsage)
		code.math(ADD, ESP, VariableStackUsage);

	// pop saved this
#ifdef _MSC_VER
	if(!isStaticFunction())
		code.pop(EDI);
#endif
	code.pop(EDI);
	code.pop(ESI);
	code.pop(EBX);
	code.pop(EBP);

#ifdef _MSC_VER
	code.ret(
		(returnType().isPOD() ? 0 : PtrSize) + 
		argumentsSize()
		);
#else
	code.ret();
#endif
	code.align(32);
}

OpCode::OpDesc OpCode::OpList[OpCode::OpCount] = {
	{"Label", "L", opLabel},
	{"Mark", "", opMark},
	{"ZeroLocal", "V", opZeroLocal},
	{"Pop", "T", opPop},
	{"Return", "T", opReturn},
	{"DeRef", "T", opDeRef},
	{"Call", "FV", opCall},
	{"CallMethod", "FV", opCallMethod},
	{"PushFunction", "F", opPushFunction},
	{"PushString", "SX", opPushString},
	{"PushValueType", "T  ", opPushValueType},
	{"PushTrue", "", opPushTrue},
	{"PushFalse", "", opPushFalse},
	{"PushNull", "", opPushNull},
	{"PushLocal", "V", opPushLocal},
	{"PushArgument", "V", opPushArgument},
	{"PushOffset", "CT", opPushOffset},
	{"PushThis", "", opPushThis},
	{"PushStatic", "G", NULL},
	{"PopLocal", "V", opPopLocal},
	{"PopArgument", "V", opPopArgument},
	{"PopStatic", "G", NULL},
	{"PopOffset", "CT", opPopOffset},
	{"Jump", "L", opJump},
	{"JumpIfTrue", "L", opJumpIfTrue},
	{"JumpIfFalse", "L", opJumpIfFalse},
	{"CallConstructor", "FTV", opCallConstructor}
};
