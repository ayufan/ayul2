%{
	
//------------------------------------//
//
// grammar.cxx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-3-31
//
//------------------------------------//

#include "StdAfx.hpp"
#include "Lexer.hpp"

#ifdef _MSC_VER
// Treat some previous errors as warnings
#pragma warning(disable : 4244 4267)
#endif

#define YYERROR_VERBOSE
//#define YYDEBUG			1
#define YYPARSE_PARAM	data

#define malloc malloc
#define free free

int yylex(void);
void yyerror(const char *message);

SyntaxLine asCurrentLine;

%}

%token					T_MODULE T_CLASS T_SCOPE T_PUBLIC T_PRIVATE T_PROTECTED T_STATIC T_GET T_SET T_STRUCT T_INTERFACE
%token					T_THIS T_FOREACH T_IS T_AS T_IF T_ELSE T_WHILE T_DO T_FOR T_RETURN T_CONTINUE T_BREAK T_NULL
%token					T_BEGIN T_END T_OPEN T_CLOSE T_S_OPEN T_S_CLOSE T_STATEMENT T_ACCESSOR T_OBJECT T_IMPLEMENTS T_EXTENDS
%left 					T_EQUAL T_EQUALITY T_NOT_EQUAL T_LESS_EQUAL T_GREATER_EQUAL T_LESS T_GREATER T_NEGATION T_AND T_OR
%left						T_ADDITION T_SUBTRACTION T_MULTIPLICATION T_DIVIDE T_MODULUS T_BXOR T_BOR T_BAND
%left						T_SEPARATOR T_TEST T_OTHERWISE
%right					T_ASSIGNMENT T_ADDITIVE_ASSIGNMENT T_SUBTRACTIVE_ASSIGNMENT T_MULTIPLY_ASSIGNMENT T_DIVIDE_ASSIGNMENT T_MODULUS_ASSIGNMENT T_BXOR_ASSIGNMENT T_BAND_ASSIGNMENT T_BOR_ASSIGNMENT
%token					T_INCREMENTATION T_DECREMENTATION T_REF T_TRY T_CATCH T_FINALLY
%token					T_DELEGATE T_EVENT T_VOID T_TYPEDEF T_NEW T_THROW T_SWITCH T_CASE T_DEFAULT
%token	<String>			T_IDENTIFIER
%token	<Integer>			T_INTEGER_VALUE
%token	<Boolean>			T_BOOLEAN_VALUE
%token	<Float>			T_FLOAT_VALUE
%token	<String>			T_STRING_VALUE

%type <Type()> type array_type base_type
%type <Value()> literal_value value
%type <Expression()> base_expression cast_expression expression optional_expression postfix_expression unary_expression
%type <Expression()> operator3_expression operator2_expression operator_expression compare5_expression 
%type <Expression()> compare4_expression compare3_expression compare2_expression compare_expression 
%type <Expression()> assignment_expression
%type <ExpressionList()> optional_expression_list expression_list
%type <Statement()> if_optional_else_statement statement optional_get optional_set optional_get_decl optional_set_decl
%type <StatementList()> statement_list
%type <Boolean> optional_static
%type <Visibility> optional_visiblity
%type <Variable()> argument default_argument identifier
%type <VariableList()> identifier_list optional_default_argument_list default_argument_list argument_list optional_argument_list
%type <NamedObject()> property_decl property method_decl method
%type <NamedObjectList()> member_list variable member_decl_list
%type <ModuleObject()> typedef class
//%type <ModuleObjectList()> main_list
%type <String> implements optional_extends
%type <StringList> implements_list optional_implements_list
%type <Statement()> optional_finally
%type <CatchStatement()> catch 
%type <CatchStatementList()> optional_catch_list catch_list 
%type <CaseStatement()> case_label
%type <CaseStatementList()> case_label_list
%type <Context()> main

//bison configuration
%start		main

%%

base_type : T_IDENTIFIER { $$ = new SyntaxNameType($1); }
	| base_type T_SCOPE T_IDENTIFIER { $$ = new SyntaxOfType($1, $3); }
	
array_type : base_type { $$ = $1; }
	| T_OBJECT { $$ = new SyntaxObjectType(); }
	| array_type T_S_OPEN T_S_CLOSE { $$ = new SyntaxArrayType($1); }

type : T_EVENT base_type { $$ = new SyntaxEventType($2); }
	| array_type { $$ = $1; }
	| T_VOID { $$ = NULL; }
	
value : T_INTEGER_VALUE { $$ = new SyntaxIntValue($1); }
	| T_BOOLEAN_VALUE { $$ = new SyntaxBooleanValue($1!=0); }
	| T_FLOAT_VALUE { $$ = new SyntaxFloatValue($1); }
	| T_STRING_VALUE { $$ = new SyntaxStringValue($1); }
	| T_NULL { $$ = new SyntaxNullValue(); }
	| T_NEW base_type T_OPEN optional_expression_list T_CLOSE { $$ = new SyntaxNewObjectValue($2, $4); }
	| T_NEW array_type T_BEGIN optional_expression_list T_END { $$ = new SyntaxNewArrayValue($2, $4); }

identifier : T_IDENTIFIER { $$ = new SyntaxVariable($1); }
	| T_IDENTIFIER T_ASSIGNMENT value { $$ = new SyntaxVariable($1, NULL, $3); }
	
identifier_list : identifier_list T_SEPARATOR identifier { $$.swap($1); $$.push_back($3); }
	| identifier { $$.clear(); $$.push_back($1); }

default_argument : array_type T_IDENTIFIER T_ASSIGNMENT value { $$ = new SyntaxVariable($2, $1, $4); }

default_argument_list : default_argument_list T_SEPARATOR default_argument { $$.swap($1); $$.push_back($3); }
	| default_argument { $$.clear(); $$.push_back($1); }

optional_default_argument_list : T_SEPARATOR default_argument_list { $$.swap($2); }
	| { $$.clear(); }

argument : array_type T_IDENTIFIER { $$ = new SyntaxVariable($2, $1); }
	| array_type T_BAND T_IDENTIFIER { $$ = new SyntaxVariable($3, new SyntaxRefType($1)); }
	| T_REF array_type T_IDENTIFIER { $$ = new SyntaxVariable($3, new SyntaxRefType($2)); }

argument_list : argument_list T_SEPARATOR argument { $$.swap($1); $$.push_back($3); }
	| argument { $$.clear(); $$.push_back($1); }
	
optional_argument_list : argument_list optional_default_argument_list { $$.swap($1); $$.insert($$.end(), $2.begin(), $2.end()); $2.clear(); }
	| default_argument_list { $$.swap($1); }
	| { $$.clear(); }
	
literal_value:
	value { $$ = $1; }
	| T_IDENTIFIER { $$ = new SyntaxIdentifierValue($1); }
	| T_THIS { $$ = new SyntaxThisValue(); }

expression_list : expression_list T_SEPARATOR expression { $$.swap($1); $$.push_back($3); }
	| expression { $$.clear(); $$.push_back($1); }

optional_expression_list : expression_list { $$.swap($1); }
	| { $$.clear(); }

//call_expression:
	//
	//

base_expression:
	literal_value { $$ = new SyntaxValueExpression($1); } 
	| T_OPEN expression T_CLOSE { $$ = $2; }
	| T_IDENTIFIER T_OPEN optional_expression_list T_CLOSE { $$ = new SyntaxCallMethod(NULL, $1, $3); }
	| base_expression T_ACCESSOR T_IDENTIFIER T_OPEN optional_expression_list T_CLOSE { $$ = new SyntaxCallMethod($1, $3, $5); }
	| base_expression T_ACCESSOR T_IDENTIFIER { $$ = new SyntaxAccessorExpression($1, $3); }
	//| base_expression T_OPEN optional_expression_list T_CLOSE { $$ = new SyntaxCallExpression($1, $3); }
	| base_expression T_S_OPEN optional_expression T_S_CLOSE { $$ = new SyntaxIndexExpression($1, $3); }

cast_expression:
	base_expression { $$ = $1; }
	| T_OPEN array_type T_CLOSE base_expression { $$ = new SyntaxCastExpression($2, $4); }
	| base_expression T_AS array_type { $$ = new SyntaxAsExpression($3, $1); }
	| base_expression T_IS array_type { $$ = new SyntaxIsExpression($3, $1); }

postfix_expression:
	cast_expression { $$ = $1; }
	| postfix_expression T_INCREMENTATION { $$ = new SyntaxPostfixExpression($1, spInc); }
	| postfix_expression T_DECREMENTATION { $$ = new SyntaxPostfixExpression($1, spDec); }
	
unary_expression:
	postfix_expression { $$ = $1; }
	| T_INCREMENTATION unary_expression { $$ = new SyntaxUnaryExpression($2, suInc); }
	| T_DECREMENTATION unary_expression { $$ = new SyntaxUnaryExpression($2, suDec); }
	| T_ADDITION unary_expression { $$ = new SyntaxUnaryExpression($2, suPlus);}
	| T_SUBTRACTION unary_expression { $$ = new SyntaxUnaryExpression($2, suMinus); } 
	
operator3_expression:
	unary_expression { $$ = $1; }
	| operator3_expression T_MODULUS operator3_expression { $$ = new SyntaxOperatorExpression($1, $3, soModulus); }
	| operator3_expression T_BXOR operator3_expression { $$ = new SyntaxOperatorExpression($1, $3, soBXor); }
	| operator3_expression T_BOR operator3_expression { $$ = new SyntaxOperatorExpression($1, $3, soBOr); }
	| operator3_expression T_BAND operator3_expression { $$ = new SyntaxOperatorExpression($1, $3, soBAnd); }

operator2_expression:
	operator3_expression { $$ = $1; }
	| operator2_expression T_MULTIPLICATION operator2_expression { $$ = new SyntaxOperatorExpression($1, $3, soMult); }
	| operator2_expression T_DIVIDE operator2_expression { $$ = new SyntaxOperatorExpression($1, $3, soDiv); }

operator_expression:
	operator2_expression { $$ = $1; }
	| operator_expression T_ADDITION operator_expression { $$ = new SyntaxOperatorExpression($1, $3, soAdd); }
	| operator_expression T_SUBTRACTION operator_expression { $$ = new SyntaxOperatorExpression($1, $3, soSub); }
	
compare5_expression:
	operator_expression { $$ = $1; }
	| compare5_expression T_EQUAL compare5_expression { $$ = new SyntaxOperatorExpression($1, $3, scEqual); }
	| compare5_expression T_NOT_EQUAL compare5_expression { $$ = new SyntaxOperatorExpression($1, $3, scNotEqual); }
	| compare5_expression T_LESS_EQUAL compare5_expression { $$ = new SyntaxOperatorExpression($1, $3, scLessEqual); }
	| compare5_expression T_GREATER_EQUAL compare5_expression { $$ = new SyntaxOperatorExpression($1, $3, scGreaterEqual); }
	| compare5_expression T_LESS compare5_expression { $$ = new SyntaxOperatorExpression($1, $3, scLess); }
	| compare5_expression T_GREATER compare5_expression { $$ = new SyntaxOperatorExpression($1, $3, scGreater); }

compare4_expression:
	compare5_expression { $$ = $1; }
	| T_NEGATION compare4_expression { $$ = new SyntaxUnaryExpression($2, suMinus); }

compare3_expression:
	compare4_expression { $$ = $1;  }
	| compare3_expression T_AND compare3_expression { $$ = new SyntaxAndExpression($1, $3); }

compare2_expression:
	compare3_expression { $$ = $1; }
	| compare2_expression T_OR compare2_expression { $$ = new SyntaxOrExpression($1, $3); }

compare_expression:
	compare2_expression { $$ = $1; }
	| compare_expression T_TEST compare_expression T_OTHERWISE compare_expression { $$ = new SyntaxTestExpression($1, $3, $5); }
	
assignment_expression:
	compare_expression {$$ = $1; }
	| assignment_expression T_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saNormal); }
	| assignment_expression T_ADDITIVE_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saAdd); }
	| assignment_expression T_SUBTRACTIVE_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saSub); }
	| assignment_expression T_MULTIPLY_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saMult); }
	| assignment_expression T_DIVIDE_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saDiv); }
	| assignment_expression T_MODULUS_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saModulus); }
	| assignment_expression T_BXOR_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saBXor); }
	| assignment_expression T_BOR_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saBOr); }
	| assignment_expression T_BAND_ASSIGNMENT assignment_expression { $$ = new SyntaxAssignmentExpression($1, $3, saBAnd); }
	
expression : assignment_expression { $$ = $1; }
	
optional_expression : expression { $$ = $1; }
	| { $$ = NULL; }
	
if_optional_else_statement : T_ELSE statement { $$ = $2; }
	| { $$ = NULL; }

optional_finally : T_FINALLY T_BEGIN statement_list T_END { $$ = new SyntaxStatementList($3); }
	| { $$ = NULL; }

case_label : T_CASE T_INTEGER_VALUE T_OTHERWISE statement_list { $$ = new SyntaxValueCaseStatement($2, new SyntaxStatementList($4)); }
	| T_DEFAULT T_OTHERWISE statement_list { $$ = new SyntaxDefaultCaseStatement(new SyntaxStatementList($3)); }

case_label_list : case_label_list case_label { $$.swap($1); $$.push_back($2); }
	| case_label { $$.clear(); $$.push_back($1); }

catch : T_CATCH T_OPEN type T_IDENTIFIER T_CLOSE T_BEGIN statement_list T_END { $$ = new SyntaxCatchStatement(new SyntaxVariable($4, $3), new SyntaxStatementList($7)); }

catch_list : catch_list catch { $$.swap($1); $$.push_back($2); }
	| catch { $$.clear(); $$.reserve(6); $$.push_back($1); }

optional_catch_list : catch_list T_CATCH T_OPEN T_CLOSE T_BEGIN statement_list T_END { $$.swap($1); $$.push_back(new SyntaxCatchStatement(NULL, new SyntaxStatementList($6))); }
	| catch_list { $$.swap($1); }
	| { $$.clear(); }

statement:
	optional_expression T_STATEMENT { $$ = $1; }
	| T_RETURN optional_expression T_STATEMENT { $$ = new SyntaxReturnStatement($2); }
	| T_FOREACH T_OPEN expression T_AS T_IDENTIFIER T_CLOSE statement { $$ = new SyntaxForeachStatement($3, $5, $7); }
	| T_FOR T_OPEN optional_expression T_STATEMENT optional_expression T_STATEMENT optional_expression T_CLOSE statement { $$ = new SyntaxForStatement($3, $5, $7, $9); }
	| T_DO statement T_WHILE T_OPEN expression T_CLOSE T_STATEMENT { $$ = new SyntaxDoWhileStatement($5, $2); }
	| T_WHILE T_OPEN expression T_CLOSE statement  { $$ = new SyntaxWhileStatement($3, $5); }
	| T_IF T_OPEN expression T_CLOSE statement if_optional_else_statement { $$ = new SyntaxIfStatement($3, $5, $6); }
	| T_BEGIN statement_list T_END { $$ = new SyntaxStatementList($2); }
	| T_CONTINUE T_STATEMENT { $$ = new SyntaxContinueStatement(); }
	| T_BREAK T_STATEMENT { $$ = new SyntaxBreakStatement(); }
	| T_THROW optional_expression T_STATEMENT { $$ = new SyntaxThrowStatement($2); }
	| T_TRY T_BEGIN statement_list T_END optional_catch_list optional_finally { $$ = new SyntaxTryCatchStatement(new SyntaxStatementList($3), $5, $6); }
	| T_SWITCH T_OPEN expression T_CLOSE T_BEGIN case_label_list T_END { $$ = new SyntaxSwitchStatement($3, $6); }

statement_list : statement_list statement { $$.swap($1); $$.push_back($2); }
	| statement_list type identifier_list T_STATEMENT { 
		$$.swap($1); 
		$$.reserve($$.size() + $3.size());
		for(unsigned i = 0; i < $3.size(); ++i) {
			$3[i]->Type = $2;
			$$.push_back($3[i]);
		}
	}
	| { $$.clear(); }

optional_statement : T_STATEMENT {}
	| {}

optional_static : T_STATIC { $$ = 1; }
	| { $$ = 0; }
	
optional_visiblity : T_PUBLIC { $$ = svPublic; }
	| T_PROTECTED { $$ = svProtected; }
	| T_PRIVATE { $$ = svPrivate; }
	| { $$ = svPrivate; }

method_decl : type T_IDENTIFIER T_OPEN optional_argument_list T_CLOSE T_STATEMENT { $$ = new SyntaxClassMethod(SyntaxMethod($1, $2, $4), false, svPublic); }
	
method : optional_visiblity optional_static type T_IDENTIFIER T_OPEN optional_argument_list T_CLOSE T_BEGIN statement_list T_END { $$ = new SyntaxClassMethod(SyntaxMethod($3, $4, $6, new SyntaxStatementList($9)), $2!=0, $1); }
	| optional_visiblity optional_static T_IDENTIFIER T_OPEN optional_argument_list T_CLOSE T_BEGIN statement_list T_END { $$ = new SyntaxClassConstructor(SyntaxMethod(NULL, $3, $5, new SyntaxConstructorStatementList($8)), $2!=0, $1); }

variable : optional_visiblity optional_static type identifier_list T_STATEMENT {
			$$.clear();
			$$.reserve($4.size());
			for(unsigned i = 0; i < $4.size(); ++i) {
				$$.push_back(new SyntaxClassVariable($4[i]->Name, $3, $4[i]->Value, $2!=0, $1));
			}
		}

optional_get : T_GET T_BEGIN statement_list T_END { $$ = new SyntaxStatementList($3); }
	| { $$ = NULL; }

optional_get_decl : T_GET T_STATEMENT { $$ = new SyntaxNopStatement(); }
	| { $$ = NULL; }

optional_set : T_SET T_BEGIN statement_list T_END { $$ = new SyntaxStatementList($3); }
	| { $$ = NULL; }

optional_set_decl : T_SET T_STATEMENT { $$ = new SyntaxNopStatement(); }
	| { $$ = NULL; }

property : optional_visiblity optional_static type T_IDENTIFIER T_BEGIN optional_get optional_set T_END { $$ = new SyntaxClassProperty(SyntaxProperty($3, $4, $6, $7), $2, $1); }
	
property_decl : type T_IDENTIFIER T_BEGIN optional_get_decl optional_set_decl T_END { $$ = new SyntaxClassProperty(SyntaxProperty($1, $2, $4, $5), false, svPublic); }
	
typedef : optional_visiblity T_DELEGATE type T_IDENTIFIER T_OPEN optional_argument_list T_CLOSE T_STATEMENT { $$ = new SyntaxDelegate($3, $4, $1, $6); }

member_list : member_list method { $$.swap($1); $$.push_back($2); }
	| member_list variable { $$.swap($1); $$ += $2; }
	| member_list property { $$.swap($1); $$.push_back($2); }
	| member_list typedef { $$.swap($1); $$.push_back($2); }
	| member_list class { $$.swap($1); $$.push_back($2); }
	| { $$.clear(); }

optional_extends : T_EXTENDS T_IDENTIFIER { $$ = $2; }
	| { $$.clear(); }

implements : T_IMPLEMENTS T_IDENTIFIER { $$ = $2; }

implements_list : implements_list implements { $$.swap($1); $$.push_back($2); }
	| implements { $$.clear(); $$.push_back($1); }

optional_implements_list : implements_list { $$.swap($1); }
	| { $$.clear(); }

member_decl_list : member_decl_list method_decl { $$.swap($1); $$.push_back($2); }
	| member_decl_list property_decl { $$.swap($1); $$.push_back($2); }
	| member_decl_list typedef { $$.swap($1); $$.push_back($2); }
	| { $$.clear(); }
	
class: optional_visiblity T_CLASS T_IDENTIFIER optional_extends optional_implements_list T_BEGIN member_list T_END optional_statement { 
		SyntaxClass* class_ = new SyntaxClass($3);
		class_->Extends = $4;
		class_->Implements.swap($5);
		class_->Objects.swap($7);
		$$ = class_;
	}
	| optional_visiblity T_STATIC T_CLASS T_IDENTIFIER T_BEGIN member_list T_END optional_statement { 
		SyntaxClass* class_ = new SyntaxClass($4);
		class_->Objects.swap($6);
		class_->IsStatic = true;
		$$ = class_;
	}
	| T_STRUCT T_IDENTIFIER T_BEGIN member_list T_END optional_statement { 
		SyntaxValueType* valueType = new SyntaxValueType($2);
		valueType->Objects.swap($4);
		$$ = valueType;
	}
	| T_INTERFACE T_IDENTIFIER optional_implements_list T_BEGIN member_decl_list T_END optional_statement { 
		SyntaxInterface* interface_ = new SyntaxInterface($2);
		interface_->Implements.swap($3);
		interface_->Objects.swap($5); 
		$$ = interface_;
	}

main: member_list 
			{ 
				intrusive_ptr<SyntaxContext>* context = (intrusive_ptr<SyntaxContext>*)data;
				if(context) {
					context[0] = new SyntaxContext();
					context[0]->Objects = $1;
				}
			}

%%
