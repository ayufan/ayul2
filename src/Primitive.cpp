#include "StdAfx.hpp"

BEGIN_VALUE_TYPE(Int32) 
{
	CLASS_METHOD(toString);

	CLASS_OPERATOR2(+, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(%, ThisClass, ThisClass, ThisClass);

	CLASS_ASSIGNMENT(=, ThisClass);
	CLASS_ASSIGNMENT(+=, ThisClass);
	CLASS_ASSIGNMENT(-=, ThisClass);
	CLASS_ASSIGNMENT(*=, ThisClass);
	CLASS_ASSIGNMENT(/=, ThisClass);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>, Boolean, ThisClass, ThisClass);

	CLASS_CAST(Float32);
	CLASS_CAST(Float64);
	CLASS_CAST(Int64);
}

BEGIN_VALUE_TYPE(Int64) 
{
	CLASS_METHOD(toString);

	CLASS_OPERATOR2(+, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(%, ThisClass, ThisClass, ThisClass);

	CLASS_ASSIGNMENT(=, ThisClass);
	CLASS_ASSIGNMENT(+=, ThisClass);
	CLASS_ASSIGNMENT(-=, ThisClass);
	CLASS_ASSIGNMENT(*=, ThisClass);
	CLASS_ASSIGNMENT(/=, ThisClass);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>, Boolean, ThisClass, ThisClass);

	CLASS_CAST(Int32);
	CLASS_CAST(Float32);
	CLASS_CAST(Float64);
}

BEGIN_VALUE_TYPE(Float32) 
{
	CLASS_METHOD(toString);

	CLASS_OPERATOR2(+, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, ThisClass);

	CLASS_ASSIGNMENT(=, ThisClass);
	CLASS_ASSIGNMENT(+=, ThisClass);
	CLASS_ASSIGNMENT(-=, ThisClass);
	CLASS_ASSIGNMENT(*=, ThisClass);
	CLASS_ASSIGNMENT(/=, ThisClass);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>, Boolean, ThisClass, ThisClass);

	CLASS_CAST(Int32);
}

BEGIN_VALUE_TYPE(Float64)
{
	CLASS_METHOD(toString);

	CLASS_OPERATOR2(+, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(-, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(*, ThisClass, ThisClass, ThisClass);
	CLASS_OPERATOR2(/, ThisClass, ThisClass, ThisClass);

	CLASS_ASSIGNMENT(=, ThisClass);
	CLASS_ASSIGNMENT(+=, ThisClass);
	CLASS_ASSIGNMENT(-=, ThisClass);
	CLASS_ASSIGNMENT(*=, ThisClass);
	CLASS_ASSIGNMENT(/=, ThisClass);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(<, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>=, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(>, Boolean, ThisClass, ThisClass);

	CLASS_CAST(Int32);
	CLASS_CAST(Int64);
	CLASS_CAST(Float32);
}

BEGIN_VALUE_TYPE(Boolean) 
{
	CLASS_METHOD(toString);

	CLASS_ASSIGNMENT(=, ThisClass);

	CLASS_OPERATOR2(==, Boolean, ThisClass, ThisClass);
	CLASS_OPERATOR2(!=, Boolean, ThisClass, ThisClass);

	CLASS_CAST(Int32);
	CLASS_CAST(Int64);

	CLASS_OPERATOR1(!, Boolean, Boolean);
}

String* Boolean::toString() const
{
	static String* trueString = String::allocString("true");
	static String* falseString = String::allocString("false");
	return Value ? trueString : falseString;
}

void WriteLine(String* line) 
{
	if(!line)
		return;
	puts(line->Value);
}

void WriteLineFloat(Float32 v) 
{
	WriteLine(v.toString());
}

void System(String* line) 
{
	if(!line)
		return;
	system(line->Value);
}

BEGIN_MODULE(Core) 
{
	CLASS_OBJECT(Int32);
	CLASS_OBJECT(Int64);
	CLASS_OBJECT(Float32);
	CLASS_OBJECT(Float64);
	CLASS_OBJECT(Boolean);
	CLASS_OBJECT(String);

	CLASS_OBJECT(Vec2);
	CLASS_OBJECT(Vec3);
	CLASS_OBJECT(Vec4);
	CLASS_OBJECT(MathLib);

	CLASS_OBJECT(Object);

	MODULE_FUNCTION(WriteLine, WriteLine);
	MODULE_FUNCTION(WriteLineFloat, WriteLineFloat);
	MODULE_FUNCTION(System, System);
}

