#ifndef Primitive_h__
#define Primitive_h__

#include "String.hpp"

struct Boolean {
	typedef bool Type;
	DECLARE_VALUE_TYPE(Boolean);
	unsigned Value;
	Boolean() : Value(0) {
	}
	Boolean(bool value) : Value(value) {
	}
	String* toString() const;
	operator bool () const { 
		return Value != 0; 
	}
};

struct Int32 {	
	typedef int Type;
	DECLARE_VALUE_TYPE(Int32);
	int Value;
	Int32() : Value(0) {
	}
	Int32(int value) : Value(value) {
	}
	String* toString() const {
		char buffer[12];
		return String::allocString(_itoa(Value, buffer, 10));
	}
	string toString2() const {
		char buffer[12];
		return _itoa(Value, buffer, 10);
	}
	operator int () const { 
		return Value; 
	}
	friend ThisClass& operator += (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator -= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator *= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator /= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
};

struct Int64 {
	typedef __int64 Type;
	DECLARE_VALUE_TYPE(Int64);
	__int64 Value;
	Int64() : Value(0) {
	}
	Int64(__int64 value) : Value(value) {
	}
	String* toString() const {
		char buffer[20];
		return String::allocString(_itoa(Value, buffer, 10));
	}
	operator __int64 () const { 
		return Value; 
	}
	friend ThisClass& operator += (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator -= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator *= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator /= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
};

struct Float32 {
	typedef float Type;
	DECLARE_VALUE_TYPE(Float32);
	float Value;
	Float32() : Value(0) {
	}
	Float32(float value) : Value(value) {
	}
	String* toString() const {
		char buffer[30];
		sprintf(buffer, "%f", Value);
		return String::allocString(buffer);
	}
	operator float () const { 
		return Value; 
	}
	friend ThisClass& operator += (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator -= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator *= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator /= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
};

struct Float64 {
	typedef double Type;
	DECLARE_VALUE_TYPE(Float64);
	double Value;
	Float64() : Value(0) {
	}
	Float64(double value) : Value(value) {
	}
	String* toString() const {
		char buffer[30];
		sprintf(buffer, "%lf", Value);
		return String::allocString(buffer);
	}
	operator double () const { 
		return Value; 
	}
	friend ThisClass& operator += (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator -= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator *= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
	friend ThisClass& operator /= (ThisClass& a, ThisClass b) {
		a.Value += b.Value; return a;
	}
};

DECLARE_MODULE(Core);

#endif // Primitive_h__
