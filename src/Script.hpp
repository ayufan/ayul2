#ifndef Script_h__
#define Script_h__

class ScriptObject;
class ScriptClass;
class ScriptInterface;
class ScriptValueType;
class ScriptModule;
class ScriptCode;

class JITCode;
class JITFunction;

#include "String.hpp"
#include "ScriptObject.hpp"
#include "ScriptType.hpp"
#include "ScriptValue.hpp"
#include "ScriptVisibility.hpp"
#include "ScriptVariable.hpp"
#include "ScriptFunction.hpp"
#include "ScriptObject.hpp"
#include "ScriptClassVariable.hpp"
#include "ScriptClassMethod.hpp"
#include "ScriptClassVariable.hpp"
#include "ScriptClassDelegate.hpp"
#include "ScriptClassProperty.hpp"
#include "ScriptClassCast.hpp"
#include "ScriptClass.hpp"
#include "ScriptValueType.hpp"
#include "ScriptInterface.hpp"
#include "ScriptModule.hpp"

DECLARE_MODULE(Script);

#endif // Script_h__
