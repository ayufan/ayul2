#ifndef Script_inl__
#define Script_inl__

template<typename Type>	ScriptType ScriptType::deduceSimpleType() {
	ScriptType type(Type::id$());
	return type;
}
template<> ScriptType ScriptType::deduceSimpleType<void>() {
	return ScriptType();
}
template<> ScriptType ScriptType::deduceSimpleType<int>() {
	ScriptType type(Int32::id$());
	type.IsPOD = true;
	return type;
}
template<> ScriptType ScriptType::deduceSimpleType<float>() {
	ScriptType type(Float32::id$());
	type.IsPOD = true;
	return type;
}
template<> ScriptType ScriptType::deduceSimpleType<bool>() {
	ScriptType type(Boolean::id$());
	type.IsPOD = true;
	return type;
}
template<> ScriptType ScriptType::deduceSimpleType<string>() {
	return ScriptType(String::id$());
}

#endif // Script_inl__
