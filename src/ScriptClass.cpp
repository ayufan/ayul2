#include "StdAfx.hpp"

ScriptClass::ScriptClass(const string& className) : ScriptObject(className), SizeOf(~0) {
}

ScriptClass::~ScriptClass() {
}

bool ScriptClass::isSubObject(const intrusive_ptr<ScriptObject>& object) {
	for(ScriptClass* extends = this; extends; extends = extends->Extends.get()) {
		if(extends == object.get())
			return true;
	}
	return false;
}

string ScriptClass::toString() const {
	if(GenerateCppString) {
		string extra;
		if(Extends)
			extra = " : public " + Extends->name();
		if(Implements.size()) {
			for(unsigned i = 0; i < Implements.size(); ++i) {
				if(extra.empty())
					extra = " : protected ";
				else
					extra += ", protected ";
				extra += Implements[i]->name();
			}
		}
		return "class " + ScriptObject::toString() + extra;
	}
	else {
		return "class " + ScriptObject::toString();
	}
}

intrusive_ptr<ScriptObject> ScriptClass::expandObject( const intrusive_ptr<ScriptObject>& object )
{
	intrusive_ptr<ScriptObject> objectFound = ScriptObject::expandObject(object);
	if(objectFound)
		return objectFound;

	if(Extends)
		if(objectFound = Extends->expandObject(object))
			return objectFound;

	for(unsigned i = 0; i < Implements.size(); ++i)
		if(objectFound = Implements[i]->expandObject(object))
			return objectFound;

	return NULL;
}

void ScriptClass::finalizeGlobalObjects()
{
	ScriptObject::finalizeGlobalObjects();

	// Force recalculate of SizeOf
	sizeOf();
}

void ScriptClass::finalizeLocalObjects()
{
	ScriptObject::finalizeLocalObjects();

	if(isNative())
		return; // Don't relock native objects!

	// Calculate object size
	unsigned totalSize = sizeOf();
	unsigned baseSize = sizeof(void*);
	if(Extends)
		baseSize = Extends->sizeOf();

	// Add interfaces
	baseSize += Implements.size() * sizeof(void*);

	for(unsigned i = 0; i < Objects.size(); ++i) {
		if(!Objects[i]->is<ScriptClassVariable>())
			continue;

		// Omit native variables
		ScriptClassVariable& variable = *Objects[i]->as$<ScriptClassVariable>();
		if(variable.isNative())
			continue;

		if(variable.IsStatic) {
			variable.Offset = ~0;
		}
		else {
			// Make offset :)
			variable.Offset = baseSize;
			baseSize += variable.sizeOf();
		}
	}

	// Check resulting size
	if(baseSize != totalSize)
		throw std::runtime_error("internal size error!");
}

unsigned ScriptClass::sizeOf() const
{
	// Recalculate SizeOf
	if(SizeOf == ~0) {
		// Calculate base sizeOf
		if(Extends)
			SizeOf = Extends->sizeOf();
		else
			SizeOf = sizeof(void*);

		// Add interfaces
		SizeOf += Implements.size() * sizeof(void*);

		// Calculate sizeOf of each member
		for(unsigned i = 0; i < Objects.size(); ++i) {
			if(!Objects[i]->isStatic())
				SizeOf += Objects[i]->sizeOf();
		}
	}
	return SizeOf;
}

bool ScriptClass::isStatic() const
{
	if(this == Object::id$())
		return false;
	return Extends == NULL;
}

bool ScriptClass::isObject() const
{
	if(isStatic())
		return false;
	return true;
}

BaseNativeScriptClass::BaseNativeScriptClass( const string& className, ScriptClass* baseClass, unsigned sizeOf ) : ScriptClass(className)
{
	scoped();

	SizeOf = sizeOf;

	if(this == Object::id$()) {
		if(baseClass != NULL)
			throw std::invalid_argument("baseClass");
	}

	Extends = baseClass;
}
