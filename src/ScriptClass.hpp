#ifndef ScriptClass_h__
#define ScriptClass_h__

class ScriptClass : public ScriptObject {
protected:
	mutable unsigned SizeOf;
	intrusive_ptr<ScriptClass> Extends;
	vector<intrusive_ptr<ScriptInterface> > Implements;

protected:
	void finalizeGlobalObjects();
	void finalizeLocalObjects();

public:
	ScriptClass(const string& className = string());
	~ScriptClass();

	string toString() const;

	intrusive_ptr<ScriptObject> expandObject(const intrusive_ptr<ScriptObject>& object);

	bool isSubObject(const intrusive_ptr<ScriptObject>& object);

	bool isStatic() const;

	bool isObject() const;

	unsigned sizeOf() const;

	friend class ScriptModule;
	friend class ScriptClassVariable;
	friend class ScriptClassMethod;
	friend class ScriptClassProperty;
	friend struct SyntaxClass;
};

class BaseNativeScriptClass : public ScriptClass {
public:
	BaseNativeScriptClass(const string& className, ScriptClass* baseClass, unsigned sizeOf);

	bool isNative() const { return true; }
};

template<typename Type>
class NativeScriptClass : public BaseNativeScriptClass {
public:
	typedef Type ThisClass;

	unsigned sizeOf() const {
		return sizeof(ThisClass);
	}

	NativeScriptClass();
};

#endif // ScriptClass_h__
