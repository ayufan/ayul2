#include "StdAfx.hpp"

ScriptClassCast::ScriptClassCast() : ScriptObject(OPERATOR_CAST)
{
}

ScriptClassCast::~ScriptClassCast()
{
}

string ScriptClassCast::toString() const
{
	return join(' ',
		Function ? Function->toString(GenerateCppString).c_str() : "",
		"operator", ToType.toString(GenerateCppString).c_str(), "(", 
		Parent ? Parent->name().c_str() : "", 
		");", 
		NULL);
}

const string& ScriptClassCast::name() const
{
	static const string name("!cast");
	return name;
}

bool ScriptClassCast::equals( const ScriptObject& otherObject ) const
{
	const ScriptClassCast* cast = dynamic_cast<const ScriptClassCast*>(&otherObject);
	if(cast)
		return ToType == cast->ToType;
	else
		return ScriptObject::equals(otherObject);
}
