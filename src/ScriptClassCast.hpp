#ifndef ScriptClassCast_h__
#define ScriptClassCast_h__

class ScriptClassCast : public ScriptObject, public ScriptVisibility {
protected:
	ScriptType ToType;
	intrusive_ptr<ScriptFunction> Function;

public:
	ScriptClassCast();
	~ScriptClassCast();

	string toString() const;

	const string& name() const;

	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}

	bool isStatic() const { return true; }

	const intrusive_ptr<ScriptFunction>& function() const { 
		return Function;
	}

	template<typename To, typename From>
	void setCast(To (__cdecl *function)(From)) {
		ToType = ScriptType::fromScriptTypeT(ScriptTypeT<To>());
		Function = new ScriptNativeFunction((void*)function, ToType, vector<ScriptType>(1, ScriptType::fromScriptTypeT(ScriptTypeT<From>())), this, ScriptFunction::Static);
	}

	bool equals(const ScriptObject& otherObject) const;

	friend class ScriptClass;
};

struct ScopedClassCast : ScriptClassCast {
public:
	ScopedClassCast(const ScriptType& toType)
	{
		ToType = toType;
		ToType.IsConst = false;
		ToType.IsRef = false;
		scoped();
	}
};

#endif // ScriptClassCast_h__