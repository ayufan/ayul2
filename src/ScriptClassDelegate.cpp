#include "StdAfx.hpp"

ScriptClassDelegate::ScriptClassDelegate( const string& delegateName ) : ScriptObject(delegateName)
{
}

string ScriptClassDelegate::toString() const
{
	string buffer = join(' ',
		ScriptVisibility::toString(GenerateCppString, Parent ? Parent->isValue() : false).c_str(),
		ReturnType.toString(GenerateCppString).c_str(),
		ScriptObject::toString().c_str(),
		"\x01(", NULL);

	for(unsigned i = 0; i < Arguments.size(); ++i) {
		if(i) buffer += ", ";
		buffer += Arguments[i].toString(GenerateCppString);
	}
	return buffer + ");";
}

unsigned ScriptClassDelegate::sizeOf() const
{
	return 2*sizeof(void*);
}

struct ScriptDelegateFunction : ScriptModuleFunction
{
	ScriptDelegateFunction(const ScriptType& returnType, const vector<ScriptVariable>& argumentList, ScriptObject* context)
	{
		ReturnType = returnType;
		ArgumentList = argumentList;
		Context = context;
		Type = Method;
	}

	void jitIT(const intrusive_ptr<ScriptObject>& object, JITCode& code)
	{
		// Save function address
		Module = object->module().get();
		assert(Module);
		CodeOffset = code.size();

		// Emit delegate function
		code.label(LabelType(this, 0));

		bool usesMovs = false;

		unsigned argSize = argumentsSize();
		if(!ReturnType.isPOD())
			argSize += sizeof(void*);

		code.push(EBP);
		code.mov(ESP, EBP);

#ifdef _MSC_VER
		const int DataOffset = 4;
#else
		const int DataOffset = 8;
#endif

		// Push arguments
		switch(argSize) {
		case 16:
			code.push(MemType(EBP, DataOffset + 16));
		case 12:
			code.push(MemType(EBP, DataOffset + 12));
		case 8:
			code.push(MemType(EBP, DataOffset + 8));
		case 4:
			code.push(MemType(EBP, DataOffset + 4));
		case 0:
			break;

		default:
			assert(~argSize & 3);
			code.push(EDI);
			code.push(ESI);

			code.lea(MemType(EBP, DataOffset), ESI);
			code.math(SUB, ESP, argSize);
			code.mov(ESP, EDI);
			code.mov(ECX, argSize / 4);
			code.rep_movs();
			usesMovs = true;
			break;
		}

#ifdef _MSC_VER
		code.mov(ECX, EAX);
#else
		code.mov(MemType(EBP, DataOffset), EAX); // get this pointer
#endif
		code.cmp(MemType(EAX, 4), 0L); // check if static function
		code.jmpLabel(LabelType(this, 1), JE);

		if(1) // with This
		{ 
			// call method
#ifdef _MSC_VER
			code.mov(MemType(EAX, 4), ECX);
#else
			code.push(MemType(EAX, 4));
#endif
			code.call(MemType(EAX));
#ifndef _MSC_VER
			code.math(ADD, ESP, argSize + 4);
#endif

			if(usesMovs)
			{
				code.pop(ESI);
				code.pop(EDI);
			}
		}

		code.pop(EBP);
#ifdef _MSC_VER
		code.ret(argSize);
#else
		code.ret();
#endif

		if(1) // no this
		{ 
			// call static method
			code.label(LabelType(this, 1));
			code.call(MemType(EAX));
			code.math(ADD, ESP, argSize);
			if(usesMovs)
			{
				code.pop(ESI);
				code.pop(EDI);
			}
		}
		code.pop(EBP);
#ifdef _MSC_VER
		code.ret(argSize);
#else
		code.ret();
#endif
		code.align(16);
	}
};

void ScriptClassDelegate::populateDelegate()
{
	// Generates delegate methods!
	intrusive_ptr<ScriptClassMethod> method(new ScriptClassMethod(CALLER_NAME));
	method->ReturnType = ReturnType;
	method->Arguments = Arguments;
	method->Function = new ScriptDelegateFunction(ReturnType, Arguments, this);
	addObject(method);
}
