#ifndef ScriptClassDelegate_h__
#define ScriptClassDelegate_h__

class ScriptClassDelegate : public ScriptObject, public ScriptVisibility {
protected:
	ScriptType ReturnType;
	vector<ScriptVariable> Arguments;

public:
	ScriptClassDelegate(const string& delegateName);

	const ScriptType& returnType() const { return ReturnType; }
	unsigned argumentCount() const { return Arguments.size(); }
	const ScriptVariable& argument(unsigned i) const { return Arguments.at(i); }

	void populateDelegate();

	string toString() const;

	bool isStaticFunction() const { return false; }

	bool isValue() const { return true; }

	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}

	unsigned sizeOf() const;

	friend class ScriptClass;
	friend struct SyntaxDelegate;
};



#endif // ScriptClassDelegate_h__