#include "StdAfx.hpp"

ScriptClassMethod::ScriptClassMethod(const string& methodName) : ScriptObject(methodName) {
}

ScriptClassMethod::~ScriptClassMethod() {
}

bool ScriptClassMethod::equals(const ScriptObject& otherObject) const {
	// Check if it's method
	const ScriptClassMethod* otherMethod = dynamic_cast<const ScriptClassMethod*>(&otherObject);
	if(otherMethod == NULL)
		return ScriptObject::equals(otherObject);

	// Check method name
	if(name() != otherMethod->name())
		return false;

	// Check arguments count
	if(Arguments.size() != otherMethod->Arguments.size())
		return false;

	// Check return type
	//if(otherMethod->ReturnType != ReturnType)
	//	return false;

	// Check each argument
	for(unsigned i = 0; i < Arguments.size(); ++i) {
		const ScriptVariable& argLeft = Arguments[i];
		const ScriptVariable& argRight = otherMethod->Arguments[i];

		// Check type
		if(argLeft.Type != argRight.Type)
			return false;
	}
	return true;
}

string ScriptClassMethod::toString() const {
	string buffer = join(' ',
		Function ? Function->toString(GenerateCppString).c_str() : "",	
		ScriptVisibility::toString(GenerateCppString, Parent ? Parent->isValue() : false).c_str(),
		ReturnType.toString(GenerateCppString).c_str(),
		ScriptObject::toString().c_str(),
		"\x01(", NULL);

	for(unsigned i = 0; i < Arguments.size(); ++i) {
		if(i) buffer += ", ";
		buffer += Arguments[i].toString(GenerateCppString);
	}
	return buffer + ");";
}

void ScriptClassMethod::finalizeCodeObjects(JITCode& code)
{
	if(isNative())
		return;

	if(Function)
	{
		Function->jitIT(this, code);
	}
}

ScopedClassOperator::ScopedClassOperator( SyntaxOperator operator$, const ScriptType& arg0, const ScriptType& arg1 ) : ScopedClassMethodAutoResolver(OPERATOR2_PREFIX)
{
	switch(operator$) {
		case soModulus:
			Name = OPERATOR2_PREFIX "%";
			break;

		case soBXor:
			Name = OPERATOR2_PREFIX "^";
			break;

		case soBAnd:
			Name = OPERATOR2_PREFIX "&";
			break;

		case soMult:
			Name = OPERATOR2_PREFIX "*";
			break;

		case soDiv:
			Name = OPERATOR2_PREFIX "/";
			break;

		case soAdd:
			Name = OPERATOR2_PREFIX "+";
			break;

		case soSub:
			Name = OPERATOR2_PREFIX "-";
			break;

		case scEqual:
			Name = OPERATOR2_PREFIX "==";
			break;

		case scNotEqual:
			Name = OPERATOR2_PREFIX "!=";
			break;

		case scLessEqual:
			Name = OPERATOR2_PREFIX "<=";
			break;

		case scLess:
			Name = OPERATOR2_PREFIX "<";
			break;

		case scGreaterEqual:
			Name = OPERATOR2_PREFIX ">=";
			break;

		case scGreater:
			Name = OPERATOR2_PREFIX ">";
			break;


		default:
			throw std::runtime_error("unknown operator");
	}
	Arguments.push_back(arg0);
	Arguments.push_back(arg1);
}

ScopedClassOperator::ScopedClassOperator( SyntaxAssignemnt operator$, const ScriptType& arg0, const ScriptType& arg1 ) : ScopedClassMethodAutoResolver(OPERATOR2_PREFIX)
{
	switch(operator$) {
		case saNormal:
			Name = OPERATOR2_PREFIX "=";
			break;

		case saModulus:
			Name = OPERATOR2_PREFIX "%=";
			break;

		case saBXor:
			Name = OPERATOR2_PREFIX "^=";
			break;

		case saBAnd:
			Name = OPERATOR2_PREFIX "&=";
			break;

		case saMult:
			Name = OPERATOR2_PREFIX "*=";
			break;

		case saDiv:
			Name = OPERATOR2_PREFIX "/=";
			break;

		case saAdd:
			Name = OPERATOR2_PREFIX "+=";
			break;

		case saSub:
			Name = OPERATOR2_PREFIX "-=";
			break;

		default:
			throw std::runtime_error("unknown operator");
	}

	Arguments.push_back(arg0);
	Arguments.push_back(arg1);
}

ScopedClassConstructor::ScopedClassConstructor( const vector<ScriptType>& argumentList ) : ScopedClassMethodAutoResolver(CTOR_NAME)
{
	Arguments.assign(argumentList.begin(), argumentList.end());
}

ScopedClassMethod::ScopedClassMethod( const string& name, const vector<ScriptType>& argumentList /*= vector<ScriptType>()*/ ) : ScopedClassMethodAutoResolver(name)
{
	Arguments.assign(argumentList.begin(), argumentList.end());
}

ScopedClassMethodAutoResolver::ScopedClassMethodAutoResolver( const string& methodName ) : ScriptClassMethod(methodName)
{
	scoped();
}

bool ScopedClassMethodAutoResolver::equals( const ScriptObject& otherObject ) const
{
	// Check if it's method
	const ScriptClassMethod* otherMethod = dynamic_cast<const ScriptClassMethod*>(&otherObject);
	if(otherMethod == NULL)
		return ScriptObject::equals(otherObject);

	// Check method name
	if(name() != otherMethod->name())
		return false;

	// Check arguments count
	if(argumentCount() != otherMethod->argumentCount())
		return false;

	// Check each argument
	for(unsigned i = 0; i < argumentCount(); ++i) {
		if(!argument(i).Type.canCastToType(otherMethod->argument(i)))
			return false;
	}
	return true;
}
