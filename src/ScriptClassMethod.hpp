#ifndef ScriptClassMethod_h__
#define ScriptClassMethod_h__

class ScriptClassMethod : public ScriptObject, public ScriptVisibility {
protected:
	ScriptType ReturnType;
	vector<ScriptVariable> Arguments;
	intrusive_ptr<ScriptFunction> Function;

protected:
	void finalizeCodeObjects(JITCode& code);

public:
	ScriptClassMethod(const string& methodName);
	~ScriptClassMethod();

	const ScriptType& returnType() const { return ReturnType; }
	unsigned argumentCount() const { return Arguments.size(); }
	const ScriptVariable& argument(unsigned i) const { return Arguments.at(i); }

	string toString() const;

	const intrusive_ptr<ScriptFunction> &function() const {
		return Function;
	}

	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}

	bool isStatic() const { return IsStatic; }

	bool equals(const ScriptObject& otherObject) const;

	template<typename RetType>
	void setMethod(RetType (__cdecl *function)()) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Function = new ScriptNativeFunction((void*)function, ReturnType, Arguments, this, ScriptFunction::Static);
	}
	template<typename RetType, typename Arg1>
	void setMethod(RetType (__cdecl *function)(Arg1)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Function = new ScriptNativeFunction((void*)function, ReturnType, Arguments, this, ScriptFunction::Static);
	}
	template<typename RetType, typename Arg1, typename Arg2>
	void setMethod(RetType (__cdecl *function)(Arg1, Arg2)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Function = new ScriptNativeFunction((void*)function, ReturnType, Arguments, this, ScriptFunction::Static);
	}
	template<typename RetType, typename Arg1, typename Arg2, typename Arg3>
	void setMethod(RetType (__cdecl *function)(Arg1, Arg2, Arg3)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Function = new ScriptNativeFunction((void*)function, ReturnType, Arguments, this, ScriptFunction::Static);
	}
	template<typename RetType, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
	void setMethod(RetType (__cdecl *function)(Arg1, Arg2, Arg3, Arg4)) {
		IsStatic = true;
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg4>()));
		Function = new ScriptNativeFunction((void*)function, ReturnType, Arguments, this, ScriptFunction::Static);
	}

	template<typename RetType, typename ThisClass>
	void setMethod(RetType (__thiscall ThisClass::*function)(), bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass>
	void setMethod(RetType (__thiscall ThisClass::*function)() const, bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1), bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1) const, bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1, Arg2), bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1, Arg2) const, bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1, Arg2, Arg3), bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1, Arg2, Arg3) const, bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1, Arg2, Arg3, Arg4), bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg4>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
	void setMethod(RetType (__thiscall ThisClass::*function)(Arg1, Arg2, Arg3, Arg4) const, bool isVirtual = false) {
		ReturnType = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		Arguments.clear();
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg1>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg2>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg3>()));
		Arguments.push_back(ScriptType::fromScriptTypeT(ScriptTypeT<Arg4>()));
		Function = new ScriptNativeFunction(*(void**)&function, ReturnType, Arguments, this, isVirtual ? ScriptFunction::VirtualMethod : ScriptFunction::Method);
	}

	friend class ScriptClass;
	friend class ScriptClassDelegate;
	friend struct SyntaxClassMethod;
};

struct ScopedClassMethodAutoResolver : ScriptClassMethod {
	ScopedClassMethodAutoResolver(const string& methodName);
	bool equals(const ScriptObject& otherObject) const;
};

struct ScopedClassOperator : ScopedClassMethodAutoResolver {
	ScopedClassOperator(SyntaxOperator operator$, const ScriptType& arg0, const ScriptType& arg1);
	ScopedClassOperator(SyntaxAssignemnt operator$, const ScriptType& arg0, const ScriptType& arg1);
};

struct ScopedClassConstructor : ScopedClassMethodAutoResolver {
	ScopedClassConstructor(const vector<ScriptType>& argumentList = vector<ScriptType>());
};

struct ScopedClassMethod : ScopedClassMethodAutoResolver {
	ScopedClassMethod(const string& name, const vector<ScriptType>& argumentList = vector<ScriptType>());
};

#endif // ScriptClassMethod_h__
