#include "StdAfx.hpp"

ScriptClassProperty::ScriptClassProperty(const string& propertyName) : ScriptObject(propertyName) {
}

ScriptClassProperty::~ScriptClassProperty() {
}

string ScriptClassProperty::toString() const {
	string buffer;
	if(GenerateCppString) {
		return join('\n',
			GetFunction ? join(' ',
			ScriptVisibility::toString(true, Parent ? Parent->isValue() : false).c_str(),
			Type.toString(true).c_str(),
			"get", 
			("\x01" + ScriptObject::toString()).c_str(), 
			"\x01()",
			"const;", NULL).c_str() : "",

			SetFunction ? join(' ',
			ScriptVisibility::toString(true, Parent ? Parent->isValue() : false).c_str(),
			"void",
			"set", 
			("\x01" + ScriptObject::toString()).c_str(), 
			"\x01(", 
			("\x01" + Type.toString(true)).c_str(), 
			"\x01)",
			"const;", NULL).c_str() : "",

			NULL
			);
	}
	else {
		return join(' ',
			ScriptVisibility::toString(GenerateCppString, Parent ? Parent->isValue() : false).c_str(),
			Type.toString(GenerateCppString).c_str(), 
			Name.c_str(),
			"{",
			GetFunction ? GetFunction->toString(GenerateCppString).c_str() : "", GetFunction ? "get;" : "",
			SetFunction ? SetFunction->toString(GenerateCppString).c_str() : "", SetFunction ? "get;" : "",
			"}", NULL);
	}
	return buffer;
}

void ScriptClassProperty::finalizeCodeObjects(JITCode& code)
{
	if(GetFunction)
	{
		GetFunction->jitIT(this, code);
	}

	if(SetFunction)
	{
		SetFunction->jitIT(this, code);
	}
}
