#ifndef ScriptClassProperty_h__
#define ScriptClassProperty_h__

class ScriptClassProperty : public ScriptObject, public ScriptVisibility {
	intrusive_ptr<ScriptFunction> GetFunction, SetFunction;
	ScriptType Type;

protected:
	void finalizeCodeObjects(JITCode& code);

public:
	ScriptClassProperty(const string& propertyName);
	~ScriptClassProperty();

	string toString() const;

	bool isStatic() const { return IsStatic; }

	const intrusive_ptr<ScriptFunction>& getFunction() const {
		return GetFunction;
	}

	const intrusive_ptr<ScriptFunction>& setFunction() const {
		return SetFunction;
	}

	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}

	template<typename RetType>
	void setFunction(RetType (__cdecl *function)()) {
		IsStatic = true;
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		GetFunction = new ScriptNativeFunction(*(void**)&function, Type, vector<ScriptType>(), this, ScriptFunction::Static);
	}
	template<typename RetType>
	void setSetFunction(void (__cdecl *function)(RetType)) {
		IsStatic = true;
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());	
		SetFunction = new ScriptNativeFunction(*(void**)&function, ScriptType(), vector<ScriptType>(Type), this, ScriptFunction::Static);
	}

	template<typename RetType, typename ThisClass>
	void setFunction(RetType (__thiscall ThisClass::*function)()) {
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());		
		GetFunction = new ScriptNativeFunction(*(void**)&function, Type, vector<ScriptType>(), this, ScriptFunction::Method);
	}
	template<typename RetType, typename ThisClass>
	void setSetFunction(void (__thiscall ThisClass::*function)(RetType)) {
		Type = ScriptType::fromScriptTypeT(ScriptTypeT<RetType>());
		SetFunction = new ScriptNativeFunction(*(void**)&function, ScriptType(), vector<ScriptType>(1, Type), this, ScriptFunction::Method);
	}

	friend class ScriptClass;
	friend struct SyntaxClassProperty;
};

#endif // ScriptClassProperty_h__
