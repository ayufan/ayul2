#include "StdAfx.hpp"

ScriptClassVariable::ScriptClassVariable(const string& variableName) : ScriptObject(variableName) {
	Offset = 0;
}

ScriptClassVariable::~ScriptClassVariable() {
}

string ScriptClassVariable::toString() const {
	return join(' ',
		ScriptVisibility::toString(GenerateCppString, Parent ? Parent->isValue() : false).c_str(),
		Type.toString(GenerateCppString).c_str(),
		Name.c_str(),
		"\x02=",
		GenerateCppString ? Value.toString().c_str() : "",
		"\x01;", NULL);
}

void ScriptClassVariable::finalizeLocalObjects()
{
	if(!IsStatic)
	{
		return;
	}

	Static.resize(sizeOf());
}