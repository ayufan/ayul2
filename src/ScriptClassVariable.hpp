#ifndef ScriptClassVariable_h__
#define ScriptClassVariable_h__

class ScriptClassVariable : public ScriptObject, public ScriptVisibility {
public:
	ScriptType Type;
	ScriptValue Value;
	unsigned Offset;
	string Static;
	vector<OpCode> InitializationList;

protected:
	void finalizeLocalObjects();

public:
	ScriptClassVariable(const string& methodName);
	~ScriptClassVariable();

	string toString() const;

	bool isStatic() const { return IsStatic; }

	SyntaxVisiblity visibility() const { 
		return Visiblity; 
	}

	unsigned sizeOf() const { 
		return Type.sizeOf();
	}

	unsigned sizeOfAligned() const { 
		return Type.sizeOfAligned();
	}

	friend class ScriptClass;
};

#endif // ScriptClassVariable_h__
