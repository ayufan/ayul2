#include "StdAfx.hpp"
#include <windows.h>

ScriptFunction::ScriptFunction() : ScriptObject(""), Context(NULL), Type(Static), VirtualIndex(~0)
{
}

string ScriptFunction::toRefString() const
{
	string refString = ReturnType.toString(false) + " ";
	if(Context)
		refString += Context->toRefString();

	refString += "(";
	for(unsigned i = 0; i < ArgumentList.size(); ++i)
		refString += (i==0 ? "" : ", ") + ArgumentList[i].toString(false);
	refString += ")";

	return refString;
}

string ScriptFunction::toString( bool cppString ) const
{
	return string();
}

void ScriptFunction::compile( const intrusive_ptr<SyntaxStatement>& statementList, ScriptObject& object )
{
	throw std::runtime_error("not supported");
}

void ScriptFunction::jitIT(const intrusive_ptr<ScriptObject> &object, JITCode& code)
{
}

unsigned ScriptFunction::argumentsSize() const
{
	unsigned size = 0;
	for(unsigned i = 0; i < ArgumentList.size(); ++i)
		size += ArgumentList[i].Type.IsRef ? sizeof(void*) : ArgumentList[i].Type.sizeOfAligned();
	return size;
}

unsigned ScriptFunction::returnSize() const
{
	return ReturnType.IsRef ? sizeof(void*) : ReturnType.sizeOfAligned();
}

bool ScriptFunction::equals( const ScriptObject& otherObject ) const
{
	return false;
}

ScriptNativeFunction::ScriptNativeFunction( void* nativePtr, const ScriptType& retType, const vector<ScriptType>& argumentList, ScriptObject* context, EnumType type )
{
	NativePtr = nativePtr;
	ReturnType = retType;
	ArgumentList.assign(argumentList.begin(), argumentList.end());
	Context = context;
	Type = type;
	detectVirtualIndex();
}

ScriptNativeFunction::ScriptNativeFunction( void* nativePtr, const ScriptType& retType, const vector<ScriptVariable>& argumentList, ScriptObject* context, EnumType type )
{
	NativePtr = nativePtr;
	ReturnType = retType;
	ArgumentList = argumentList;
	Context = context;
	Type = type;
	detectVirtualIndex();
}

string ScriptNativeFunction::toString(bool cppString) const
{
	return cppString ? string() : "native";
}

#define DEFINE_VIRTUAL(name,x) virtual int __cdecl name() { return x; }
#define DEFINE_VIRTUAL2(name,x) DEFINE_VIRTUAL(name##_##0,2*(x)) DEFINE_VIRTUAL(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL4(name,x) DEFINE_VIRTUAL2(name##_##0,2*(x)) DEFINE_VIRTUAL2(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL8(name,x) DEFINE_VIRTUAL4(name##_##0,2*(x)) DEFINE_VIRTUAL4(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL16(name,x) DEFINE_VIRTUAL8(name##_##0,2*(x)) DEFINE_VIRTUAL8(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL32(name,x) DEFINE_VIRTUAL16(name##_##0,2*(x)) DEFINE_VIRTUAL16(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL64(name,x) DEFINE_VIRTUAL32(name##_##0,2*(x)) DEFINE_VIRTUAL32(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL128(name,x) DEFINE_VIRTUAL64(name##_##0,2*(x)) DEFINE_VIRTUAL64(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL256(name,x) DEFINE_VIRTUAL128(name##_##0,2*(x)) DEFINE_VIRTUAL128(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL512(name,x) DEFINE_VIRTUAL256(name##_##0,2*(x)) DEFINE_VIRTUAL256(name##_##1,2*(x)+1)
#define DEFINE_VIRTUAL1024(name,x) DEFINE_VIRTUAL512(name##_##0,2*(x)) DEFINE_VIRTUAL512(name##_##1,2*(x)+1)

struct VirtualIndexResolver {
	typedef int (__cdecl *Functor)(const VirtualIndexResolver& this$);
	DEFINE_VIRTUAL1024(index,0);
};

void ScriptNativeFunction::detectVirtualIndex()
{
	if(Type == VirtualMethod) {
		byte* ptr = (byte*)NativePtr;
	
		// first is always JMP
		if(ptr[0] != 0xE9)
			goto setNormalMethod;

		// detect function pointer
		{
			VirtualIndexResolver::Functor functor = (VirtualIndexResolver::Functor)(ptr + *(unsigned*)&ptr[1] + 5);
			VirtualIndex = functor(VirtualIndexResolver());
			printf("- %s is at %i index\n", toRefString().c_str(), VirtualIndex);
		}
		return;

setNormalMethod:
		Type = Method;
		return;
	}
}

void ScriptNativeFunction::jitIT( const intrusive_ptr<ScriptObject> &object, JITCode& code )
{
}

string ScriptImportFunction::toString(bool cppString) const
{
	return cppString ? string() : "import";
}

ScriptScriptFunction::ScriptScriptFunction(const ScriptType& retType, const vector<ScriptVariable>& argumentList, ScriptObject* context, EnumType type)
{
	ReturnType = retType;
	ArgumentList = argumentList;
	Context = context;
	Type = type;
	VariableStackUsage = 0;
}

void ScriptScriptFunction::compile(const intrusive_ptr<SyntaxStatement>& statementList, ScriptObject& object)
{
	Module = object.module().get();
	if(Module == NULL)
		statementList->error("couldn't compile statement not assigned to module");

	// Initialize context
	SyntaxCompilerContext context(object, *Module);
	context.ReturnType = ReturnType;
	context.VariableList.assign(ArgumentList.rbegin(), ArgumentList.rend()); // reverse order of variables (cdecl/stdcall)
	if(isStaticFunction()) 
		context.ContextType.setStatic();
	if(context.ContextType.isValue())
		context.ContextType.IsRef = true;
	SyntaxCompilerResults results(context);

	// Compile statement
	statementList->compile(results);

	// Check return type
	if(context.ReturnType.isValid() && !results.Returned)
		statementList->error("end of method without return reached");

	// Save compiled code
	results.pushLabel(results.LeaveLabel);
	OpList = results.OpList;
	VariableList = context.VariableList;

	OpDesc.resize(OpList.size());

#define _DEBUG
#ifdef _DEBUG
	logf("\n\n");
	logf("Function : %s\n", toRefString().c_str());
	logf("Variables\n");
	for(unsigned i = 0; i < context.VariableList.size(); ++i) {
		ScriptLocalVariable& variable = context.VariableList[i];
		logf(variable.NextVariable != ~0 ? "%*c %i) %s [level=%i] [next=%i]\n" : "%*c %i) %s [level=%i] [next=%i]\n", variable.Level * 2, ' ', i, variable.toString().c_str(), variable.Level, variable.NextVariable);
	}
	logf("Code\n");
	for(unsigned i = 0; i < results.OpList.size(); ++i) {
		OpCode& code = results.OpList[i];
		OpDesc[i] = code.toString(results.Context);
		logf(" %i) %s\n", i, OpDesc[i].c_str());
	}
	logf("End.\n");
#endif
}

void ScriptScriptFunction::jitIT(const intrusive_ptr<ScriptObject> &object, JITCode& code)
{
	// save pointer to module
	Module = object->module().get();
	assert(Module);

	// Generate RefString!
	RefString = toRefString();

	// prepare variable offsets
	alignVariables();

	// emit function
	CodeOffset = code.size();
	code.label(LabelType(this, 0));
	jitEmit(code);
}

ScriptScriptFunction::~ScriptScriptFunction()
{
}

void ScriptScriptFunction::alignVariables()
{
	// Calculate initial arguments size
	int offset = 0;
	offset -= argumentsSize(); // add arguments size
	if(isStaticFunction())
		offset += sizeof(void*); // add this pointer

	vector<int> offsetLevels(6);
	offsetLevels[0] = offset;

	VariableStackUsage = 0;

	for(unsigned i = 0; i < VariableList.size(); ++i) {
		// Get variable
		ScriptLocalVariable& variable = VariableList[i];
		if(offsetLevels.size() <= variable.Level)
			offsetLevels.resize(variable.Level+1);

		// Get previous variable level
		unsigned parentLevel = ~0;
		if(variable.NextVariable >= 0) {
			parentLevel = VariableList[variable.NextVariable].Level;
			if(variable.Level < parentLevel)
				throw std::runtime_error("expected that NextVariable has lower or equal level than current");
			if(parentLevel == 0)
				parentLevel = ~0;
		}

		// Assign variable offset
		unsigned sizeOf = variable.Type.IsRef ? sizeof(void*) : variable.Type.sizeOfAligned();
		variable.Offset = offsetLevels[parentLevel != ~0 ? parentLevel : variable.Level];
		offsetLevels[variable.Level] = variable.Offset + sizeOf;

		// Calculate stack usage
		if(!variable.isArgument())
			VariableStackUsage = std::max<int>(VariableStackUsage, variable.Offset + sizeOf);
	}
}

ScriptImportFunction::ScriptImportFunction(const ScriptType& retType, const vector<ScriptType>& argumentList, ScriptObject* context, EnumType type)
{
	ReturnType = retType;
	ArgumentList.assign(argumentList.begin(), argumentList.end());
	Context = context;
	Type = type;
}

ScriptImportFunction::ScriptImportFunction(const ScriptType& retType, const vector<ScriptVariable>& argumentList, ScriptObject* context, EnumType type)
{
	ReturnType = retType;
	ArgumentList.assign(argumentList.begin(), argumentList.end());
	Context = context;
	Type = type;
}

ScriptModuleFunction::ScriptModuleFunction()
{
	CodeOffset = ~0;
	Module = NULL;
}

void* ScriptModuleFunction::nativePtr() const
{
	if(CodeOffset == ~0)
		return NULL;
	if(!Module || !Module->Code.get())
		return NULL;
	return Module->Code->code(CodeOffset); 
}
