#ifndef ScriptFunction_h__
#define ScriptFunction_h__

struct SyntaxStatement;

struct ScriptFunction : public ScriptObject {
	enum EnumType {
		Static,
		Method,
		VirtualMethod
	};

protected:
	ScriptObject* Context;
	ScriptType ReturnType;
	vector<ScriptVariable> ArgumentList;
	EnumType Type;
	unsigned VirtualIndex;

public:
	ScriptFunction();
	const ScriptType& returnType() const { return ReturnType; }
	unsigned argumentCount() const { return ArgumentList.size(); }
	const ScriptVariable& argument(unsigned i) const { return ArgumentList.at(i); }
	unsigned returnSize() const;
	unsigned argumentsSize() const;
	unsigned sizeOf() const { return 2*sizeof(void*); }
	bool equals(const ScriptObject& otherObject) const;
	bool isValue() const { return true; }
	virtual void jitIT(const intrusive_ptr<ScriptObject> &object, JITCode& code);
	virtual bool isNative() const { return false; }
	virtual bool isImport() const { return false; }
	virtual bool isCompilable() const { return false; }
	virtual void* nativePtr() const { return NULL; }
	bool isStaticFunction() const { return Type == Static; }
	bool isVirtual() const { return Type == VirtualMethod; }
	virtual string toRefString() const;
	virtual string toString(bool cppString) const;
	virtual void compile(const intrusive_ptr<SyntaxStatement>& statementList, ScriptObject& object);
};

struct ScriptNativeFunction : public ScriptFunction {
protected:
	void *NativePtr;

private:
	void detectVirtualIndex();

public:
	ScriptNativeFunction(void* nativePtr, const ScriptType& retType, const vector<ScriptType>& argumentList, ScriptObject* context, EnumType type);
	ScriptNativeFunction(void* nativePtr, const ScriptType& retType, const vector<ScriptVariable>& argumentList, ScriptObject* context, EnumType type);
	bool isNative() const { return true; }
	void* nativePtr() const { return NativePtr; }
	string toString(bool cppString) const;
	void jitIT(const intrusive_ptr<ScriptObject> &object, JITCode& code);
};

struct ScriptImportFunction : public ScriptFunction {
public:
	ScriptImportFunction(const ScriptType& retType, const vector<ScriptType>& argumentList, ScriptObject* context, EnumType type);
	ScriptImportFunction(const ScriptType& retType, const vector<ScriptVariable>& argumentList, ScriptObject* context, EnumType type);
	bool isImport() const { return true; }
	string toString(bool cppString) const;
};

struct ScriptModuleFunction : public ScriptFunction {
	ScriptModule* Module;
	unsigned CodeOffset;

	ScriptModuleFunction();
	void* nativePtr() const;
};

struct ScriptScriptFunction : public ScriptModuleFunction {
	vector<ScriptLocalVariable> VariableList;
	vector<OpCode> OpList;
	unsigned VariableStackUsage;
	vector<string> OpDesc;
	string RefString;

private:
	void jitEmit(JITCode& code);
	void alignVariables();

public:
	ScriptScriptFunction(const ScriptType& retType, const vector<ScriptVariable>& argumentList, ScriptObject* context, EnumType type);
	~ScriptScriptFunction();
	bool isCompilable() const { return true; }
	void jitIT(const intrusive_ptr<ScriptObject> &object, JITCode& code);
	void compile(const intrusive_ptr<SyntaxStatement>& statementList, ScriptObject& object);
	friend struct OpCode;
};

#endif // ScriptFunction_h__
