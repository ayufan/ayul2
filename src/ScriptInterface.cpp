#include "StdAfx.hpp"

ScriptInterface::ScriptInterface(const string& interfaceName) : ScriptObject(interfaceName) {
}

ScriptInterface::~ScriptInterface() {
}

string ScriptInterface::toString() const {
	if(GenerateCppString) {
		string extra;
		if(Implements.size()) {
			for(unsigned i = 0; i < Implements.size(); ++i) {
				if(extra.empty())
					extra = " : protected";
				else
					extra += ", protected ";
				extra += Implements[i]->name();
			}
		}
		return "class " + ScriptObject::toString() + extra;
	}
	else {
		return "interface " + ScriptObject::toString();
	}
}

intrusive_ptr<ScriptObject> ScriptInterface::expandObject( const intrusive_ptr<ScriptObject>& object )
{
	intrusive_ptr<ScriptObject> objectFound = ScriptObject::expandObject(object);
	if(objectFound)
		return objectFound;

	for(unsigned i = 0; i < Implements.size(); ++i)
		if(objectFound = Implements[i]->expandObject(object))
			return objectFound;
	return NULL;
}
