#ifndef ScriptInterface_h__
#define ScriptInterface_h__

class ScriptInterface : public ScriptObject {
	vector<intrusive_ptr<ScriptInterface> > Implements;

public:
	ScriptInterface(const string& interfaceName = string());
	~ScriptInterface();

	unsigned sizeOf() const { return sizeof(void*); }

	string toString() const;

	intrusive_ptr<ScriptObject> expandObject(const intrusive_ptr<ScriptObject>& object);

	friend class ScriptModule;
	friend struct SyntaxInterface;
};

#endif // ScriptInterface_h__
