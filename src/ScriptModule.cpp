#include "StdAfx.hpp"

ScriptModule::ScriptModule(const string& name) : ScriptObject(name) {
}

string ScriptModule::toString() const { 
	return (GenerateCppString ? "namespace " : "module ") + ScriptObject::toString(); 
}

unsigned ScriptModule::findScriptType(const ScriptType& type) {
	if(!type.isValid())
		return ~0;

	// find type
	vector<ScriptType>::iterator itor = std::find(TypeList.begin(), TypeList.end(), type);
	if(itor != TypeList.end()) {
		return itor - TypeList.begin();
	}

	// add type to list
	TypeList.push_back(type);
	return TypeList.size()-1;
}

unsigned ScriptModule::findFunction( const intrusive_ptr<ScriptFunction>& function )
{
	if(function == NULL)
		return ~0;

	// find type
	vector<intrusive_ptr<ScriptFunction> >::iterator itor = std::find(FunctionList.begin(), FunctionList.end(), function);
	if(itor != FunctionList.end()) {
		return itor - FunctionList.begin();
	}

	// add type to list
	FunctionList.push_back(function);
	return FunctionList.size()-1;
}

intrusive_ptr<ScriptObject> ScriptModule::expandObjectWithParent( const intrusive_ptr<ScriptObject>& object )
{
	// Check parent object
	intrusive_ptr<ScriptObject> objectFound = ScriptObject::expandObjectWithParent(object);
	if(objectFound)
		return objectFound;

	// Check all submodules
	for(unsigned i = 0; i < Objects.size(); ++i) {
		if(Objects[i]->is<ScriptModule>()) {
			if(objectFound = Objects[i]->as$<ScriptModule>()->expandObject(object))
				return objectFound;
		}
	}

	return NULL;
}

void ScriptModule::dump() const
{
	logf("\n");
	logf("List of function references: %i\n", FunctionList.size());

	for(unsigned i = 0; i < FunctionList.size(); ++i) {
		ScriptFunction& function = *FunctionList[i];
		logf("- %s\n", function.toRefString().c_str());
	}

	logf("\n");
	logf("List of type references: %i\n", TypeList.size());

	for(unsigned i = 0; i < TypeList.size(); ++i) {
		const ScriptType& type = TypeList[i];
		logf("- %s\n", type.toString().c_str());
	}

	logf("\n");
	logf("List of variable references: %i\n", VariableList.size());

	for(unsigned i = 0; i < VariableList.size(); ++i) {
		const ScriptClassVariable& variable = *VariableList[i];
		logf("- %s\n", variable.toString().c_str());
	}

	logf("\n");
}

unsigned ScriptModule::addData( const string& data )
{
	vector<string>::iterator itor = std::find(DataList.begin(), DataList.end(), data);
	if(itor != DataList.end())
		return itor - DataList.begin();

	DataList.push_back(data);
	return DataList.size()-1;
}

unsigned ScriptModule::addData( const void *data, unsigned length )
{
	return addData(string((const char*)data, length));
}

unsigned ScriptModule::findVariable( const intrusive_ptr<ScriptClassVariable>& variable )
{
	if(variable == NULL)
		return ~0;

	// find variable on list
	vector<intrusive_ptr<ScriptClassVariable> >::iterator itor = std::find(VariableList.begin(), VariableList.end(), variable);
	if(itor != VariableList.end()) {
		return itor - VariableList.begin();
	}

	// add variable to list
	VariableList.push_back(variable);
	return VariableList.size()-1;	
}

struct ResolveFunctor
{
	typedef std::pair<unsigned, ScriptFunction*> ResolveAddress;
	typedef vector<ResolveAddress> ResolveList;

	JITCode& Code;
	ResolveList& List;
	ImportList& FinalList;

	ResolveFunctor(JITCode& code, ResolveList& resolveList, ImportList& finalList) : Code(code), List(resolveList), FinalList(finalList)
	{
		List.reserve(100);
		FinalList.reserve(100);
	}

	void operator () (const ResolveAddress& address)
	{
		void* nativePtr = address.second->nativePtr();
		assert(nativePtr);
		FinalList.push_back(ImportEntry(address.first, nativePtr));
	}

	void operator () (const intrusive_ptr<ScriptFunction>& function)
	{
		void* nativePtr = function->nativePtr();

		if(!Code.has(LabelType(function.get(), 0)))
		{
			// Create JumpTable
			Code.label(LabelType(function.get(), 0));
			Code.jmp(JITFunctionImport);

			if(nativePtr)
				FinalList.push_back(ImportEntry(Code.size() - sizeof(void*), nativePtr));
			else
				List.push_back(std::make_pair(Code.size() - sizeof(void*), function.get()));
		}

		vector<unsigned> labelList = Code.needs(LabelType(function.get(), JITResolveAsAddress));

		if(labelList.size())
		{
			for(unsigned j = 0; j < labelList.size(); ++j) 
			{
				if(nativePtr)
					FinalList.push_back(ImportEntry(labelList[j] | JITResolveAsAddress, nativePtr));
				else
					List.push_back(ResolveAddress(labelList[j] | JITResolveAsAddress, function.get()));
			}
		}
	}

	void operator () (void* customFunction)
	{
		// Create JumpTable
		Code.label(LabelType(customFunction, 0));
		Code.jmp(JITFunctionImport);

		// Add to import table
		FinalList.push_back(ImportEntry(Code.size() - sizeof(void*), customFunction));
	}

	void FinalizeList()
	{
		std::for_each(List.begin(), List.end(), *this);
		List.clear();
	}
};

void ScriptModule::finalize()
{
	JITCode code;

	// Finalize module
	finalizeGlobalObjects();
	finalizeLocalObjects();
	finalizeCodeObjects(code);

	// Create import list
	ImportList importList;
	ResolveFunctor::ResolveList resolveList;
	ResolveFunctor resolver(code, resolveList, importList);
	std::for_each(FunctionList.begin(), FunctionList.end(), resolver);
	std::for_each(code.customFunctions().begin(), code.customFunctions().end(), resolver);

	// Create function
	Code = code.function();

	// Resolve functions
	resolver.FinalizeList();
	Code->resolveImportTable(importList);
}

BaseNativeScriptModule::BaseNativeScriptModule( const string& moduleName ) : ScriptModule(moduleName)
{
	scoped();
}
