#ifndef ScriptModule_h__
#define ScriptModule_h__

class ScriptModule : public ScriptObject {
public:
	std::auto_ptr<JITFunction> Code;

	vector<intrusive_ptr<ScriptFunction> > FunctionList;
	vector<intrusive_ptr<ScriptClassVariable> > VariableList;
	vector<ScriptType> TypeList;
	vector<string> DataList;

	ScriptModule(const string& name);

	string toString() const;

	bool isModule() const { return true; }
	bool isStatic() const { return true; }

	unsigned findScriptType(const ScriptType& type);
	unsigned findVariable(const intrusive_ptr<ScriptClassVariable>& variable);
	unsigned findFunction(const intrusive_ptr<ScriptFunction>& function);
	unsigned addData(const string& data);
	unsigned addData(const void *data, unsigned length);

	intrusive_ptr<ScriptObject> expandObjectWithParent(const intrusive_ptr<ScriptObject>& object);

	void dump() const;

	void finalize();

	friend class ScriptClass;
};

class BaseNativeScriptModule : public ScriptModule {
public:
	BaseNativeScriptModule(const string& moduleName);

	bool isNative() const { return true; }
};

template<typename Type>
class NativeScriptModule : public BaseNativeScriptModule {
public:
	typedef Type ThisClass;

	NativeScriptModule();
};

#endif // ScriptModule_h__
