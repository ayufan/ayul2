#include "StdAfx.hpp"

BEGIN_CLASS(ScriptObject, Object)
{
	CLASS_METHOD(ToString);
}

ScriptObject::ScriptObject(const string& name) : Name(name) {
	Parent = NULL;
	ObjectIndex = 0;
}

ScriptObject::~ScriptObject() {
	struct DestroyFunctor {
		DestroyFunctor() {}
		void operator () (intrusive_ptr<ScriptObject>& object) {
			object->Parent = NULL;
		}
	};

	std::for_each(Objects.begin(), Objects.end(), DestroyFunctor());	

	Parent && erasePtrFromVector(Parent->Objects, this);
}

static bool sortObjectByIndex(const intrusive_ptr<ScriptObject>& left, const intrusive_ptr<ScriptObject>& right) {
	return left->index() < right->index();
}

string ScriptObject::toFullString(unsigned level) const {
	if(Objects.size())
		return string(level, '\t') + toFullString2();

	string buffer;

	ObjectPtrList objectSorted(Objects);
	std::sort(objectSorted.begin(), objectSorted.end(), sortObjectByIndex);

	buffer = string(level, '\t') + toString() + " {\n";
	for(unsigned i = 0; i < objectSorted.size(); ++i) {
		if(GenerateCppString && (i == 0 || objectSorted[i]->visibility() != objectSorted[i-1]->visibility())) {
			switch(objectSorted[i]->visibility()) {
				case svPublic:
					buffer += string(level, '\t') + "public:\n";
					break;
				case svPrivate:
					buffer += string(level, '\t') + "private:\n";
					break;
				case svProtected:
					buffer += string(level, '\t') + "protected:\n";
					break;
			}
		}

		buffer += objectSorted[i]->toFullString(level+1) + "\n";
	}
	buffer += string(level, '\t') + "}";
	return buffer;
}


bool operator < (const intrusive_ptr<ScriptObject>& left, const intrusive_ptr<ScriptObject>& right) {
	return strcmp(left->name().c_str(), right->name().c_str()) < 0;
}

intrusive_ptr<ScriptObject> ScriptObject::expandObject(const intrusive_ptr<ScriptObject>& object) {
	intrusive_ptr<ScriptObject> objectFound;

	ObjectPtrList::iterator itor = std::lower_bound(Objects.begin(), Objects.end(), object);
	while(itor != Objects.end()) {
		if(itor->get()->name() != object->name())
			break;

		if(object->equals(*itor->get())) {
			if(objectFound)
				throw std::runtime_error("more than one possible object to expand");
			objectFound = *itor;
		}
		++itor;
	}

	return objectFound;
}

struct ScopedScriptObject : public ScriptObject {
	ScopedScriptObject(const string& name) : ScriptObject(name) { scoped(); }
};

intrusive_ptr<ScriptObject> ScriptObject::findObject(const string& objectName) {
	ScopedScriptObject object(objectName);
	return expandObject(&object);
}

void ScriptObject::addObject(const intrusive_ptr<ScriptObject>& object) {
	if(object->Parent == this)
		return;

	if(isStatic()) {
		if(!object->is<ScriptObject>() && !object->isStatic()) {
			throw std::runtime_error("can't add non static object to static module");
		}
	}

	ObjectPtrList::iterator itor = std::lower_bound(Objects.begin(), Objects.end(), object);

	assert(object->Parent == NULL);

	// Check each object with added name
	while(itor != Objects.end()) {
		if(itor->get()->name() != object->name())
			break;
		if(*itor == object)
			return;

		// Check if object equals
		if(itor->get()->equals(*object)) {
			throw std::runtime_error("object already exist in module context");			
		}
		++itor;
	}

	// Insert object to collection
	Objects.insert(itor, object);

	if(!object->isModule())
	{
		object->Index = ++ObjectIndex;
		object->Parent = this;
	}
}

intrusive_ptr<ScriptObject> ScriptObject::expandObjectWithParent( const intrusive_ptr<ScriptObject>& object )
{
	intrusive_ptr<ScriptObject> objectFound(expandObject(object));
	if(objectFound)
		return objectFound;
	if(Parent)
		return Parent->expandObjectWithParent(object);
	return NULL;
}

intrusive_ptr<ScriptObject> ScriptObject::findObjectWithParent( const string& objectName )
{	
	ScopedScriptObject object(objectName);
	return expandObjectWithParent(&object);
}

void ScriptObject::finalizeLocalObjects()
{
	std::for_each(Objects.begin(), Objects.end(), FinalizeLocalObjects());
}

void ScriptObject::finalizeCodeObjects( JITCode& code )
{
	std::for_each(Objects.begin(), Objects.end(), FinalizeCodeObjects(code));
}

void ScriptObject::finalizeGlobalObjects()
{
	std::for_each(Objects.begin(), Objects.end(), FinalizeGlobalObjects());
}

string ScriptObject::toRefString() const {
	string refString = name();
	for(ScriptObject* object = Parent; object; object = object->Parent)
		refString = object->name() + "::" + refString;
	return refString;
}

string ScriptObject::toFullString2(unsigned level) const {
	string buffer(toString());
	string output;

	size_t pos = string::npos;
	size_t prev = 0;
	while((pos = buffer.find_first_of('\n', pos+1)) != string::npos) {
		output += string(level, '\t') + buffer.substr(prev, pos - prev) + "\n";
		prev = pos+1;
	}

	if(output.empty())
		return string(level, '\t') + buffer;

	if(prev > 0 && pos == string::npos)
		output += string(level, '\t') + buffer.substr(prev);
	return output;
}

intrusive_ptr<ScriptModule> ScriptObject::module() const {
	for(const ScriptObject* object = this; object; object = object->Parent) {
		ScriptModule* module = dynamic_cast<ScriptModule*>(const_cast<ScriptObject*>(object));
		if(module) {
			return module;
		}
	}
	return NULL;
}

unsigned ScriptObject::parentSizeOf() const
{
	return Parent ? Parent->sizeOf() : ~0;
}

inline void hashCombine(unsigned& seed, char c) {
	seed ^= c + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

inline void hashCombine(unsigned& seed, const char* name) {
	while(*name)
		hashCombine(seed, *name++);
}

unsigned ScriptObject::hash() const
{
	unsigned seed = 1;
	for(const ScriptObject* object = this; object; object = object->Parent)
		hashCombine(seed, object->Name.c_str());
	return seed;
}
