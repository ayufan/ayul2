#ifndef ScriptObject_h__
#define ScriptObject_h__

class ScriptObject : public Object {
	DECLARE_CLASS(ScriptObject);

public:
	typedef vector<intrusive_ptr<ScriptObject> > ObjectPtrList;
	static bool GenerateCppString;

protected:
	ScriptObject* Parent;
	unsigned Index;
	string Name;
	ObjectPtrList Objects;
	unsigned ObjectIndex;

	ScriptObject(const string& name);

private:
	string toFullString2(unsigned level = 0) const;

public:
	~ScriptObject();

	String* ToString() const { return String::allocString(name()); }

	virtual const string& name() const { return Name; }

	unsigned hash() const;

	ScriptObject* parent() const { return Parent; }

	unsigned index() const { return Index; }

	virtual string toString() const { return name(); }

	virtual string toFullString(unsigned level = 0) const;

	virtual SyntaxVisiblity visibility() const { return svNone; }

	virtual string toRefString() const;

	virtual bool isNative() const { return false; }

	virtual bool isModule() const { return false; }

	virtual bool isStatic() const { return false; }

	virtual bool equals(const ScriptObject& otherObject) const { return name() == otherObject.name(); }

	virtual intrusive_ptr<ScriptModule> module() const;

	virtual unsigned sizeOf() const { return 0; }

	unsigned parentSizeOf() const;

	unsigned objectCount() const {
		return Objects.size();
	}

	const ObjectPtrList::value_type& object(unsigned i) const {
		return Objects[i];
	}

	ObjectPtrList::value_type& object(unsigned i) {
		return Objects[i];
	}

	virtual bool isValue() const {
		return false;
	}

	virtual bool isObject() const {
		return false;
	}

	virtual void finalizeGlobalObjects();

	virtual void finalizeLocalObjects();

	virtual void finalizeCodeObjects(JITCode& code);

	void addObject(const intrusive_ptr<ScriptObject>& object);

	template<typename Type>
	void addObject() {
		addObject(Type::id$());
	}

	virtual intrusive_ptr<ScriptObject> expandObjectWithParent(const intrusive_ptr<ScriptObject>& object);

	template<typename Type>
	intrusive_ptr<Type> expandObjectWithParent(const intrusive_ptr<ScriptObject>& object, bool throw_ = true) {
		return castToType<Type>(expandObjectWithParent(object), throw_);
	}

	virtual intrusive_ptr<ScriptObject> expandObject(const intrusive_ptr<ScriptObject>& object);

	template<typename Type>
	intrusive_ptr<Type> expandObject(const intrusive_ptr<ScriptObject>& object, bool throw_ = true) {
		return castToType<Type>(expandObject(object), throw_);
	}

	virtual intrusive_ptr<ScriptObject> findObject(const string& objectName);

	template<typename Type>
	intrusive_ptr<Type> findObject(const string& objectName, bool throw_ = true) {
		return castToType<Type>(findObject(objectName), throw_);
	}

	virtual intrusive_ptr<ScriptObject> findObjectWithParent(const string& objectName);

	template<typename Type>
	intrusive_ptr<Type> findObjectWithParent(const string& objectName, bool throw_ = true) {
		return castToType<Type>(findObjectWithParent(objectName), throw_);
	}

	virtual bool isSubObject(const intrusive_ptr<ScriptObject>& object) const {
		return object.get() == this;
	}

private:
	template<typename Type>
	intrusive_ptr<Type> castToType(const intrusive_ptr<ScriptObject>& object, bool throw_ = true) {
		if(object) {
			intrusive_ptr<Type> objectType = boost::dynamic_pointer_cast<Type>(object);
			if(objectType)
				return objectType;
			if(throw_)
				throw std::runtime_error("object of specified can't be expanded");
		}
		return NULL;
	}

protected:
	friend class ScriptObject;
	friend struct FinalizeGlobalObjects;
	friend struct FinalizeLocalObjects;
	friend struct FinalizeCodeObjects;
};

struct FinalizeGlobalObjects 
{
	void operator () (const intrusive_ptr<ScriptObject>& object) 
	{
		return object->finalizeGlobalObjects();
	}
};

struct FinalizeLocalObjects 
{
	void operator () (const intrusive_ptr<ScriptObject>& object) 
	{
		return object->finalizeLocalObjects();
	}
};

struct FinalizeCodeObjects 
{
	JITCode& Code;

	FinalizeCodeObjects(JITCode& code) : Code(code)
	{}

	void operator () (const intrusive_ptr<ScriptObject>& object) 
	{
		return object->finalizeCodeObjects(Code);
	}
};

#endif // ScriptObject_h__
