#include "StdAfx.hpp"

string ScriptType::toString(bool cppString) const {
	// No object - is void
	if(Object == NULL) {
		return "void";
	}

	// Add const prefix
	string buffer;

	if(isStatic())
		buffer += "static ";

	if(IsConst)
		buffer += "const ";

	// Add object name
	buffer += Object->toRefString();

	// If class add pointer
	if(isObject()) {
		if(cppString)
			buffer += "*";
	}

	// Add reference postfix
	if(IsRef)
		buffer += "&";

	return buffer;
}

unsigned ScriptType::sizeOf(bool objectSize) const
{
	if(!Object)
		return 0;
	if(IsRef)
		return sizeof(void*);
	if(isValue())
		return Object->sizeOf();
	if(isObject())
		return objectSize ? Object->sizeOf() : sizeof(void*);

	return 0;
}

bool ScriptType::isPOD() const
{
	if(IsPOD)
		return true;
	if(!Object)
		return true;
	if(isObject())
		return true;
	if(IsRef)
		return true;
	if(intrusive_ptr<ScriptValueType> valueType = Object->as<ScriptValueType>())
		return valueType->isPOD() && valueType->sizeOf() <= sizeof(__int64);
	return false;
}

unsigned ScriptType::sizeOfAligned(bool objectSize) const
{
	unsigned size = sizeOf(objectSize);
	return (size + 3) & -4;
}

intrusive_ptr<ScriptFunction> ScriptType::findConstructor( const vector<ScriptType>& argumentList /*= vector<ScriptType>()*/ ) const
{
	if(isStatic())
		return NULL;

	ScopedClassConstructor scoped(argumentList);
	if(intrusive_ptr<ScriptObject> module = Object->as<ScriptObject>())
		if(intrusive_ptr<ScriptClassMethod> method = module->expandObject<ScriptClassMethod>(&scoped))
			if(method->function() && !method->function()->isStaticFunction())
				return method->function();

	return NULL;
}

intrusive_ptr<ScriptFunction> ScriptType::findMethod( const string& name, const vector<ScriptType>& argumentList /*= vector<ScriptType>()*/, bool withParents ) const
{
	if(!Object)
		return NULL;

	ScopedClassMethod scoped(name, argumentList);

	// Find member methods
	if(intrusive_ptr<ScriptClassMethod> method = Object->expandObject<ScriptClassMethod>(&scoped)) {
		if(method->function() && (method->function()->isStaticFunction() || !isStatic()))
			return method->function();
	}

	// Find static methods
	if(withParents) {
		if(intrusive_ptr<ScriptClassMethod> method = Object->expandObjectWithParent<ScriptClassMethod>(&scoped)) {
			if(method->function() && method->function()->isStaticFunction())
				return method->function();
		}
	}

	return NULL;
}

intrusive_ptr<ScriptFunction> ScriptType::findOperator( SyntaxOperator operator$, const ScriptType& left, const ScriptType& right ) const
{
	if(!Object)
		return NULL;

	ScopedClassOperator scoped(operator$, left, right);
	if(intrusive_ptr<ScriptClassMethod> method = Object->expandObject<ScriptClassMethod>(&scoped)) {
		if(method->function() && method->function()->isStaticFunction())
			return method->function();
	}
	return NULL;
}

intrusive_ptr<ScriptFunction> ScriptType::findOperator( SyntaxAssignemnt operator$, const ScriptType& value ) const
{
	if(!Object)
		return NULL;

	// Take reference to first argument
	ScriptType type(*this);
	if(type.isValue())
		type.IsRef = true;

	ScopedClassOperator scoped(operator$, type, value);
	if(intrusive_ptr<ScriptClassMethod> method = Object->expandObject<ScriptClassMethod>(&scoped)) {
		if(method->function() && method->function()->isStaticFunction())
			return method->function();
	}
	return NULL;
}

intrusive_ptr<ScriptFunction> ScriptType::findCast( const ScriptType& toType ) const
{
	if(!Object)
		return NULL;

	ScopedClassCast scoped(toType);
	if(intrusive_ptr<ScriptClassMethod> method = Object->expandObject<ScriptClassMethod>(&scoped)) {
		if(method->function() && method->function()->isStaticFunction())
			return method->function();
	}
	return NULL;
}

intrusive_ptr<ScriptObject> ScriptType::findObject( const string& name, bool withParents ) const
{
	if(!Object)
		return NULL;

	// Find member object
	if(intrusive_ptr<ScriptObject> object = Object->findObject(name)) {
		if(object && (object->isStatic() || !isStatic()))
			return object;
	}

	// Find static object
	if(withParents) {
		if(intrusive_ptr<ScriptObject> object = Object->findObjectWithParent<ScriptObject>(name)) {
			if(object && object->isStatic())
				return object;
		}
	}

	return NULL;
}

intrusive_ptr<ScriptObject> ScriptType::findModuleObject( const string& name, bool withParents ) const
{
	if(!Object)
		return NULL;

	// Find sub-object
	if(intrusive_ptr<ScriptObject> object = Object->findObject<ScriptObject>(name)) {
		if(object && object->isStatic())
			return object;
	}

	// Find static object
	if(withParents) {
		if(intrusive_ptr<ScriptObject> object = Object->findObjectWithParent<ScriptObject>(name)) {
			if(object && object->isStatic())
				return object;
		}
	}

	return NULL;
}

bool ScriptType::isValue() const
{
	if(Object)
		return Object->isValue();
	return false;
}

bool ScriptType::isObject() const
{
	if(Object)
		return Object->isObject();
	return false;
}

bool ScriptType::isStatic() const
{
	return Object == NULL || IsStatic || Object->isStatic();
}

bool ScriptType::canCastToType( const ScriptType& toType ) const
{
	// Check type conversion
	if(*this == toType)
		return true;

	if(isStatic() || toType.isStatic())
		return false;

	if(isObject() && toType.isObject()) {
		if(IsRef < toType.IsRef)
			return false;

		// search if it can be casted
		if(Object->isSubObject(toType.Object))
			return true;

		return false;
	}
	else if(isValue() && toType.isValue()) {
		if(IsRef < toType.IsRef)
			return false;

		// This same type
		if(Object == toType.Object)
			return true;

		return false;
	}

	return false;
}
