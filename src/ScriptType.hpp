#ifndef ScriptType_h__
#define ScriptType_h__

template<typename Type>
class ScriptTypeT {
};

template<typename Type> static ScriptTypeT<Type&> makeScriptTypeFromPtr(Type*) {
	return ScriptTypeT<Type&>();
}

struct ScriptFunction;

struct ScriptType {
public:
	intrusive_ptr<ScriptObject> Object;
	bool IsRef;
	bool IsConst;
	bool IsPOD; // primitive type may be pod (from C++ side) - this is special flag to force this
private:
	bool IsStatic;

public: // Constructor
	ScriptType() : IsRef(false), IsConst(false), IsPOD(false), IsStatic(false) {}
	template<typename Type>
	explicit ScriptType(const intrusive_ptr<Type>& object_, bool isRef = false, bool isConst = false) : Object(object_), IsRef(isRef), IsConst(isConst), IsPOD(false), IsStatic(false) {}
	template<typename Type>
	explicit ScriptType(Type* object_, bool isRef = false, bool isConst = false) : Object(object_), IsRef(isRef), IsConst(isConst), IsPOD(false), IsStatic(false) {}
	ScriptType(const ScriptType& type, bool isRef, bool isConst = false) : Object(type.Object), IsRef(isRef), IsConst(isConst), IsPOD(type.IsPOD), IsStatic(type.IsStatic) {}

public: // Methods
	intrusive_ptr<ScriptFunction> findConstructor(const vector<ScriptType>& argumentList = vector<ScriptType>()) const;
	intrusive_ptr<ScriptFunction> findMethod(const string& name, const vector<ScriptType>& argumentList = vector<ScriptType>(), bool withParents = false) const;
	intrusive_ptr<ScriptFunction> findOperator(SyntaxOperator operator$, const ScriptType& left, const ScriptType& right) const;
	intrusive_ptr<ScriptFunction> findOperator(SyntaxAssignemnt operator$, const ScriptType& value) const;
	intrusive_ptr<ScriptFunction> findCast(const ScriptType& toType) const;
	intrusive_ptr<ScriptObject> findObject(const string& name, bool withParents = false) const;
	intrusive_ptr<ScriptObject> findModuleObject(const string& name, bool withParents = false) const;

	bool canCastToType(const ScriptType& type) const;

public:
	string toString(bool cppString = false) const;

	bool isValid() const {
		return Object != NULL && !IsStatic;
	}

	ScriptType noRef() const {
		return ScriptType(*this, false, IsConst);
	}
	ScriptType noConst() const {
		return ScriptType(*this, IsRef, false);
	}

	bool isStatic() const;
	bool isValue() const;
	bool isObject() const;
	unsigned sizeOf(bool objectSize = false) const;
	unsigned sizeOfAligned(bool objectSize = false) const;
	bool isPOD() const;

	void setStatic() {
		IsStatic = true;
	}

	// Operators
public:
	template<typename Type>
	ScriptType& operator = (const intrusive_ptr<Type>& object_) {
		*this = ScriptType(object_);
		return *this;
	}
	template<typename Type>
	ScriptType& operator = (Type* object_) {
		*this = ScriptType(object_);
		return *this;
	}

private:
	template<typename Type> static ScriptType fromScriptType3T(const ScriptTypeT<const Type>&) {
		ScriptType type = deduceSimpleType<Type>();
		type.IsConst = true;
		return type;
	}
	template<typename Type> static ScriptType fromScriptType3T(const ScriptTypeT<Type>&) {
		ScriptType type = deduceSimpleType<Type>();
		return type;
	}

	template<typename Type> static ScriptType fromScriptType2T(const ScriptTypeT<Type>&) {
		ScriptType type = fromScriptType3T(ScriptTypeT<Type>());
		return type;
	}
	template<typename Type> static ScriptType fromScriptType2T(const ScriptTypeT<Type&>&) {
		ScriptType type = fromScriptType3T(ScriptTypeT<Type>());
		type.IsRef = true;
		return type;
	}
	template<typename Type> static ScriptType fromScriptType2T(const ScriptTypeT<Type*>&) {
		ScriptType type = fromScriptType3T(ScriptTypeT<Type>());
		type.IsRef = true;
		return type;
	}

public:
	template<typename Type> static ScriptType fromPtrT(Type*) {
		return fromScriptTypeT(ScriptTypeT<Type>());
	}
	template<typename Type> static ScriptType fromScriptTypeT(const ScriptTypeT<Type>& x) {
		ScriptType type = fromScriptType2T(x);
		if(type.isObject()) {
			if(!type.IsRef)
				throw std::runtime_error("class type has to be ref!");
			type.IsRef = false;
		}
		return type;
	}

private:
	template<typename Type>	static ScriptType deduceSimpleType();
	template<> static ScriptType deduceSimpleType<void>();
	template<> static ScriptType deduceSimpleType<int>();
	template<> static ScriptType deduceSimpleType<float>();
	template<> static ScriptType deduceSimpleType<bool>();
	template<> static ScriptType deduceSimpleType<string>();

	friend bool operator == (const ScriptType& left, const ScriptType& right) {
		if(!left.Object && !right.Object)
			return true;

		return left.Object == right.Object && left.IsRef == right.IsRef;
	}
	friend bool operator != (const ScriptType& left, const ScriptType& right) {
		return !(left == right);
	}
};

#endif // ScriptType_h__
