#include "StdAfx.hpp"

string ScriptValue::toString() const {
	char buffer[256];
	switch(Type) {
		case svtNone:
			return string();

		case svtString:
			sprintf(buffer, "\"%s\"", StringValue.c_str());
			return buffer;

		case svtInt:
			sprintf(buffer, "%i", IntValue);
			return buffer;

		case svtFloat:
			sprintf(buffer, "%f", FloatValue);
			return buffer;

		case svtBoolean:
			return BooleanValue ? "true" : "false";

		case svtNewObject:
			assert(ObjectType != NULL);
			sprintf(buffer, "new %s", ObjectType->name().c_str());
			return buffer;

		case svtNull:
			return "null";
	}
	return string();
}

ScriptValue::ScriptValue() : Type(svtNone), IntValue(0), FloatValue(0), BooleanValue(0)
{
}
