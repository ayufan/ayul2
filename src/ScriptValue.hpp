#ifndef ScriptValue_h__
#define ScriptValue_h__

struct ScriptValue {
	enum EnumType {
		svtNone,
		svtString,
		svtInt,
		svtFloat,
		svtBoolean,
		svtNewObject,
		svtNull
	};

	EnumType Type;
	string StringValue;
	int IntValue;
	float FloatValue;
	bool BooleanValue;
	intrusive_ptr<ScriptClass> ObjectType;

	ScriptValue();

	string toString() const;
};

#endif // ScriptValue_h__
