#include "StdAfx.hpp"

string ScriptValueType::toString() const {
	return "struct " + ScriptObject::toString();
}

ScriptValueType::ScriptValueType(const string& className) : ScriptObject(className), SizeOf(~0) {
}

ScriptValueType::~ScriptValueType() {
}

bool ScriptValueType::isValue() const
{
	return true;
}

unsigned ScriptValueType::sizeOf() const
{
	// Recalculate SizeOf
	if(SizeOf == ~0) {
		SizeOf = 0;

		// Calculate sizeOf of each member
		for(unsigned i = 0; i < Objects.size(); ++i) {
			SizeOf += Objects[i]->sizeOf();
		}
	}
	return SizeOf;
}

void ScriptValueType::finalizeGlobalObjects()
{
	ScriptObject::finalizeGlobalObjects();
	sizeOf();
}

void ScriptValueType::finalizeLocalObjects()
{
	ScriptObject::finalizeLocalObjects();

	if(isNative())
		return;

	// Calculate object size
	unsigned totalSize = sizeOf();
	unsigned baseSize = 0;

	for(unsigned i = 0; i < Objects.size(); ++i) {
		if(!Objects[i]->is<ScriptClassVariable>())
			continue;

		// Omit native variables
		ScriptClassVariable& variable = *Objects[i]->as$<ScriptClassVariable>();
		if(variable.isNative())
			continue;

		// Make offset :)
		variable.Offset = baseSize;
		baseSize += variable.sizeOf();
	}

	// Check resulting size
	if(baseSize != totalSize)
		throw std::runtime_error("internal size error!");
}

BaseNativeScriptValueType::BaseNativeScriptValueType( const string& className, unsigned sizeOf ) : ScriptValueType(className)
{
	scoped();
	SizeOf = sizeOf;
}
