#ifndef ScriptValueType_h__
#define ScriptValueType_h__

class ScriptValueType : public ScriptObject {
protected:
	mutable unsigned SizeOf;

protected:
	void finalizeGlobalObjects();
	void finalizeLocalObjects();

public:
	ScriptValueType(const string& className = string());
	~ScriptValueType();

	string toString() const;

	bool isValue() const;

	virtual bool isPOD() const { return false; }

	unsigned sizeOf() const;

	friend class ScriptModule;
	friend struct SyntaxClass;
};

class BaseNativeScriptValueType : public ScriptValueType {
public:
	BaseNativeScriptValueType(const string& className, unsigned sizeOf);

	bool isNative() const { return true; }
};

template<typename Type>
class NativeScriptValueType : public BaseNativeScriptValueType {
public:
	typedef Type ThisClass;

	unsigned sizeOf() const {
		return sizeof(ThisClass);
	}

	bool isPOD() const {
		return __is_pod(ThisClass);
	} 

	NativeScriptValueType();
};

template<typename Type>
class NativeScriptPrimitiveType : public BaseNativeScriptValueType {
public:
	typedef Type ThisClass;

	bool isPOD() const {
		return true;
	} 

	NativeScriptPrimitiveType();
};

#endif // ScriptValueType_h__
