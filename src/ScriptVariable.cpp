#include "StdAfx.hpp"

string ScriptVariable::toString(bool cppString) const {
	return join(' ',
		Type.toString(cppString).c_str(),
		Name.c_str(),
		"\x02=",
		cppString ? Value.toString().c_str() : "",
		NULL);
}

ScriptVariable::ScriptVariable()
{
}

ScriptVariable::ScriptVariable( const ScriptType& type, const string& name /*= string()*/, const ScriptValue& value /*= ScriptValue()*/ ) : Type(type), Name(name), Value(value)
{
}
