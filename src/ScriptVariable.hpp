#ifndef ScriptVariable_h__
#define ScriptVariable_h__

struct ScriptVariable {
	ScriptType Type;
	string Name;
	ScriptValue Value;

	ScriptVariable();
	ScriptVariable(const ScriptType& type, const string& name = string(), const ScriptValue& value = ScriptValue());

	string toString(bool cppString = false) const;

	operator const ScriptType& () const {
		return Type;
	}
	operator const string& () const {
		return Name;
	}
	operator const ScriptValue& () const {
		return Value;
	}
};

struct ScriptLocalVariable : ScriptVariable {
	int NextVariable;
	int Offset;
	unsigned Level; // 0 - arguments

	bool isArgument() const {
		return Level == 0;
	}

	ScriptLocalVariable() : NextVariable(-1), Level(0), Offset(0) {}
	ScriptLocalVariable(const ScriptVariable& variable) : ScriptVariable(variable), NextVariable(-1), Level(0), Offset(0) {}
};

#endif // ScriptVariable_h__
