#include "StdAfx.hpp"

string ScriptVisibility::toString(bool cppString, bool valueType) const {
	switch(Visiblity) {
		case svPublic:
			if(cppString)
				return IsStatic ? "static" : valueType ? string() : "virtual";
			return IsStatic ? "static public" : "public";
		case svProtected:
			if(cppString)
				return IsStatic ? "static" : valueType ? string() : "virtual";
			return IsStatic ? "static protected" : "protected";
		case svPrivate:
			if(cppString)
				return IsStatic ? "static" : valueType ? string() : "virtual";
			return IsStatic ? "static private" : "private";
		default:
			if(cppString)
				return IsStatic ? "static" : valueType ? string() : "virtual";
			return IsStatic ? "static" : string();
	}
}

ScriptVisibility::ScriptVisibility() : IsStatic(false), Visiblity(svNone)
{
}
