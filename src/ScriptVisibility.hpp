#ifndef ScriptVisibility_h__
#define ScriptVisibility_h__

struct ScriptVisibility {
	bool IsStatic;
	SyntaxVisiblity Visiblity;

	ScriptVisibility();

	string toString(bool cppString = false, bool valueType = false) const;
};

#endif // ScriptVisibility_h__
