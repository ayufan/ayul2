#ifndef Serializer_h__
#define Serializer_h__

#define DEF_SERIALIZER(type)	friend Serializer& operator & (Serializer& s, type& value)
#define PRIM_SERIALIZER(type)	DEF_SERIALIZER(type) { s.serialize(&value, sizeof(value)); return s; }

const unsigned SerializeMagic = 'IRES';

struct SerializerHeader
{
	unsigned Magic;
	unsigned Offset;
	unsigned Count;
};

struct SerializerObject
{
	unsigned ClassId;
	unsigned Offset;
	unsigned Size;
};

class Serializer
{
public:
	static string saveObject(Object* object);
	static Object* loadObject(const string& data);

private:
	virtual void serialize(void* data, unsigned size) = 0;
	virtual void serializeObject(Object*& object) = 0;

public: // Primitive serializer
	PRIM_SERIALIZER(char);
	PRIM_SERIALIZER(unsigned char);
	PRIM_SERIALIZER(short);
	PRIM_SERIALIZER(unsigned short);
	PRIM_SERIALIZER(int);
	PRIM_SERIALIZER(unsigned int);
	PRIM_SERIALIZER(long);
	PRIM_SERIALIZER(unsigned long);
	PRIM_SERIALIZER(long long);
	PRIM_SERIALIZER(unsigned long long);

public: // Array serializers
	template<typename T>
	DEF_SERIALIZER(std::vector<T>)
	{
		unsigned count = value.size();
		s & count;
		value.resize(count);
		for(unsigned i = 0; i < count; ++i)
			s & value[i];
		return s;
	}

public: // Object serializer
	friend Serializer& operator & (Serializer& s, intrusive_ptr<Object>& object)
	{
		Object* value = object.get();
		s & value;
		if(object != value)
			object = value;
		return s; 
	}

	friend Serializer& operator & (Serializer& s, Object*& object)
	{
		s.serializeObject(object);
		return s; 
	}
};

#endif // Serializer_h__