#include "StdAfx.hpp"

int logf(const char* fmt, ...) {
	char buffer[10000];
	va_list list;
	va_start(list, fmt);
	vsprintf(buffer, fmt, list);
	fprintf(stdout, "%s", buffer);
	OutputDebugStringA(buffer);
	return 0;
}

int errorf(const char* fmt, ...) {
	char buffer[10000];
	va_list list;
	va_start(list, fmt);
	vsprintf(buffer, fmt, list);
	fprintf(stderr, "[ERROR]: %s", buffer);
	OutputDebugStringA("[ERROR]: ");
	OutputDebugStringA(buffer);
	return 0;
}
