#ifndef StdAfx_h__
#define StdAfx_h__

#define _CRT_SECURE_NO_WARNINGS

int logf(const char* fmt, ...);
int errorf(const char* fmt, ...);

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <cmath>
#include <map>
#include <string>
#include <list>
#include <vector>
#include <set>
#include <algorithm>
#include <boost/intrusive_ptr.hpp>
#include <windows.h>

#include "Common.hpp"
#include "Syntax.hpp"
#include "JITCode.hpp"

#endif // StdAfx_h__
