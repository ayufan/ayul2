#include "StdAfx.hpp"

void* operator new(size_t baseSize, size_t extraSize)
{
	return malloc(baseSize + extraSize);
}

void operator delete(void* x, size_t extraSize)
{
	free(x);
}

void Object::serialize( Serializer& s )
{
	throw std::runtime_error("not supported");
}

BEGIN_STATIC_CLASS(Object) {
	CLASS_METHOD(id);
}

BEGIN_CLASS(String, Object) {
	CLASS_METHOD(toString);
	//CLASS_CAST(Int32);
	//CLASS_CAST(Float32);

	CLASS_OPERATOR2(+, String*, String&, String&);
}

String::String()
{
	Value[0] = 0;
}

String* String::allocString( const string& value )
{
	unsigned length = value.size();
	String* newString = new(length) String();
	strcpy(newString->Value, value.c_str());
	return newString;
}

String* String::allocString( const char* value )
{
	unsigned length = strlen(value);
	String* newString = new(length) String();
	strcpy(newString->Value, value);
	return newString;
}

String* String::allocString( const char* value, unsigned length )
{
	length = strnlen(value, length);
	String* newString = new(length) String();
	strcpy(newString->Value, value);
	return newString;
}

const String* String::toString() const
{
	return this;
}

String::operator int() const
{
	return atoi(Value);
}

String::operator float() const
{
	return (float)atof(Value);
}

String::operator const char*() const
{
	return Value;
}

unsigned String::length() const
{
	return strlen(Value);
}

String* operator + ( String& left, String& right )
{
	if(!&left)
		return &right;
	if(!&right)
		return &left;

	unsigned leftLength = left.length();
	unsigned rightLength = right.length();

	String* newString = new(leftLength + rightLength) String();
	strcpy(newString->Value, left.Value);
	strcat(newString->Value, right.Value);

	return newString;
}