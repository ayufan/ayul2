#ifndef String_h__
#define String_h__

class Serializer;

class Object {
	DECLARE_CLASS(Object);

public:
	template<typename Type>
	bool is() const {
		return dynamic_cast<const Type*>(this) != NULL;
	}
	template<typename Type>
	intrusive_ptr<Type> as() {
		return intrusive_ptr<Type>(dynamic_cast<Type*>(this));
	}
	template<typename Type>
	intrusive_ptr<Type> as$() {
		return intrusive_ptr<Type>(static_cast<Type*>(this));
	}

	virtual void serialize(Serializer& s);

	int Count;

public:
	virtual ~Object() {
		if(Count != ~0)
			assert(Count == 0);
	}

	void scoped() {
		Count = ~0;
	}

protected:
	Object() {
		Count = 0;
	}
	Object(const Object& object) {
		Count = 0;
	}

	friend void intrusive_ptr_add_ref(Object* object) {
		if(object->Count != ~0)
			++object->Count;
	}
	friend void intrusive_ptr_release(Object* object) {
		if(object->Count != ~0 && --object->Count <= 0)
			delete object;
	}

	friend void* __stdcall allocMemory(unsigned size);
};

class String : public Object {
	typedef string Type;
	DECLARE_CLASS(String);
	char Value[1];

public:
	String();
	unsigned length() const;
	static String* allocString(const string& value);
	static String* allocString(const char* value);
	static String* allocString(const char* value, unsigned length);
	const String* toString() const;
	operator int () const;
	operator float () const;
	operator const char* () const;
	friend String* operator + (String& left, String& right);
};

#endif // String_h__