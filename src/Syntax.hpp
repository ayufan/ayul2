#ifndef Syntax_h__
#define Syntax_h__

struct SyntaxLine;
struct SyntaxStatement;
struct SyntaxExpression;
struct SyntaxContext;
struct SyntaxCompilerContext;
struct SyntaxCompilerResults;

extern SyntaxLine asCurrentLine;

#include "SyntaxLine.hpp"
#include "SyntaxObject.hpp"
#include "SyntaxType.hpp"
#include "SyntaxValue.hpp"
#include "SyntaxStatement.hpp"
#include "SyntaxExpression.hpp"
#include "SyntaxVariable.hpp"
#include "SyntaxProperty.hpp"
#include "SyntaxMethod.hpp"
#include "SyntaxDelegate.hpp"
#include "SyntaxClass.hpp"
#include "SyntaxContext.hpp"
#include "SyntaxCompiler.hpp"

#endif // Syntax_h__
