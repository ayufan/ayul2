#include "StdAfx.hpp"

SyntaxClass::SyntaxClass( const string& name ) : SyntaxModuleObject(name), IsStatic(false)
{
}

SyntaxInterface::SyntaxInterface( const string& name ) : SyntaxModuleObject(name)
{
}

void SyntaxInterface::addGlobalObjects(ScriptObject& module) {
	Interface = new ScriptInterface(Name);
	module.addObject(Interface);
}

void SyntaxInterface::addLocalObjects(ScriptObject& module) {
	// resolve interfaces
	if(Implements.size()) {
		Interface->Implements.resize(Implements.size());

		for(unsigned i = 0; i < Implements.size(); ++i) {
			Interface->Implements[i] = module.findObject<ScriptInterface>(Implements[i]);
			if(Interface->Implements[i] == NULL)
				error("extending interface not found");
		}
	}

	std::for_each(Objects.begin(), Objects.end(), AddLocalObjectsFunctor(*Interface));
}

void SyntaxClass::addGlobalObjects(ScriptObject& module) {
	Class = new ScriptClass(Name);
	module.addObject(Class);
	std::for_each(Objects.begin(), Objects.end(), AddGlobalObjectsFunctor(*Class));
}

void SyntaxClass::addLocalObjects(ScriptObject& module) {
	assert(Class != NULL);

	// resolve base classes
	if(Extends.size()) {
		if(IsStatic)
			error("static class can't be extended");

		Class->Extends = module.findObjectWithParent<ScriptClass>(Extends);
		if(Class->Extends == NULL)
			error("extending class not found");

		for(intrusive_ptr<ScriptClass> extends = Class->Extends; extends; extends = extends->Extends) {
			if(extends == Class) {
				error("extending class circular reference");
			}
		}
	}
	else if(!IsStatic) {
		Class->Extends = Object::id$();
	}

	// resolve interfaces
	if(Implements.size()) {
		if(IsStatic)
			error("static class can't be interfaced");

		Class->Implements.resize(Implements.size());

		for(unsigned i = 0; i < Implements.size(); ++i) {
			Class->Implements[i] = module.findObjectWithParent<ScriptInterface>(Implements[i]);
			if(Class->Implements[i] == NULL)
				error("implementing interface not found");
		}
	}

	std::for_each(Objects.begin(), Objects.end(), AddLocalObjectsFunctor(*Class));

	try 
	{
		// Check if class constructor does exist
		if(intrusive_ptr<ScriptClassMethod> method = Class->findObject<ScriptClassMethod>(CTOR_NAME))
		{
			if(method->parent() == Class)
				return;
		}
	}
	catch(...)
	{
		return;
	}

	// Create dummy constructor
	if(Class->isStatic() == false)
	{
		intrusive_ptr<SyntaxClassMethod> method(new SyntaxClassConstructor(Class->name(), Class->isStatic(), svPublic));
		Objects.push_back(method);
		method->addLocalObjects(*Class);
	}
}

void SyntaxClass::addCodeObjects(ScriptObject& module) 
{
	std::for_each(Objects.begin(), Objects.end(), AddCodeObjectsFunctor(*Class));
}

SyntaxValueType::SyntaxValueType( const string& name ) : SyntaxModuleObject(name)
{
}

void SyntaxValueType::addGlobalObjects( ScriptObject& module )
{
	ValueType = new ScriptValueType(Name);
	module.addObject(ValueType);
	std::for_each(Objects.begin(), Objects.end(), AddGlobalObjectsFunctor(*ValueType));
}

void SyntaxValueType::addLocalObjects( ScriptObject& module )
{
	if(ValueType)
	{
		std::for_each(Objects.begin(), Objects.end(), AddLocalObjectsFunctor(*ValueType));
	}

	try 
	{
		// Check if class constructor does exist
		if(intrusive_ptr<ScriptClassMethod> method = ValueType->findObject<ScriptClassMethod>(CTOR_NAME))
		{
			return;
		}
	}
	catch(...)
	{
		return;
	}

	// Create dummy constructor
	if(ValueType->isStatic() == false)
	{
		intrusive_ptr<SyntaxClassMethod> method(new SyntaxClassConstructor(ValueType->name(), ValueType->isStatic(), svPublic));
		Objects.push_back(method);
		method->addLocalObjects(*ValueType);
	}
}

void SyntaxValueType::addCodeObjects( ScriptObject& module )
{
	if(ValueType)
	{
		std::for_each(Objects.begin(), Objects.end(), AddCodeObjectsFunctor(*ValueType));
	}
}
