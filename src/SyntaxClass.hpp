#ifndef SyntaxClass_h__
#define SyntaxClass_h__

struct SyntaxClass : SyntaxModuleObject {
	string Extends;
	vector<string> Implements;
	vector<intrusive_ptr<SyntaxNamedObject> > Objects;
	bool IsStatic;
	SyntaxClass(const string& name);

	intrusive_ptr<ScriptClass> Class;

	void addGlobalObjects(ScriptObject& module);
	void addLocalObjects(ScriptObject& module);
	void addCodeObjects(ScriptObject& module);
};

struct SyntaxValueType : SyntaxModuleObject {
	vector<intrusive_ptr<SyntaxNamedObject> > Objects;

	SyntaxValueType(const string& name);

	intrusive_ptr<ScriptValueType> ValueType;

	void addGlobalObjects(ScriptObject& module);
	void addLocalObjects(ScriptObject& module);
	void addCodeObjects(ScriptObject& module);
};

struct SyntaxInterface : SyntaxModuleObject {
	string Extends;
	vector<string> Implements;
	vector<intrusive_ptr<SyntaxNamedObject> > Objects;
	SyntaxInterface(const string& name);

	intrusive_ptr<ScriptInterface> Interface;

	void addGlobalObjects(ScriptObject& module);
	void addLocalObjects(ScriptObject& module);
};

#endif // SyntaxClass_h__
