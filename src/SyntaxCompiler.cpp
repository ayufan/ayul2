#include "StdAfx.hpp"

SyntaxCompilerContext::SyntaxCompilerContext( ScriptObject& object, ScriptModule& module ) : ContextType(intrusive_ptr<ScriptObject>(&object)), Object(object), Module(module), LabelIndex(0)
{
}

SyntaxCompilerContext::SyntaxCompilerContext( const ScriptType& type, ScriptObject& object, ScriptModule& module ) : ContextType(type), Object(object), Module(module), LabelIndex(0)
{
}

SyntaxCompilerResults::SyntaxCompilerResults( SyntaxCompilerContext& context ) : Context(context), Next(NULL), Temporary(false)
{
	Level = 0;
	VariableIndex = -1;
	Returned = false;
	LeaveLabel = allocLabel();
	ContinueLabel = ~0;
	BreakLabel = ~0;

	// initialize variable chain
	for(unsigned i = 0; i < Context.VariableList.size(); ++i) {
		ScriptLocalVariable& variable = Context.VariableList[i];

		if(!variable.isArgument())
			continue;

		variable.NextVariable = VariableIndex;
		VariableIndex = i;
	}
}

SyntaxCompilerResults::SyntaxCompilerResults( SyntaxCompilerResults& results, bool temporary ) : Context(results.Context), Next(&results), Temporary(temporary)
{
	LeaveLabel = allocLabel();
	ContinueLabel = results.ContinueLabel;
	BreakLabel = results.BreakLabel;
	VariableIndex = results.VariableIndex;
	Level = results.Level+1;
	Returned = false;
}

SyntaxCompilerResults::~SyntaxCompilerResults()
{
}

void SyntaxCompilerResults::deRefType(const ScriptType& type) {
	if(Temporary)
		return;

	// Check previous OpCode
	if(OpList.size()) {
		OpCode& code = OpList.back();

#if 0
		switch(code.Type) {
			case OpCode::PushArgument:
			case OpCode::PushLocal:
				{
					const ScriptLocalVariable* variable = &Context.VariableList.at(code.Args[0]);
					if(variable->Type.isValueType()) {
						if(code.Args[2] == 0) {
							code.Args[2] = 1;
							return;
						}
					}
				}
				break;
		}
#endif
	}

	pushCode(OpCode::DeRef, (*this)[type]);
}
	
void SyntaxCompilerResults::convertType(const ScriptType& from, const ScriptType& to, bool convertType) {
	// Check type conversion
	if(from == to)
		return;

	if(from.isStatic() || to.isStatic())
		throw std::runtime_error("can't convert from or to static type");

	if(from.isObject() && to.isObject()) {
		// Dereference variable
		if(from.IsRef > to.IsRef)
			deRefType(to);
		else if(from.IsRef < to.IsRef)
			throw std::runtime_error("can't convert from non-ref to ref");

		// search if it can be casted
		if(from.Object->isSubObject(to.Object))
			return;

		throw std::runtime_error("ObjectType can't be downcasted");
	}
	else if(from.isValue() && to.isValue()) {
		// Dereference variable
		if(from.IsRef > to.IsRef)
			deRefType(to);
		else if(from.IsRef < to.IsRef)
			throw std::runtime_error("can't convert from non-ref to ref");

		// Hack for delegates!
		if(from.Object->is<ScriptFunction>() && to.Object->is<ScriptClassDelegate>())
			return;

		// This same type
		if(from.Object == to.Object)
			return;

		if(convertType) {
			// search for cast operator
			if(intrusive_ptr<ScriptFunction> function = from.findCast(to)) {
				if(Temporary)
					return;

				OpList.push_back(OpCode(OpCode::Call, (*this)[function], allocTempVariable(function->returnType())));
				return;
			}
		}

		throw std::runtime_error("ValueType can't be casted");
	}

	throw std::runtime_error("type can't be casted");
}

ScriptLocalVariable* SyntaxCompilerResults::findVariable( const string& name, bool all )
{
	// Check if variable with corresponding name already exists
	for(int i = VariableIndex; i != -1; i = Context.VariableList.at(i).NextVariable) {
		ScriptLocalVariable& variable = Context.VariableList.at(i);
		if(!all && variable.Level != Level)
			break;
		if(variable.Name == name)
			return &variable;
	}

	return NULL;
}

unsigned SyntaxCompilerResults::allocLabel()
{
	if(Temporary)
		return ~0;

	return ++Context.LabelIndex;
}

void SyntaxCompilerResults::pushLabel( unsigned label )
{
	if(Temporary)
		return;

	OpList.push_back(OpCode(OpCode::Label, label));
}

void SyntaxCompilerResults::pushCode( OpCode::OpType type, unsigned arg0 /*= 0*/, unsigned arg1 /*= 0*/, unsigned arg2 /*= 0*/ )
{
	if(Temporary)
		return;

	OpList.push_back(OpCode(type, arg0, arg1, arg2));
}

unsigned SyntaxCompilerResults::operator[]( const ScriptType& type )
{
	if(Temporary)
		return ~0;

	return Context.Module.findScriptType(type);
}

unsigned SyntaxCompilerResults::operator[]( const intrusive_ptr<ScriptClassVariable>& variable )
{
	if(Temporary)
		return ~0;

	return Context.Module.findVariable(variable);
}

unsigned SyntaxCompilerResults::operator[]( const intrusive_ptr<ScriptFunction>& function )
{
	if(Temporary)
		return ~0;

	return Context.Module.findFunction(function);
}

void SyntaxCompilerResults::discardType( const ScriptType& type )
{
	if(Temporary)
		return;

	// Discard value
	if(type.isValid()) {
		if(type.isStatic())
			return; // when type is static, there's nothing to discard!
		pushCode(OpCode::Pop, (*this)[type]);
	}
}

unsigned SyntaxCompilerResults::allocTempVariable( const ScriptType& type )
{
	if(Temporary)
		return ~0;

	// local variable only for ValueTypes
	if(type.isValue())
	{
		ScriptLocalVariable variable;
		variable.Type = type;
		variable.Level = Level;
		variable.Name = "$temp$" + Int32(Context.VariableList.size()).toString2();
		variable.NextVariable = VariableIndex;
		VariableIndex = Context.VariableList.size();
		Context.VariableList.push_back(variable);
		return VariableIndex;
	}

	return ~0;
}
