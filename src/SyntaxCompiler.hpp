#ifndef SyntaxCompiler_h__
#define SyntaxCompiler_h__

struct SyntaxCompilerContext {
	ScriptType ContextType;
	ScriptObject& Object;
	ScriptModule& Module;

	// useful values
	vector<ScriptLocalVariable> VariableList;
	ScriptType ReturnType;
	unsigned LabelIndex;

	SyntaxCompilerContext(ScriptObject& object, ScriptModule& module); 
	SyntaxCompilerContext(const ScriptType& type, ScriptObject& object, ScriptModule& module); 
};

struct SyntaxCompilerResults {
	SyntaxCompilerContext& Context;
	SyntaxCompilerResults* Next;
	vector<OpCode> OpList;

	// current values
	unsigned Level;
	int VariableIndex;
	bool Returned;
	unsigned LeaveLabel;
	unsigned ContinueLabel;
	unsigned BreakLabel;
	bool Temporary;

	SyntaxCompilerResults(SyntaxCompilerContext& context);
	SyntaxCompilerResults(SyntaxCompilerResults& results, bool temporary = false);
	~SyntaxCompilerResults();

	void convertType(const ScriptType& from, const ScriptType& to, bool convertType = true);
	void expectType(const ScriptType& from, const ScriptType& to) {
		convertType(from, to, false);
	}
	unsigned allocLabel();
	void pushLabel(unsigned label);
	void pushCode(OpCode::OpType type, unsigned arg0 = 0, unsigned arg1 = 0, unsigned arg2 = 0);
	ScriptLocalVariable* findVariable(const string& name, bool all = true);
	void discardType(const ScriptType& type);

	unsigned allocTempVariable(const ScriptType& type);

	unsigned operator [] (const ScriptType& type);
	unsigned operator [] (const intrusive_ptr<ScriptFunction>& function);
	unsigned operator [] (const intrusive_ptr<ScriptClassVariable>& variable);

private:
	void deRefType(const ScriptType& type);
};

#endif // SyntaxCompiler_h__
