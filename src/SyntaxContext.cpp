#include "StdAfx.hpp"

SyntaxContext::SyntaxContext()
{
}

void SyntaxContext::build(ScriptObject& module) {
	std::for_each(Objects.begin(), Objects.end(), AddGlobalObjectsFunctor(module));
	std::for_each(Objects.begin(), Objects.end(), AddLocalObjectsFunctor(module));
	std::for_each(Objects.begin(), Objects.end(), AddCodeObjectsFunctor(module));
}
