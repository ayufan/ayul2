#ifndef SyntaxContext_h__
#define SyntaxContext_h__

struct SyntaxContext : SyntaxObject {
	vector<intrusive_ptr<SyntaxNamedObject> > Objects;
	SyntaxContext();

	void build(ScriptObject& module);
};

struct AddGlobalObjectsFunctor {
	ScriptObject& Module;
	AddGlobalObjectsFunctor(ScriptObject& module) : Module(module) {}
	void operator () (intrusive_ptr<SyntaxNamedObject>& object) {
		object->addGlobalObjects(Module);
	}
	void operator () (intrusive_ptr<SyntaxModuleObject>& object) {
		object->addGlobalObjects(Module);
	}
};

struct AddLocalObjectsFunctor {
	ScriptObject& Module;
	AddLocalObjectsFunctor(ScriptObject& module) : Module(module) {}
	void operator () (intrusive_ptr<SyntaxNamedObject>& object) {
		object->addLocalObjects(Module);
	}
	void operator () (intrusive_ptr<SyntaxModuleObject>& object) {
		object->addLocalObjects(Module);
	}
};

struct AddCodeObjectsFunctor {
	ScriptObject& Module;
	AddCodeObjectsFunctor(ScriptObject& module) : Module(module) {}
	void operator () (intrusive_ptr<SyntaxNamedObject>& object) {
		object->addCodeObjects(Module);
	}
	void operator () (intrusive_ptr<SyntaxModuleObject>& object) {
		object->addCodeObjects(Module);
	}
};

#endif // SyntaxContext_h__
