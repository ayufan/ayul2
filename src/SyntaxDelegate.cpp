#include "StdAfx.hpp"

SyntaxDelegate::SyntaxDelegate( const intrusive_ptr<SyntaxType>& type, const string& name, SyntaxVisiblity visibility, vector<intrusive_ptr<SyntaxVariable> >& arguments ) : SyntaxModuleObject(name), ReturnType(type), Arguments(arguments)
{
}

SyntaxClassDelegate::SyntaxClassDelegate( const SyntaxDelegate& delegate_, SyntaxVisiblity visibility, vector<intrusive_ptr<SyntaxVariable> >& arguments ) : SyntaxDelegate(delegate_), Visibility(visibility)
{
}

void SyntaxDelegate::addLocalObjects( ScriptObject& module )
{
	Delegate = new ScriptClassDelegate(Name);
	//Delegate->Visiblity = Visibility;
	if(ReturnType) {
		ReturnType->resolveType(module, Delegate->ReturnType);
		if(Delegate->ReturnType.isStatic())
			error("type can't be static");
	}

	Delegate->Arguments.resize(Arguments.size());

	for(unsigned i = 0; i < Arguments.size(); ++i) {
		Delegate->Arguments[i].Name = Arguments[i]->Name;
		if(Arguments[i]->Type) {
			Arguments[i]->Type->resolveType(module, Delegate->Arguments[i].Type);
			if(Delegate->Arguments[i].Type.isStatic())
				error("type can't be static");
		}
		if(Arguments[i]->Value)
			Arguments[i]->Value->setValue(module, Delegate->Arguments[i].Value);
	}

	Delegate->populateDelegate();

	module.addObject(Delegate);
}
