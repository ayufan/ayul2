#ifndef SyntaxDelegate_h__
#define SyntaxDelegate_h__

struct SyntaxDelegate : SyntaxModuleObject {
	intrusive_ptr<SyntaxType> ReturnType;
	intrusive_ptr<ScriptClassDelegate> Delegate;
	vector<intrusive_ptr<SyntaxVariable> > Arguments;
	SyntaxDelegate(const intrusive_ptr<SyntaxType>& type, 
		const string& name, 
		SyntaxVisiblity visibility,
		vector<intrusive_ptr<SyntaxVariable> >& arguments);
	void addLocalObjects(ScriptObject& module);
};

struct SyntaxClassDelegate : SyntaxDelegate {
	SyntaxVisiblity Visibility;
	intrusive_ptr<ScriptClassDelegate> Delegate;
	SyntaxClassDelegate(const SyntaxDelegate& delegate_, 
		SyntaxVisiblity visibility,
		vector<intrusive_ptr<SyntaxVariable> >& arguments);
};

#endif // SyntaxDelegate_h__
