#include "StdAfx.hpp"

SyntaxExpression::SyntaxExpression()
{
}

void SyntaxExpression::compile( SyntaxCompilerResults& results )
{
	SyntaxCompilerResults lresults(results);

	// Compile expression
	ScriptType returnedType;
	getValue(lresults, returnedType);
	lresults.discardType(returnedType);
	results.OpList += lresults.OpList;
	results.pushCode(OpCode::Mark);
}

void SyntaxExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	error("not supported");
}

void SyntaxExpression::setValue( SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment )
{
	error("not supported");
}

SyntaxCastExpression::SyntaxCastExpression( const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxExpression>& expression ) : Type(type), Expression(expression)
{
}

void SyntaxCastExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	// compile expression
	ScriptType valueType;
	if(Expression)
		Expression->getValue(results, valueType);

	// resolve casted type
	Type->resolveType(*results.Context.ContextType.Object, returnedType);

	// convert expression
	results.convertType(valueType, returnedType);
}

SyntaxAsExpression::SyntaxAsExpression( const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxExpression>& expression ) : Type(type), Expression(expression)
{
}

SyntaxIsExpression::SyntaxIsExpression( const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxExpression>& expression ) : Type(type), Expression(expression)
{
}

SyntaxValueExpression::SyntaxValueExpression( const intrusive_ptr<SyntaxValue> &value ) : Value(value)
{
}

void SyntaxValueExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	if(Value) 
	{
		Value->getValue(results, returnedType);
	}
	else 
	{
		error("value not assigned");
	}
}

void SyntaxValueExpression::setValue( SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment )
{
	if(Value) 
	{
		Value->setValue(results, expectedType, returnedType, assignment);
	}
	else 
	{
		error("value not assigned");
	}
}

SyntaxCallExpression::SyntaxCallExpression( const intrusive_ptr<SyntaxExpression>& function, const vector<intrusive_ptr<SyntaxExpression> >& argumentList ) : Function(function), ArgumentList(argumentList)
{
}

void SyntaxCallExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	ScriptType type;

	// Find function in current context
	vector<ScriptType> argumentList(ArgumentList.size());

	{
		SyntaxCompilerResults lresults(results, true);

		Function->getValue(lresults, type);
		if(type.isStatic())
			error("can't call static type");

		// Push arguments in reverse order
		for(unsigned i = ArgumentList.size(); i-- > 0; ) {
			ArgumentList[i]->getValue(lresults, argumentList[i]);
		}
	}

	// Find method
	if(intrusive_ptr<ScriptFunction> function = type.findMethod(CALLER_NAME, argumentList)) {
		// Push arguments
		for(unsigned i = ArgumentList.size(); i-- > 0; ) {
			ScriptType type;
			ArgumentList[i]->getValue(results, type);
			results.expectType(type, function->argument(i));
		}

		// Call as static method
		if(!function->isStaticFunction()) 
		{
			Function->getValue(results, type);

			results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));
		}
		else
		{
			error("invalid caller type");
		}

		// Save return type
		returnedType = function->returnType();
		if(returnedType.isValue())
			returnedType.IsRef = true;
	}
	else 
	{
		error("method not found");
	}
}

SyntaxAccessorExpression::SyntaxAccessorExpression( const intrusive_ptr<SyntaxExpression>& expression, const string& name ) : Expression(expression), Name(name)
{
}

void SyntaxAccessorExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	ScriptType type;
	{
		SyntaxCompilerResults lresults(results, true);
		Expression->getValue(lresults, type);
	}

	// Find class member
	if(intrusive_ptr<ScriptObject> member = type.findObject(Name)) 
	{
		// Get value of class variable
		if(intrusive_ptr<ScriptClassVariable> variable = member->as<ScriptClassVariable>()) 
		{
			// Push static variable
			if(variable->isStatic()) {
				results.pushCode(OpCode::PushStatic, results[variable]);
			}

			// Push class variable
			else {
				// Get this pointer
				Expression->getValue(results, type);

				// Push opcode
				results.pushCode(OpCode::PushOffset, results[variable], results[type]);
			}

			// Save returned type - preserve reference status
			returnedType = variable->Type;
			returnedType.IsRef = (type.isObject() || type.IsRef) && returnedType.isValue();
		}

		// Get value of property
		else if(intrusive_ptr<ScriptClassProperty> property_ = member->as<ScriptClassProperty>())
		{
			// Get getter function
			if(intrusive_ptr<ScriptFunction> function = property_->getFunction()) 
			{
				// Call static function
				if(function->isStatic()) 
				{
					results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));
				}

				// Call class property getter
				else 
				{
					// Get this pointer
					Expression->getValue(results, type);

					// Push opcode
					results.pushCode(OpCode::CallMethod, results[function], results.allocTempVariable(function->returnType()));
				}

				returnedType = function->returnType();
				if(returnedType.isValue())
					returnedType.IsRef = true;
			}
			else 
			{
				error("no property getter");
			}
		}

		// Get address of method
		else if(intrusive_ptr<ScriptClassMethod> method = member->as<ScriptClassMethod>())
		{
			if(intrusive_ptr<ScriptFunction> function = method->function())
			{
				returnedType = function;

				if(function->isStaticFunction())
				{
					// Get this pointer
					results.pushCode(OpCode::PushNull);
					results.pushCode(OpCode::PushFunction, results[function]);
				}
				else
				{
					// Get this pointer
					Expression->getValue(results, type);

					// Push opcode
					results.pushCode(OpCode::PushFunction, results[function]);
				}
			}
			else
			{
				error("method not found");
			}
		}

		// Push module section
		else if(intrusive_ptr<ScriptObject> module = member->as<ScriptObject>()) 
		{
			returnedType = module;
			returnedType.setStatic();
		}

		else 
		{
			error("member not supported");
		}
	}
	else 
	{
		error("member not found");
	}
}

void SyntaxAccessorExpression::setValue( SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment )
{
	ScriptType type;
	{
		SyntaxCompilerResults lresults(results, true);
		Expression->getValue(lresults, type);
	}

	if(!type.IsRef && type.isValue())
		error("requires reference to value");


	// Find class member
	if(intrusive_ptr<ScriptObject> member = type.findObject(Name)) 
	{
		// Get value of class variable
		if(intrusive_ptr<ScriptClassVariable> variable = member->as<ScriptClassVariable>()) 
		{
			// Use assignment operator
			if(intrusive_ptr<ScriptFunction> function = variable->Type.findOperator(assignment, expectedType.noRef())) {
				// Push value to operator function
				results.expectType(expectedType, function->argument(1).Type.noRef());

				// Save as static variable
				if(variable->isStatic()) {
					results.pushCode(OpCode::PushStatic, results[variable]);
				}

				// Save as class variable
				else {
					// Get this pointer
					Expression->getValue(results, type);

					results.pushCode(OpCode::PushOffset, results[variable], results[type]);
				}

				// Execute function
				results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));

				// Discard return value
				returnedType = function->returnType();
				if(returnedType.isValue())
					returnedType.IsRef = true;
			}
			else {
				if(assignment != saNormal)
					error("operator not supported");

				// Convert type to desired type
				results.expectType(expectedType, variable->Type.noRef());

				// Pop static variable
				if(variable->isStatic()) 
				{
					results.pushCode(OpCode::PopStatic, results[variable]);
				}
				// Pop class variable
				else 
				{
					// Get this pointer
					Expression->getValue(results, type);

					// Push opcode
					results.pushCode(OpCode::PopOffset, results[variable], results[type]);
				}
			}
		}

		// Get value of property
		else if(intrusive_ptr<ScriptClassProperty> property_ = member->as<ScriptClassProperty>())
		{
			if(assignment != saNormal)
				error("operator not supported");

			// Get setter function
			if(intrusive_ptr<ScriptFunction> function = property_->setFunction()) 
			{
				// Convert type to desired type
				results.expectType(expectedType, function->argument(0).Type.noRef());

				// Call static function
				if(function->isStaticFunction()) 
				{
					results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));
				}

				// Call class property setter
				else 
				{
					// Get this pointer
					Expression->getValue(results, type);

					// Push opcode
					results.pushCode(OpCode::CallMethod, results[function], results.allocTempVariable(function->returnType()));
				}

				returnedType = function->returnType();
				if(returnedType.isValue())
					returnedType.IsRef = true;
			}
			else {
				error("no property setter");
			}
		}

		else {
			error("member not supported");
		}
	}
	else {
		error("member not found");
	}
}

SyntaxIndexExpression::SyntaxIndexExpression( const intrusive_ptr<SyntaxExpression>& expression, const intrusive_ptr<SyntaxExpression>& index ) : Expression(expression), Index(index)
{
}

SyntaxPostfixExpression::SyntaxPostfixExpression( const intrusive_ptr<SyntaxExpression>& expression, SyntaxPostfix operation ) : Expression(expression), Operation(operation)
{
}

SyntaxUnaryExpression::SyntaxUnaryExpression( const intrusive_ptr<SyntaxExpression>& expression, SyntaxUnary operation ) : Expression(expression), Operation(operation)
{
}

SyntaxOperatorExpression::SyntaxOperatorExpression( const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right, SyntaxOperator operation ) : Left(left), Right(right), Operation(operation)
{
}

void SyntaxOperatorExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	ScriptType arg0, arg1;

	// Detect arguments
	{
		SyntaxCompilerResults lresults(results, true);
		Left->getValue(lresults, arg0);
		Right->getValue(lresults, arg1);
	}

	ScopedClassOperator scoped(Operation, arg0, arg1);
	intrusive_ptr<ScriptClassMethod> method;

	// Check left and right argument
	if(intrusive_ptr<ScriptObject> moduleObject = arg0.Object->as<ScriptObject>()) {
		method = moduleObject->expandObject<ScriptClassMethod>(&scoped);
	}
	if(!method && arg0.Object != arg1.Object) {
		if(intrusive_ptr<ScriptObject> moduleObject = arg1.Object->as<ScriptObject>()) {
			method = moduleObject->expandObject<ScriptClassMethod>(&scoped);
		}
	}

	if(!method) {
		error("operator not found");
	}

	// Operator function has to be static
	intrusive_ptr<ScriptFunction> function = method->function();
	if(!function || !function->isStaticFunction())
		error("operator function has to be static");

	// Generate code (reverse order)
	Right->getValue(results, arg1);
	results.expectType(arg1, function->argument(1).Type);
	Left->getValue(results, arg0);
	results.expectType(arg0, function->argument(0).Type);

	// Call method function
	results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));

	// Return value
	returnedType = function->returnType();
	if(returnedType.isValue())
		returnedType.IsRef = true;
}

SyntaxAssignmentExpression::SyntaxAssignmentExpression( const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right, SyntaxAssignemnt operation ) : Left(left), Right(right), Operation(operation)
{
}

void SyntaxAssignmentExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	ScriptType type;
	Right->getValue(results, type);
	Left->setValue(results, type, returnedType, Operation);
}

void SyntaxAssignmentExpression::setValue( SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment )
{
	ScriptType type;
	Right->getValue(results, type);
	Left->setValue(results, type, returnedType, Operation);
}

SyntaxOrExpression::SyntaxOrExpression( const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right ) : Left(left), Right(right)
{
}

void SyntaxOrExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	unsigned whenTrueLabel = results.allocLabel();
	unsigned afterExpressionLabel = results.allocLabel();

	// Expand left expression
	ScriptType leftType;
	Left->getValue(results, leftType);
	results.expectType(leftType, ScriptType(Boolean::id$()));
	results.pushCode(OpCode::JumpIfTrue, whenTrueLabel);

	// Expand right expression
	ScriptType rightType;
	Left->getValue(results, rightType);
	results.expectType(rightType, ScriptType(Boolean::id$()));
	results.pushCode(OpCode::Jump, afterExpressionLabel);

	// Push labels
	results.pushLabel(whenTrueLabel);
	results.pushCode(OpCode::PushTrue);
	results.pushLabel(afterExpressionLabel);

	// return type
	returnedType = ScriptType(Boolean::id$());
}

SyntaxAndExpression::SyntaxAndExpression( const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right ) : Left(left), Right(right)
{
}

void SyntaxAndExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	unsigned whenFalseLabel = results.allocLabel();
	unsigned afterExpressionLabel = results.allocLabel();

	// Expand left expression
	ScriptType leftType;
	Left->getValue(results, leftType);
	results.expectType(leftType, ScriptType(Boolean::id$()));
	results.pushCode(OpCode::JumpIfFalse, whenFalseLabel);

	// Expand right expression
	ScriptType rightType;
	Left->getValue(results, rightType);
	results.expectType(rightType, ScriptType(Boolean::id$()));
	results.pushCode(OpCode::Jump, afterExpressionLabel);

	// Push labels
	results.pushLabel(whenFalseLabel);
	results.pushCode(OpCode::PushFalse);
	results.pushLabel(afterExpressionLabel);

	// return type
	returnedType = ScriptType(Boolean::id$());
}

SyntaxTestExpression::SyntaxTestExpression( const intrusive_ptr<SyntaxExpression>& test, const intrusive_ptr<SyntaxExpression>& ifTrue, const intrusive_ptr<SyntaxExpression>& ifFalse ) : Test(test), True(ifTrue), False(ifFalse)
{
}

void SyntaxTestExpression::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	// Get statement value
	ScriptType testType;
	Test->getValue(results, testType);
	results.expectType(testType, ScriptType(Boolean::id$()));

	SyntaxCompilerResults trueResults(results);
	SyntaxCompilerResults falseResults(results);

	// Check statement value
	results.pushCode(OpCode::JumpIfFalse, trueResults.LeaveLabel);

	// Compile left expression
	True->getValue(trueResults, returnedType);
	trueResults.pushCode(OpCode::Jump, falseResults.LeaveLabel);
	trueResults.pushLabel(trueResults.LeaveLabel);
	results.OpList += trueResults.OpList;

	// Compile right expression
	ScriptType otherType;
	if(False)
		False->getValue(falseResults, otherType);
	falseResults.expectType(otherType, returnedType);
	falseResults.pushLabel(falseResults.LeaveLabel);
	results.OpList += falseResults.OpList;
}

SyntaxCallMethod::SyntaxCallMethod( const intrusive_ptr<SyntaxExpression>& function, const string& name, const vector<intrusive_ptr<SyntaxExpression> >& argumentList ) : Function(function), Name(name), ArgumentList(argumentList)
{
}

void SyntaxCallMethod::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	// Find function in current context
	vector<ScriptType> argumentList(ArgumentList.size());

	{
		SyntaxCompilerResults lresults(results, true);
		// Push arguments in reverse order
		for(unsigned i = ArgumentList.size(); i-- > 0; ) {
			ArgumentList[i]->getValue(lresults, argumentList[i]);
		}
	}

	// Get current processing context
	ScriptType context;
	{
		SyntaxCompilerResults lresults(results, true);
		if(Function)
			Function->getValue(lresults, context);
		else
			context = results.Context.ContextType;
	}

	// Find method
	if(intrusive_ptr<ScriptObject> object = context.findObject(Name)) {
		if(intrusive_ptr<ScriptClassVariable> variable = object->as<ScriptClassVariable>()) {
			// Find method
			if(intrusive_ptr<ScriptFunction> function = variable->Type.findMethod(CALLER_NAME, argumentList)) {
				// Push arguments
				for(unsigned i = ArgumentList.size(); i-- > 0; ) {
					ScriptType type;
					ArgumentList[i]->getValue(results, type);
					results.expectType(type, function->argument(i));
				}

				// Call as static method
				if(!function->isStaticFunction()) 
				{
					if(variable->isStatic())
					{
						results.pushCode(OpCode::PushStatic, results[variable]);
					}
					else
					{
						if(Function)
							Function->getValue(results, context);
						else
							results.pushCode(OpCode::PushThis);
						results.pushCode(OpCode::PushOffset, results[variable], results[context]);
					}
					results.pushCode(OpCode::CallMethod, results[function], results.allocTempVariable(function->returnType()));
				}
				else
				{
					error("invalid caller type");
				}

				// Save return type
				returnedType = function->returnType();
				if(returnedType.isValue())
					returnedType.IsRef = true;
				return;
			}
			else 
			{
				error("can't call variable");
			}
		}
	}
	
	if(intrusive_ptr<ScriptFunction> function = context.findMethod(Name, argumentList, Function ? false : true)) {
		// Push arguments
		for(unsigned i = ArgumentList.size(); i-- > 0; ) {
			ScriptType type;
			ArgumentList[i]->getValue(results, type);
			results.expectType(type, function->argument(i));
		}

		// Call as static method
		if(function->isStaticFunction()) {
			results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));
		}

		// Call as class method
		else {
			if(Function)
				Function->getValue(results, context);
			else
				results.pushCode(OpCode::PushThis);

			results.pushCode(OpCode::CallMethod, results[function], results.allocTempVariable(function->returnType()));
		}

		// Save return type
		returnedType = function->returnType();
		if(returnedType.isValue())
			returnedType.IsRef = true;
	}
	else {
		error("method not found");
	}
}