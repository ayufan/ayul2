#ifndef SyntaxExpression_h__
#define SyntaxExpression_h__

struct SyntaxExpression : SyntaxStatement {
	SyntaxExpression();
	void compile(SyntaxCompilerResults& results);
	virtual void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
	virtual void setValue(SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment);
};

struct SyntaxCastExpression : SyntaxExpression {
	intrusive_ptr<SyntaxType> Type;
	intrusive_ptr<SyntaxExpression> Expression;
	SyntaxCastExpression(const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxExpression>& expression);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

struct SyntaxAsExpression : SyntaxExpression {
	intrusive_ptr<SyntaxType> Type;
	intrusive_ptr<SyntaxExpression> Expression;
	SyntaxAsExpression(const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxExpression>& expression);
};

struct SyntaxIsExpression : SyntaxExpression {
	intrusive_ptr<SyntaxType> Type;
	intrusive_ptr<SyntaxExpression> Expression;
	SyntaxIsExpression(const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxExpression>& expression);
};

struct SyntaxValueExpression : SyntaxExpression {
	intrusive_ptr<SyntaxValue> Value;
	SyntaxValueExpression(const intrusive_ptr<SyntaxValue> &value);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
	void setValue(SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment);
};

struct SyntaxCallExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Function;
	vector<intrusive_ptr<SyntaxExpression> > ArgumentList;
	SyntaxCallExpression(const intrusive_ptr<SyntaxExpression>& function, const vector<intrusive_ptr<SyntaxExpression> >& argumentList);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

struct SyntaxCallMethod : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Function;
	string Name;
	vector<intrusive_ptr<SyntaxExpression> > ArgumentList;
	SyntaxCallMethod(const intrusive_ptr<SyntaxExpression>& function, const string& name, const vector<intrusive_ptr<SyntaxExpression> >& argumentList);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

struct SyntaxAccessorExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Expression;
	string Name;
	SyntaxAccessorExpression(const intrusive_ptr<SyntaxExpression>& expression, const string& name);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
	void setValue(SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment);
};

struct SyntaxIndexExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Expression;
	intrusive_ptr<SyntaxExpression> Index;
	SyntaxIndexExpression(const intrusive_ptr<SyntaxExpression>& expression, const intrusive_ptr<SyntaxExpression>& index);
};

enum SyntaxPostfix {
	spInc,
	spDec
};

struct SyntaxPostfixExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Expression;
	SyntaxPostfix Operation;
	SyntaxPostfixExpression(const intrusive_ptr<SyntaxExpression>& expression, SyntaxPostfix operation);
};

enum SyntaxUnary {
	suInc,
	suDec,
	suPlus,
	suMinus,
	suNot
};

struct SyntaxUnaryExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Expression;
	SyntaxUnary Operation;
	SyntaxUnaryExpression(const intrusive_ptr<SyntaxExpression>& expression, SyntaxUnary operation);
};

struct SyntaxOperatorExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Left, Right;
	SyntaxOperator Operation;
	SyntaxOperatorExpression(const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right, SyntaxOperator operation);
	virtual void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

struct SyntaxAssignmentExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Left, Right;
	SyntaxAssignemnt Operation;
	SyntaxAssignmentExpression(const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right, SyntaxAssignemnt operation);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
	void setValue(SyntaxCompilerResults& results, const ScriptType& expectedType, ScriptType& returnedType, SyntaxAssignemnt assignment);
};

struct SyntaxOrExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Left, Right;
	SyntaxOrExpression(const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

struct SyntaxAndExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Left, Right;
	SyntaxAndExpression(const intrusive_ptr<SyntaxExpression>& left, const intrusive_ptr<SyntaxExpression>& right);
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

struct SyntaxTestExpression : SyntaxExpression {
	intrusive_ptr<SyntaxExpression> Test, True, False;
	SyntaxTestExpression(const intrusive_ptr<SyntaxExpression>& test, 
		const intrusive_ptr<SyntaxExpression>& ifTrue, 
		const intrusive_ptr<SyntaxExpression>& ifFalse);
	virtual void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

#endif // SyntaxExpression_h__
