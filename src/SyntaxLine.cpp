#include "StdAfx.hpp"

SyntaxLine::SyntaxLine() {
	update();
}

void SyntaxLine::update() {
	*this = asCurrentLine;	
}

string SyntaxLine::at() const {
	char buffer[256];
	sprintf(buffer, "%s(%i)", File ? File->Value : "(stdin)", Line);
	return buffer;
}

void SyntaxLine::error(const char* fmt, ...) const {
	char buffer[256];
	va_list list;
	va_start(list, fmt);
	vsprintf(buffer, fmt, list);
	throw std::runtime_error(at() + ": " + buffer);
}

void SyntaxLine::warning(const char* fmt, ...) const {
	char buffer[256];
	va_list list;
	va_start(list, fmt);
	vsprintf(buffer, fmt, list);
	errorf("%s: %s\n", at().c_str(), buffer);
}
