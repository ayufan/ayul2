#ifndef SyntaxLine_h__
#define SyntaxLine_h__

struct SyntaxLine {
	intrusive_ptr<String> File;
	int Line;

	SyntaxLine();
	string at() const;
	void update();
	void error(const char* message, ...) const;
	void warning(const char* message, ...) const;
};

#endif // SyntaxLine_h__
