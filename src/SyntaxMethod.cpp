#include "StdAfx.hpp"

SyntaxMethod::SyntaxMethod( const intrusive_ptr<SyntaxType>& returnType, const string& name, const vector<intrusive_ptr<SyntaxVariable> >& arguments, const intrusive_ptr<SyntaxStatement>& statement /*= intrusive_ptr<SyntaxStatement>()*/ ) : SyntaxNamedObject(name), ReturnType(returnType), Arguments(arguments), Statement(statement)
{
}

SyntaxClassMethod::SyntaxClassMethod( const SyntaxMethod& method, bool isStatic, SyntaxVisiblity visibility ) : SyntaxMethod(method), IsStatic(isStatic), Visibility(visibility)
{
}

void SyntaxClassMethod::addLocalObjects(ScriptObject& module) {
	Method = new ScriptClassMethod(Name);
	Method->IsStatic = IsStatic;
	Method->Visiblity = Visibility;
	if(ReturnType) {
		ReturnType->resolveType(module, Method->ReturnType);
		if(Method->ReturnType.isStatic())
			error("type can't be static");
	}

	Method->Arguments.resize(Arguments.size());

	for(unsigned i = 0; i < Arguments.size(); ++i) {
		Method->Arguments[i].Name = Arguments[i]->Name;
		if(Arguments[i]->Type) {
			Arguments[i]->Type->resolveType(module, Method->Arguments[i].Type);
			if(Method->Arguments[i].Type.isStatic())
				error("type can't be static");
		}
		if(Arguments[i]->Value)
			Arguments[i]->Value->setValue(module, Method->Arguments[i].Value);
	}

	Method->Function = new ScriptScriptFunction(Method->ReturnType, Method->Arguments, Method.get(), IsStatic ? ScriptFunction::Static : ScriptFunction::Method);

	module.addObject(Method);
}

void SyntaxClassMethod::addCodeObjects(ScriptObject& module) 
{
	if(Method)
	{
		Method->Function->compile(Statement, module);
	}
}

SyntaxClassConstructor::SyntaxClassConstructor( const SyntaxMethod& method, bool isStatic, SyntaxVisiblity visibility ) : SyntaxClassMethod(method, isStatic, visibility)
{
}

SyntaxClassConstructor::SyntaxClassConstructor( const string& name, bool isStatic, SyntaxVisiblity visibility ) : SyntaxClassMethod(SyntaxMethod(NULL, name, vector<intrusive_ptr<SyntaxVariable> >(), new SyntaxConstructorStatementList()), isStatic, visibility)
{
}

void SyntaxClassConstructor::addLocalObjects( ScriptObject& module )
{
	if(Name != module.name())
	{
		error("constructor method have to has the same name as class");
	}

	if(IsStatic) 
	{
		if(Arguments.size())
		{
			error("static constructor can't have arguments");
		}
	}

	Name = CTOR_NAME;

	SyntaxClassMethod::addLocalObjects(module);
}
