#ifndef SyntaxMethod_h__
#define SyntaxMethod_h__

struct SyntaxMethod : SyntaxNamedObject {
	intrusive_ptr<SyntaxType> ReturnType;
	vector<intrusive_ptr<SyntaxVariable> > Arguments;
	intrusive_ptr<SyntaxStatement> Statement;
	SyntaxMethod(const intrusive_ptr<SyntaxType>& returnType, 
		const string& name,
		const vector<intrusive_ptr<SyntaxVariable> >& arguments,
		const intrusive_ptr<SyntaxStatement>& statement = intrusive_ptr<SyntaxStatement>());
};

struct SyntaxClassMethod : SyntaxMethod {
	bool IsStatic;
	SyntaxVisiblity Visibility;
	intrusive_ptr<ScriptClassMethod> Method;

	SyntaxClassMethod(const SyntaxMethod& method, bool isStatic, SyntaxVisiblity visibility);

	void addLocalObjects(ScriptObject& module);
	void addCodeObjects(ScriptObject& module);

	SyntaxVisiblity visibility() const { return Visibility; }
	bool isStatc() const { return IsStatic; }
};

struct SyntaxClassConstructor : SyntaxClassMethod {
	SyntaxClassConstructor(const SyntaxMethod& method, bool isStatic, SyntaxVisiblity visibility);
	SyntaxClassConstructor(const string& name, bool isStatic, SyntaxVisiblity visibility);

	void addLocalObjects(ScriptObject& module);
};


#endif // SyntaxMethod_h__
