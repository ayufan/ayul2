#include "StdAfx.hpp"

SyntaxObject::SyntaxObject() {
}

SyntaxNamedObject::SyntaxNamedObject( const string& name ) : Name(name) {
}

void SyntaxNamedObject::addGlobalObjects( ScriptObject& module ) {
}

void SyntaxNamedObject::addLocalObjects( ScriptObject& module ) {
}

void SyntaxNamedObject::addCodeObjects( ScriptObject& module ) {
}

bool SyntaxNamedObject::isStatc() const {
	return false;
}

SyntaxVisiblity SyntaxNamedObject::visibility() const { 
	return svNone;
}

SyntaxModuleObject::SyntaxModuleObject( const string& name ) : SyntaxNamedObject(name)
{
}
