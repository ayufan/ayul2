#ifndef SyntaxObject_h__
#define SyntaxObject_h__

class SyntaxObject : public Object, public SyntaxLine {
protected:
	SyntaxObject();
};

struct SyntaxNamedObject : SyntaxObject {
	string Name;
	SyntaxNamedObject(const string& name);

	virtual void addGlobalObjects(ScriptObject& module);
	virtual void addLocalObjects(ScriptObject& module);
	virtual void addCodeObjects(ScriptObject& module);

	virtual SyntaxVisiblity visibility() const;
	virtual bool isStatc() const;
};

struct SyntaxModuleObject : SyntaxNamedObject {
	SyntaxModuleObject(const string& name);
};

#endif // SyntaxObject_h__
