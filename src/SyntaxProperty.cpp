#include "StdAfx.hpp"

SyntaxProperty::SyntaxProperty( const intrusive_ptr<SyntaxType>& type, const string& name, const intrusive_ptr<SyntaxStatement>& getNode, const intrusive_ptr<SyntaxStatement>& setNode ) : SyntaxNamedObject(name), Type(type), GetNode(getNode), SetNode(setNode)
{
}

SyntaxClassProperty::SyntaxClassProperty( const SyntaxProperty& property_, bool isStatic, SyntaxVisiblity visibility ) : SyntaxProperty(property_), IsStatic(isStatic), Visibility(visibility)
{
}

void SyntaxClassProperty::addLocalObjects(ScriptObject& module) {
	Property = new ScriptClassProperty(Name);
	Property->IsStatic = IsStatic;
	Property->Visiblity = Visibility;
	if(Type) {
		Type->resolveType(module, Property->Type);
		if(Property->Type.isStatic())
			error("type can't be static");
	}
	if(GetNode) {
		Property->GetFunction = new ScriptScriptFunction(Property->Type, vector<ScriptVariable>(), Property.get(), IsStatic ? ScriptFunction::Static : ScriptFunction::Method);
	}
	if(SetNode) {
		Property->SetFunction = new ScriptScriptFunction(ScriptType(), vector<ScriptVariable>(1, ScriptVariable(Property->Type, "value")), Property.get(), IsStatic ? ScriptFunction::Static : ScriptFunction::Method);
	}
	module.addObject(Property);
}

void SyntaxClassProperty::addCodeObjects(ScriptObject& module) {
	if(GetNode) {
		Property->GetFunction->compile(GetNode, module);
	}
	if(SetNode) {
		Property->SetFunction->compile(SetNode, module);
	}
}
