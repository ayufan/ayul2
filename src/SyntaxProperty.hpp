#ifndef SyntaxProperty_h__
#define SyntaxProperty_h__

struct SyntaxProperty : SyntaxNamedObject {
	intrusive_ptr<SyntaxType> Type;
	intrusive_ptr<SyntaxStatement> GetNode, SetNode;
	SyntaxProperty(const intrusive_ptr<SyntaxType>& type, 
		const string& name,
		const intrusive_ptr<SyntaxStatement>& getNode,
		const intrusive_ptr<SyntaxStatement>& setNode);
};

struct SyntaxClassProperty : SyntaxProperty {
	bool IsStatic;
	SyntaxVisiblity Visibility;

	intrusive_ptr<ScriptClassProperty> Property;

	SyntaxClassProperty(const SyntaxProperty& property_, bool isStatic, SyntaxVisiblity visibility);

	void addLocalObjects(ScriptObject& module);
	void addCodeObjects(ScriptObject& module);

	SyntaxVisiblity visibility() const { return Visibility; }
	bool isStatc() const { return IsStatic; }
};

#endif // SyntaxProperty_h__
