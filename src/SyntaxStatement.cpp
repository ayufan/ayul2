#include "StdAfx.hpp"

SyntaxStatement::SyntaxStatement()
{
}

void SyntaxStatement::compile( SyntaxCompilerResults& results ) {
	error("not supported");
}

SyntaxStatementList::SyntaxStatementList( const vector<intrusive_ptr<SyntaxStatement> >& statementList ) : StatementList(statementList)
{
}

SyntaxStatementList::SyntaxStatementList()
{
}

void SyntaxStatementList::compile(SyntaxCompilerResults& results) 
{
	SyntaxCompilerResults local(results);

	// Process each statement
	for(unsigned i = 0; i < StatementList.size(); ++i) {
		// Compile sub-statements
		StatementList[i]->compile(local);

		// Returned data (rest of statements is unreachable)
		if(local.Returned) {
			break;
		}
	}

	local.pushLabel(local.LeaveLabel);

	results.OpList += local.OpList;
	results.Returned = local.Returned;
}

SyntaxReturnStatement::SyntaxReturnStatement( const intrusive_ptr<SyntaxExpression>& expression ) : Expression(expression)
{
}

void SyntaxReturnStatement::compile(SyntaxCompilerResults& results) {
	SyntaxCompilerResults lresults(results);

	// Compile expression
	ScriptType returnedType;
	if(Expression)
		Expression->getValue(lresults, returnedType);

	// Convert type
	if(lresults.Context.ReturnType.isValid()) {
		if(!returnedType.isValid())
			error("expression doesn't return value");

		// convert type
		lresults.expectType(returnedType, lresults.Context.ReturnType);
	
		// Add return statement
		lresults.pushCode(OpCode::Return, results[lresults.Context.ReturnType]);
	}
	
	// Check if expression returned something
	else if(returnedType.isValid())
		error("function doesn't return value");

	// Add return without return type
	else
		lresults.pushCode(OpCode::Return, ~0);

	// mark as returned
	results.OpList += lresults.OpList;
	results.Returned = true;
}

SyntaxIfStatement::SyntaxIfStatement( const intrusive_ptr<SyntaxExpression>& test, const intrusive_ptr<SyntaxStatement>& ifTrue, const intrusive_ptr<SyntaxStatement>& ifFalse ) : Test(test), True(ifTrue), False(ifFalse)
{
}

void SyntaxIfStatement::compile( SyntaxCompilerResults& results )
{
	SyntaxCompilerResults testResults(results);
	SyntaxCompilerResults trueResults(testResults);
	SyntaxCompilerResults falseResults(testResults);

	// Get statement value
	SyntaxCompilerResults lresults(testResults);
	ScriptType returnedType;
	Test->getValue(lresults, returnedType);
	lresults.expectType(returnedType, ScriptType(Boolean::id$()));
	testResults.OpList += lresults.OpList;

	// Check statement value
	testResults.pushCode(OpCode::JumpIfFalse, trueResults.LeaveLabel);

	// Compile left expression
	True->compile(trueResults);
	trueResults.pushCode(OpCode::Jump, falseResults.LeaveLabel);
	trueResults.pushLabel(trueResults.LeaveLabel);	
	testResults.OpList += trueResults.OpList;

	// Compile right expression
	if(False)
		False->compile(falseResults);
	falseResults.pushLabel(falseResults.LeaveLabel);
	testResults.OpList += falseResults.OpList;
	
	// Save return value
	results.Returned = trueResults.Returned && (!False || falseResults.Returned);
	results.OpList += testResults.OpList;
}

SyntaxWhileStatement::SyntaxWhileStatement( const intrusive_ptr<SyntaxExpression>& test, const intrusive_ptr<SyntaxStatement>& loop ) : Test(test), Loop(loop)
{
}

void SyntaxWhileStatement::compile( SyntaxCompilerResults& results )
{
	SyntaxCompilerResults lresults(results);
	lresults.ContinueLabel = lresults.allocLabel();
	lresults.BreakLabel = lresults.allocLabel();
	SyntaxCompilerResults testResults(lresults);

	// Compile test expression
	ScriptType testType;
	testResults.pushLabel(testResults.ContinueLabel);
	Test->getValue(testResults, testType);
	testResults.expectType(testType, ScriptType(Boolean::id$()));
	testResults.pushCode(OpCode::JumpIfFalse, testResults.BreakLabel);
	lresults.OpList += testResults.OpList;

	// Compile loop expression
	Loop->compile(lresults);
	lresults.pushCode(OpCode::Jump, lresults.ContinueLabel);
	lresults.pushLabel(lresults.BreakLabel);

	// Save results
	results.OpList += lresults.OpList;
	results.Returned = lresults.Returned;
}

SyntaxDoWhileStatement::SyntaxDoWhileStatement( const intrusive_ptr<SyntaxExpression>& test, const intrusive_ptr<SyntaxStatement>& loop ) : Test(test), Loop(loop)
{
}

void SyntaxDoWhileStatement::compile( SyntaxCompilerResults& results )
{
	SyntaxCompilerResults lresults(results);
	lresults.ContinueLabel = lresults.allocLabel();
	lresults.BreakLabel = lresults.allocLabel();
	SyntaxCompilerResults testResults(lresults);

	// Compile loop expression
	lresults.pushLabel(lresults.ContinueLabel);
	Loop->compile(lresults);

	// Compile test expression
	ScriptType testType;
	Test->getValue(testResults, testType);
	testResults.expectType(testType, ScriptType(Boolean::id$()));
	testResults.pushCode(OpCode::JumpIfTrue, testResults.ContinueLabel);
	testResults.pushLabel(lresults.BreakLabel);
	lresults.OpList += testResults.OpList;

	// Save results
	results.OpList += lresults.OpList;
	results.Returned = lresults.Returned;
}

SyntaxForStatement::SyntaxForStatement( const intrusive_ptr<SyntaxExpression>& init, const intrusive_ptr<SyntaxExpression>& test, const intrusive_ptr<SyntaxExpression>& next, const intrusive_ptr<SyntaxStatement>& loop ) : Init(init), Test(test), Next(next), Loop(loop)
{
}

SyntaxForeachStatement::SyntaxForeachStatement( const intrusive_ptr<SyntaxExpression>& expression, const string& identifier, const intrusive_ptr<SyntaxStatement>& loop ) : Expression(expression), Identifier(identifier), Loop(loop)
{
}

SyntaxCatchStatement::SyntaxCatchStatement( const intrusive_ptr<SyntaxVariable>& variable, const intrusive_ptr<SyntaxStatement>& statement ) : Variable(variable), Statement(statement)
{
}

SyntaxTryCatchStatement::SyntaxTryCatchStatement( const intrusive_ptr<SyntaxStatement>& try_, const vector<intrusive_ptr<SyntaxCatchStatement> >& catchList, const intrusive_ptr<SyntaxStatement>& finally_ ) : Try(try_), CatchList(catchList), Finally(finally_)
{
}

SyntaxCaseStatement::SyntaxCaseStatement( const intrusive_ptr<SyntaxStatement>& statement ) : Statement(statement)
{
}

SyntaxDefaultCaseStatement::SyntaxDefaultCaseStatement( const intrusive_ptr<SyntaxStatement>& statement ) : SyntaxCaseStatement(statement)
{
}

SyntaxSwitchStatement::SyntaxSwitchStatement( const intrusive_ptr<SyntaxExpression>& value, const vector<intrusive_ptr<SyntaxCaseStatement> >& caseList ) : Value(value), CaseList(caseList)
{
}

SyntaxValueCaseStatement::SyntaxValueCaseStatement( int value, const intrusive_ptr<SyntaxStatement>& statement ) : SyntaxCaseStatement(statement), Value(value)
{
}

SyntaxThrowStatement::SyntaxThrowStatement( const intrusive_ptr<SyntaxExpression>& expression ) : Expression(expression)
{
}

void SyntaxNopStatement::compile(SyntaxCompilerResults& results) 
{
}

SyntaxNopStatement::SyntaxNopStatement()
{
}

void SyntaxContinueStatement::compile( SyntaxCompilerResults& results )
{
	if(results.ContinueLabel == ~0)
		error("no continue in this context");
	results.pushCode(OpCode::Jump, results.ContinueLabel);
}

void SyntaxBreakStatement::compile( SyntaxCompilerResults& results )
{
	if(results.BreakLabel == ~0)
		error("no break in this context");
	results.pushCode(OpCode::Jump, results.BreakLabel);
}

SyntaxConstructorStatementList::SyntaxConstructorStatementList( const vector<intrusive_ptr<SyntaxStatement> >& statementList ) : SyntaxStatementList(statementList)
{
}

SyntaxConstructorStatementList::SyntaxConstructorStatementList()
{
}

void SyntaxConstructorStatementList::compile( SyntaxCompilerResults& results )
{
	for(unsigned i = 0; i < results.Context.Object.objectCount(); ++i) 
	{
		if(intrusive_ptr<ScriptClassVariable> variable = results.Context.Object.object(i)->as<ScriptClassVariable>())
		{
			if(results.Context.ContextType.isStatic() != variable->isStatic())
				continue;
			if(variable->InitializationList.empty())
				continue;

			results.OpList += variable->InitializationList;
		}

nextVariable:;
	}

	SyntaxStatementList::compile(results);
}
