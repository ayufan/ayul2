#ifndef SyntaxStatement_h__
#define SyntaxStatement_h__

struct SyntaxVariable;

struct SyntaxStatement : SyntaxObject {
	SyntaxStatement();

	virtual void compile(SyntaxCompilerResults& results);
};

struct SyntaxNopStatement : SyntaxStatement {
	SyntaxNopStatement();

	void compile(SyntaxCompilerResults& results);
};

struct SyntaxStatementList : SyntaxStatement {
	vector<intrusive_ptr<SyntaxStatement> > StatementList;
	SyntaxStatementList(const vector<intrusive_ptr<SyntaxStatement> >& statementList);
	SyntaxStatementList();
	void compile(SyntaxCompilerResults& results);
};

struct SyntaxConstructorStatementList : SyntaxStatementList {
	SyntaxConstructorStatementList(const vector<intrusive_ptr<SyntaxStatement> >& statementList);
	SyntaxConstructorStatementList();
	void compile(SyntaxCompilerResults& results);
};

struct SyntaxReturnStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Expression;
	SyntaxReturnStatement(const intrusive_ptr<SyntaxExpression>& expression);
	void compile(SyntaxCompilerResults& results);
};

struct SyntaxContinueStatement : SyntaxStatement {
	void compile(SyntaxCompilerResults& results);
};

struct SyntaxBreakStatement : SyntaxStatement {
	void compile(SyntaxCompilerResults& results);
};

struct SyntaxIfStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Test;
	intrusive_ptr<SyntaxStatement> True;
	intrusive_ptr<SyntaxStatement> False;
	SyntaxIfStatement(const intrusive_ptr<SyntaxExpression>& test, 
		const intrusive_ptr<SyntaxStatement>& ifTrue, 
		const intrusive_ptr<SyntaxStatement>& ifFalse);
	virtual void compile(SyntaxCompilerResults& results);
};

struct SyntaxWhileStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Test;
	intrusive_ptr<SyntaxStatement> Loop;
	SyntaxWhileStatement(const intrusive_ptr<SyntaxExpression>& test, 
		const intrusive_ptr<SyntaxStatement>& loop);
	void compile(SyntaxCompilerResults& results);
};

struct SyntaxDoWhileStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Test;
	intrusive_ptr<SyntaxStatement> Loop;
	SyntaxDoWhileStatement(const intrusive_ptr<SyntaxExpression>& test, 
		const intrusive_ptr<SyntaxStatement>& loop);
	void compile(SyntaxCompilerResults& results);
};

struct SyntaxForStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Init;
	intrusive_ptr<SyntaxExpression> Test;
	intrusive_ptr<SyntaxExpression> Next;
	intrusive_ptr<SyntaxStatement> Loop;
	SyntaxForStatement(const intrusive_ptr<SyntaxExpression>& init, 
		const intrusive_ptr<SyntaxExpression>& test, 
		const intrusive_ptr<SyntaxExpression>& next, 
		const intrusive_ptr<SyntaxStatement>& loop);
};

struct SyntaxForeachStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Expression;
	string Identifier;
	intrusive_ptr<SyntaxStatement> Loop;
	SyntaxForeachStatement(const intrusive_ptr<SyntaxExpression>& expression, 
		const string& identifier,
		const intrusive_ptr<SyntaxStatement>& loop);
};

struct SyntaxCatchStatement : SyntaxStatement {
	intrusive_ptr<SyntaxVariable> Variable;
	intrusive_ptr<SyntaxStatement> Statement;
	SyntaxCatchStatement(const intrusive_ptr<SyntaxVariable>& variable, 
		const intrusive_ptr<SyntaxStatement>& statement);
};

struct SyntaxTryCatchStatement : SyntaxStatement {
	intrusive_ptr<SyntaxStatement> Try;
	vector<intrusive_ptr<SyntaxCatchStatement> > CatchList;
	intrusive_ptr<SyntaxStatement> Finally;
	SyntaxTryCatchStatement(const intrusive_ptr<SyntaxStatement>& try_, 
		const vector<intrusive_ptr<SyntaxCatchStatement> >& catchList,
		const intrusive_ptr<SyntaxStatement>& finally_);
};

struct SyntaxCaseStatement : SyntaxStatement {
	intrusive_ptr<SyntaxStatement> Statement;
	SyntaxCaseStatement(const intrusive_ptr<SyntaxStatement>& statement);
};

struct SyntaxValueCaseStatement : SyntaxCaseStatement {
	int Value;
	SyntaxValueCaseStatement(int value, const intrusive_ptr<SyntaxStatement>& statement);
};

struct SyntaxDefaultCaseStatement : SyntaxCaseStatement {
	SyntaxDefaultCaseStatement(const intrusive_ptr<SyntaxStatement>& statement);
};

struct SyntaxSwitchStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Value;
	vector<intrusive_ptr<SyntaxCaseStatement> > CaseList;
	SyntaxSwitchStatement(const intrusive_ptr<SyntaxExpression>& value,
		const vector<intrusive_ptr<SyntaxCaseStatement> >& caseList);
};

struct SyntaxThrowStatement : SyntaxStatement {
	intrusive_ptr<SyntaxExpression> Expression;
	SyntaxThrowStatement(const intrusive_ptr<SyntaxExpression>& expression);
};

#endif // SyntaxStatement_h__
