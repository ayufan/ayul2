#include "StdAfx.hpp"

SyntaxNameType::SyntaxNameType( const string& name ) : Name(name)
{
	if(name == "int")
		Name = "Int32";
	else if(name == "float")
		Name = "Float32";
	else if(name == "bool")
		Name = "Boolean";
	else if(name == "string")
		Name = "String";
}

void SyntaxNameType::resolveType( ScriptObject& module, ScriptType& type ) const
{
	intrusive_ptr<ScriptObject> object = module.findObjectWithParent<ScriptObject>(Name);
	if(object && (object->isValue() || object->isObject())) 
	{
		type = object;
		return;
	}
	error("type '%s' not found", Name.c_str());
	throw 0;
}

void SyntaxObjectType::resolveType( ScriptObject& module, ScriptType& type ) const
{
	type = Object::id$();
}

SyntaxArrayType::SyntaxArrayType( const intrusive_ptr<SyntaxType>& baseType ) : BaseType(baseType)
{
}

SyntaxRefType::SyntaxRefType( const intrusive_ptr<SyntaxType>& baseType ) : BaseType(baseType)
{
}

void SyntaxRefType::resolveType( ScriptObject& module, ScriptType& type ) const
{
	BaseType->resolveType(module, type);
	type.IsRef = true;
}

SyntaxEventType::SyntaxEventType( const intrusive_ptr<SyntaxType>& baseType ) : BaseType(baseType)
{
}

SyntaxOfType::SyntaxOfType( const intrusive_ptr<SyntaxType>& baseType, const string& name ) : SyntaxNameType(name), BaseType(baseType)
{
}

void SyntaxOfType::resolveType( ScriptObject& module, ScriptType& type ) const
{
	ScriptType baseType;
	BaseType->resolveType(module, baseType);

	if(intrusive_ptr<ScriptObject> module = baseType.findModuleObject(Name)) 
	{
		if(module->isValue() || module->isObject())
		{
			type = module;
		}
		else
		{
			error("invalid type");
		}
	}
	else 
	{
		error("type not found");
	}
}

void SyntaxType::resolveType( ScriptObject& module, ScriptType& type ) const
{
	error("not supported");
}
