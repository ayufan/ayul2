#ifndef SyntaxType_h__
#define SyntaxType_h__

struct SyntaxType : SyntaxObject {
	virtual void resolveType(ScriptObject& module, ScriptType& type) const;
};

struct SyntaxNameType : SyntaxType {
	string Name;
	SyntaxNameType(const string& name);
	void resolveType(ScriptObject& module, ScriptType& type) const;
};

struct SyntaxObjectType : SyntaxType {
	void resolveType(ScriptObject& module, ScriptType& type) const;
};

struct SyntaxArrayType : SyntaxType {
	intrusive_ptr<SyntaxType> BaseType;
	SyntaxArrayType(const intrusive_ptr<SyntaxType>& baseType);
};

struct SyntaxRefType : SyntaxType {
	intrusive_ptr<SyntaxType> BaseType;
	SyntaxRefType(const intrusive_ptr<SyntaxType>& baseType);
	void resolveType(ScriptObject& module, ScriptType& type) const;
};

struct SyntaxEventType : SyntaxType {
	intrusive_ptr<SyntaxType> BaseType;
	SyntaxEventType(const intrusive_ptr<SyntaxType>& baseType);
};

struct SyntaxOfType : SyntaxNameType {
	intrusive_ptr<SyntaxType> BaseType;
	SyntaxOfType(const intrusive_ptr<SyntaxType>& baseType, const string& name);
	void resolveType(ScriptObject& module, ScriptType& type) const;
};

#endif // SyntaxType_h__
