#include "StdAfx.hpp"

void SyntaxValue::setValue( ScriptObject& module, ScriptValue& value )
{
	error("value not supported");
}

void SyntaxValue::setValue( SyntaxCompilerResults& results, const ScriptType& type, ScriptType& returnedType, SyntaxAssignemnt assignment )
{
	error("value not supported");
}

void SyntaxValue::getValue( SyntaxCompilerResults& results, ScriptType& type )
{
	error("value not supported");
}

void SyntaxStringValue::setValue( ScriptObject& module, ScriptValue& value )
{
	value.Type = ScriptValue::svtString;
	value.StringValue = Value;
}

SyntaxStringValue::SyntaxStringValue( const string& value ) : Value(value)
{
}

void SyntaxStringValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	results.pushCode(OpCode::PushString, results.Context.Module.addData(Value), Value.size());
	returnedType = String::id$();
}

void SyntaxIntValue::setValue( ScriptObject& module, ScriptValue& value )
{
	value.Type = ScriptValue::svtInt;
	value.IntValue = Value;
}

void SyntaxIntValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	returnedType = Int32::id$();
	results.pushCode(OpCode::PushValueType, results[returnedType], results.Context.Module.addData(&Value, sizeof(Value)), sizeof(Value));
}

SyntaxIntValue::SyntaxIntValue( int value ) : Value(value)
{
}

void SyntaxFloatValue::setValue( ScriptObject& module, ScriptValue& value )
{
	value.Type = ScriptValue::svtFloat;
	value.FloatValue = Value;
}

void SyntaxFloatValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	returnedType = Float32::id$();
	results.pushCode(OpCode::PushValueType, results[returnedType], results.Context.Module.addData(&Value, sizeof(Value)), sizeof(Value));
}

SyntaxFloatValue::SyntaxFloatValue( float value ) : Value(value)
{
}

void SyntaxBooleanValue::setValue( ScriptObject& module, ScriptValue& value )
{
	value.Type = ScriptValue::svtBoolean;
	value.BooleanValue = Value;
}

void SyntaxBooleanValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	returnedType = Boolean::id$();
	results.pushCode(OpCode::PushValueType, results[returnedType], results.Context.Module.addData(&Value, sizeof(Value)), sizeof(Value));
}

SyntaxBooleanValue::SyntaxBooleanValue( bool value ) : Value(value)
{
}

void SyntaxNullValue::setValue( ScriptObject& module, ScriptValue& value )
{
	value.Type = ScriptValue::svtNull;
}

void SyntaxNullValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	results.pushCode(OpCode::PushNull);
	returnedType = ScriptType(intrusive_ptr<ScriptClass>());
}

SyntaxNewValue::SyntaxNewValue( const intrusive_ptr<SyntaxType>& type ) : Type(type)
{
}

void SyntaxNewValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	Type->resolveType(*results.Context.ContextType.Object, returnedType);

	if(intrusive_ptr<ScriptFunction> function = returnedType.findConstructor()) {
		results.pushCode(OpCode::CallConstructor, results[function], results[returnedType], results.allocTempVariable(returnedType));

		if(returnedType.isValue())
			returnedType.IsRef = true;
	}
	else {
		error("constructor not found");
	}
}

SyntaxNewObjectValue::SyntaxNewObjectValue( const intrusive_ptr<SyntaxType>& type, const vector<intrusive_ptr<SyntaxExpression> >& arguments ) : SyntaxNewValue(type), Arguments(arguments)
{
}

void SyntaxNewObjectValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	Type->resolveType(*results.Context.ContextType.Object, returnedType);

	// Push arguments in reverse order
	vector<ScriptType> argumentList(Arguments.size());
	for(unsigned i = Arguments.size(); i-- > 0; ) {
		Arguments[i]->getValue(results, argumentList[i]);
	}

	// Find constructor
	if(intrusive_ptr<ScriptFunction> function = returnedType.findConstructor(argumentList)) {
		results.pushCode(OpCode::CallConstructor, results[function], results[returnedType], results.allocTempVariable(returnedType));

		if(returnedType.isValue())
			returnedType.IsRef = true;
	}
	else {
		error("constructor not found");
	}
}

SyntaxNewArrayValue::SyntaxNewArrayValue( const intrusive_ptr<SyntaxType>& type, const vector<intrusive_ptr<SyntaxExpression> >& values ) : SyntaxNewValue(type), Values(values)
{
}

SyntaxIdentifierValue::SyntaxIdentifierValue( const string& identifier ) : Identifier(identifier)
{
}

void SyntaxIdentifierValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	// Find local variable
	ScriptLocalVariable* variable = results.findVariable(Identifier);
	if(variable) 
	{
		// Returning object contains reference to object
		returnedType = variable->Type;
		returnedType.IsRef = returnedType.isValue();

		// Push either argument or local variable
		if(variable->isArgument()) 
		{
			results.pushCode(OpCode::PushArgument, variable - &results.Context.VariableList[0]);
		}
		else 
		{
			results.pushCode(OpCode::PushLocal, variable - &results.Context.VariableList[0]);
		}
		return;
	}

	// Find class member
	if(intrusive_ptr<ScriptObject> member = results.Context.ContextType.findObject(Identifier, true)) 
	{
		if(member->is<ScriptClassVariable>()) 
		{
			ScriptClassVariable& variable = *member->as$<ScriptClassVariable>();

			// Returning object contains reference to object
			returnedType = variable.Type;
			returnedType.IsRef = returnedType.isValue();

			// Push static variable
			if(variable.IsStatic) 
			{
				results.pushCode(OpCode::PushStatic, results[&variable]);
				return;
			}

			// Push this variable
			else 
			{
				results.pushCode(OpCode::PushThis);
				results.pushCode(OpCode::PushOffset, results[&variable], results[returnedType]);
			}
			return;
		}

		// Find class get property
		else if(member && member->is<ScriptClassProperty>() && member->as$<ScriptClassProperty>()->getFunction()) 
		{
			intrusive_ptr<ScriptFunction> function = member->as$<ScriptClassProperty>()->getFunction();

			// Returning object contains reference to object
			returnedType = function->returnType();
			if(returnedType.isValue())
				returnedType.IsRef = true;
			
			// Call static method
			if(function->isStaticFunction()) 
			{
				results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));
				return;
			}

			// Call this method
			else 
			{
				results.pushCode(OpCode::PushThis, results[function]);
				results.pushCode(OpCode::CallMethod, results[function], results.allocTempVariable(function->returnType()));
			}
			return;
		}

		// Get address of method
		else if(intrusive_ptr<ScriptClassMethod> method = member->as<ScriptClassMethod>())
		{
			if(intrusive_ptr<ScriptFunction> function = method->function())
			{
				returnedType = function;

				if(function->isStaticFunction())
				{
					// Get this pointer
					results.pushCode(OpCode::PushNull);
					results.pushCode(OpCode::PushFunction, results[function]);
				}
				else
				{
					// Push opcode
					results.pushCode(OpCode::PushThis);
					results.pushCode(OpCode::PushFunction, results[function]);
				}
				return;
			}
			else
			{
				error("method not found");
			}
		}

		else if(intrusive_ptr<ScriptObject> object = member->as<ScriptObject>()) 
		{
			returnedType = object;
			returnedType.setStatic();
			return;
		}
	}

	error("not found");
}

void SyntaxIdentifierValue::setValue( SyntaxCompilerResults& results, const ScriptType& type, ScriptType& returnedType, SyntaxAssignemnt assignment )
{
	returnedType = ScriptType();

	// Find local variable
	ScriptLocalVariable* variable = results.findVariable(Identifier);
	if(variable) {
		// Use assignment operator
		if(intrusive_ptr<ScriptFunction> function = variable->Type.findOperator(assignment, type.noRef())) {
			// Push value to operator function
			results.expectType(type, function->argument(1).Type.noRef());

			// Save either as argument or local variable
			if(variable->isArgument()) {
				results.pushCode(OpCode::PushArgument, variable - &results.Context.VariableList[0]);
			}
			else {
				results.pushCode(OpCode::PushLocal, variable - &results.Context.VariableList[0]);
			}

			// Execute function
			results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));

			// Discard return value
			returnedType = function->returnType();
			if(returnedType.isValue())
				returnedType.IsRef = true;
		}
		else {
			if(assignment != saNormal)
				error("operator not supported");

			// Convert type to desired type
			results.expectType(type, variable->Type.noRef());

			// Save either as argument or local variable
			if(variable->isArgument()) {
				results.pushCode(OpCode::PopArgument, variable - &results.Context.VariableList[0]);
			}
			else {
				results.pushCode(OpCode::PopLocal, variable - &results.Context.VariableList[0]);
			}
		}
		return;
	}

	// Find class member
	if(intrusive_ptr<ScriptObject> member = results.Context.ContextType.findObject(Identifier)) {
		if(member->is<ScriptClassVariable>()) {
			ScriptClassVariable& variable = *member->as$<ScriptClassVariable>();

			// Use assignment operator
			if(intrusive_ptr<ScriptFunction> function = variable.Type.findOperator(assignment, type.noRef())) {
				// Push value to operator function
				results.expectType(type, function->argument(1).Type.noRef());

				// Save as static variable
				if(variable.IsStatic) {
					results.pushCode(OpCode::PushStatic, results[&variable]);
				}

				// Save as class variable
				else {
					results.pushCode(OpCode::PushThis);
					results.pushCode(OpCode::PushOffset, results[&variable], results[results.Context.ContextType]);
				}

				// Execute function
				results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));

				ScriptType otherType = function->returnType();
				if(otherType.isValue())
					otherType.IsRef = true;

				// Discard return value
				results.discardType(otherType);
			}
			else {
				if(assignment != saNormal)
					error("operator not supported");

				// Convert type to desired type
				results.expectType(type, variable.Type.noRef());

				// Save as static variable
				if(variable.IsStatic) {
					results.pushCode(OpCode::PopStatic, results[&variable]);
					return;
				}

				// Save as class variable
				else {
					results.pushCode(OpCode::PushThis);
					results.pushCode(OpCode::PopOffset, results[&variable], results[results.Context.ContextType]);
				}
			}
			return;
		}

		// Find class get property
		else if(member && member->is<ScriptClassProperty>() && member->as$<ScriptClassProperty>()->setFunction()) {
			intrusive_ptr<ScriptFunction> function = member->as$<ScriptClassProperty>()->setFunction();

			if(assignment != saNormal)
				error("operator not supported");

			// Convert type to desired type
			results.expectType(type, function->argument(0).Type);

			// Call static method
			if(function->isStaticFunction()) {
				results.pushCode(OpCode::Call, results[function], results.allocTempVariable(function->returnType()));
				return;
			}

			// Call this method
			else {
				results.pushCode(OpCode::PushThis, results[function], results.allocTempVariable(function->returnType()));
				results.pushCode(OpCode::CallMethod, results[function], results.allocTempVariable(function->returnType()));
			}

			returnedType = function->returnType();
			if(returnedType.isValue())
				returnedType.IsRef = true;
			return;
		}
	}

	error("not found");
}

void SyntaxThisValue::getValue( SyntaxCompilerResults& results, ScriptType& returnedType )
{
	returnedType = results.Context.ContextType;

	// No this when static
	if(returnedType.isStatic())
		return;

	// Generate PushThis opcode
	if(returnedType.isObject()) {
		results.pushCode(OpCode::PushThis);
	}
	// Generate PushThis opcode and mark as reference
	else if(returnedType.isValue()) {
		returnedType.IsRef = true;
		results.pushCode(OpCode::PushThis);
	}
	else {
		error("not supported");
	}
}
