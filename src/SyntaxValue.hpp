#ifndef SyntaxValue_h__
#define SyntaxValue_h__

struct SyntaxValue : SyntaxObject {
	virtual void setValue(ScriptObject& module, ScriptValue& value);
	virtual void getValue(SyntaxCompilerResults& results, ScriptType& type);
	virtual void setValue(SyntaxCompilerResults& results, const ScriptType& type, ScriptType& returnedType, SyntaxAssignemnt assignment);
};

struct SyntaxStringValue : SyntaxValue {
	string Value;
	SyntaxStringValue(const string& value);
	void setValue(ScriptObject& module, ScriptValue& value);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
};

struct SyntaxIntValue : SyntaxValue {
	int Value;
	SyntaxIntValue(int value);
	void setValue(ScriptObject& module, ScriptValue& value);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
};

struct SyntaxFloatValue : SyntaxValue {
	float Value;
	SyntaxFloatValue(float value);

	void setValue(ScriptObject& module, ScriptValue& value);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
};

struct SyntaxBooleanValue : SyntaxValue {
	bool Value;
	SyntaxBooleanValue(bool value);
	void setValue(ScriptObject& module, ScriptValue& value);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
};

struct SyntaxNullValue : SyntaxValue {
	void setValue(ScriptObject& module, ScriptValue& value);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
};

struct SyntaxNewValue : SyntaxValue {
	intrusive_ptr<SyntaxType> Type;
	SyntaxNewValue(const intrusive_ptr<SyntaxType>& type);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
};

struct SyntaxNewObjectValue : SyntaxNewValue {
	vector<intrusive_ptr<SyntaxExpression> > Arguments;
	SyntaxNewObjectValue(const intrusive_ptr<SyntaxType>& type, const vector<intrusive_ptr<SyntaxExpression> >& arguments);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
};

struct SyntaxNewArrayValue : SyntaxNewValue {
	vector<intrusive_ptr<SyntaxExpression> > Values;
	SyntaxNewArrayValue(const intrusive_ptr<SyntaxType>& type, const vector<intrusive_ptr<SyntaxExpression> >& values);
};

struct SyntaxIdentifierValue : SyntaxValue {
	string Identifier;
	SyntaxIdentifierValue(const string& identifier);
	void getValue(SyntaxCompilerResults& results, ScriptType& type);
	void setValue(SyntaxCompilerResults& results, const ScriptType& type, ScriptType& returnedType, SyntaxAssignemnt assignment);
};

struct SyntaxThisValue : SyntaxValue {
	void getValue(SyntaxCompilerResults& results, ScriptType& returnedType);
};

#endif // SyntaxValue_h__
