#include "StdAfx.hpp"

SyntaxVariable::SyntaxVariable( const string& name, const intrusive_ptr<SyntaxType>& type /*= intrusive_ptr<SyntaxType>()*/, const intrusive_ptr<SyntaxValue>& value /*= intrusive_ptr<SyntaxValue>()*/ ) : Name(name), Type(type), Value(value)
{
}

SyntaxClassVariable::SyntaxClassVariable( const string& name, const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxValue>& value, bool isStatic, SyntaxVisiblity visibility ) : SyntaxNamedObject(name), Type(type), Value(value), IsStatic(isStatic), Visibility(visibility)
{
}

void SyntaxClassVariable::addLocalObjects(ScriptObject& module) {
	intrusive_ptr<ScriptClassVariable> variable(new ScriptClassVariable(Name));
	if(Value) {
		Value->setValue(module, variable->Value);
	}
	variable->IsStatic = IsStatic;
	variable->Visiblity = Visibility;
	if(Type) {
		Type->resolveType(module, variable->Type);
		if(variable->Type.isStatic())
			error("invalid variable type");
	}
	module.addObject(variable);

	// Compile variable
	if(Value) {
		ScriptType contextType(&module, module.isValue());
		ScriptType returnedType;

		// Initialize context
		SyntaxCompilerContext context(ScriptType(), module, *module.module());
		SyntaxCompilerResults results(context);
		Value->getValue(results, returnedType);
		results.expectType(returnedType, variable->Type);
		if(variable->isStatic())
		{
			results.pushCode(OpCode::PopStatic, results[variable]);
		}
		else
		{
			results.pushCode(OpCode::PushThis);
			results.pushCode(OpCode::PopOffset, results[variable], results[contextType]);
		}
		results.pushCode(OpCode::Mark);

		// Save opCodes
		variable->InitializationList = results.OpList;
	}
}

void SyntaxVariable::compile(SyntaxCompilerResults& results) {
	// Check if variable with corresponding name already exists
	if(results.findVariable(Name, false)) {
		error("variable '%s' already exist", Name.c_str());
	}

	// Check type
	if(Type == NULL)
		error("no type specified");

	// Create variable
	ScriptLocalVariable newVariable;
	newVariable.Level = results.Level;
	newVariable.Name = Name;
	Type->resolveType(*results.Context.ContextType.Object, newVariable.Type);
	if(newVariable.Type.isStatic())
		error("invalid variable type");

	// Check type other way
	if(newVariable.Type.IsRef)
		error("ref type not allowed here");
	if(newVariable.Type.IsConst)
		error("const type not allowed here");

	// Append variable
	newVariable.NextVariable = results.VariableIndex;
	results.VariableIndex = results.Context.VariableList.size();
	results.Context.VariableList.push_back(newVariable);

	if(Value) {
		SyntaxCompilerResults lresults(results);
		// Push new variable value
		ScriptType returnedType;
		Value->getValue(lresults, returnedType);
		lresults.expectType(returnedType, newVariable.Type.noRef());
		lresults.pushCode(OpCode::PopLocal, results.VariableIndex);
		lresults.pushCode(OpCode::Mark);
		results.OpList += lresults.OpList;
	}
	else {
		// Zero variable
		results.pushCode(OpCode::ZeroLocal, results.VariableIndex);
	}
}
