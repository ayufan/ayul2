#ifndef SyntaxVariable_h__
#define SyntaxVariable_h__

struct SyntaxVariable : SyntaxStatement {	
	string Name;
	intrusive_ptr<SyntaxType> Type;
	intrusive_ptr<SyntaxValue> Value;

	SyntaxVariable(const string& name, const intrusive_ptr<SyntaxType>& type = intrusive_ptr<SyntaxType>(), const intrusive_ptr<SyntaxValue>& value = intrusive_ptr<SyntaxValue>());

	void compile(SyntaxCompilerResults& results);
};

struct SyntaxClassVariable : SyntaxNamedObject {
	intrusive_ptr<SyntaxType> Type;
	intrusive_ptr<SyntaxValue> Value;
	bool IsStatic;
	SyntaxVisiblity Visibility;
	SyntaxClassVariable(const string& name, const intrusive_ptr<SyntaxType>& type, const intrusive_ptr<SyntaxValue>& value, bool isStatic, SyntaxVisiblity visibility);

	void addLocalObjects(ScriptObject& module);
};

#endif // SyntaxVariable_h__
