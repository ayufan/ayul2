#ifndef Version_h__
#define Version_h__

#define AYUL_MAJOR() 0
#define AYUL_MINOR() 1

static unsigned AYUL_REVISION() {
	const char* revision = strchr("$Rev\t$", '\t');
	return revision ? atoi(revision) : 1;
}

static const char* AYUL_VERSION() {
	static char buffer[20];
	if(!buffer[0])
		sprintf(buffer, "%i.%i.%i", AYUL_MAJOR(), AYUL_MINOR(), AYUL_REVISION());
	return buffer;
}

#endif // Version_h__