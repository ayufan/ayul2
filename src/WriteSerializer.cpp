#include "StdAfx.hpp"

class WriteSerializer : public Serializer
{
	//! List of serialized objects
	vector<Object*> ObjectList;
	vector<SerializerObject> ObjectDescList;
	string ObjectOutput;

public:
	WriteSerializer();
	void saveObject(Object*& object, string& output);

private:
	virtual void serialize(void* data, unsigned size);
	virtual void serializeObject(Object*& object);
};

void WriteSerializer::saveObject( Object*& object, string& output )
{
	ObjectList.resize(0);
	ObjectDescList.resize(0);
	ObjectList.push_back(object);

	SerializerHeader header = {SerializeMagic, ~0, ~0};
	output.append((char*)&header, sizeof(header));

	for(unsigned i = 0; i < ObjectList.size(); ++i)
	{
		ObjectOutput.resize(0);
		ObjectList[i]->serialize(*this);
		SerializerObject desc = {ObjectList[i]->id()->hash(), Output.size(), ObjectOutput.size()};
		ObjectDescList.push_back(desc);
		output += ObjectOutput;
	}

	if(ObjectDescList.size())
	{
		SerializerHeader* header = (SerializerHeader*)&output[0];
		header->Offset = output.size();
		header->Count = ObjectDescList.size();
		output.append((char*)&ObjectDescList[0], sizeof(ObjectDescList[0]) * ObjectDescList.size());
	}
}

WriteSerializer::WriteSerializer()
{
	ObjectOutput.reserve(1000);
	ObjectDescList.reserve(100);
	ObjectList.reserve(100);
}

void WriteSerializer::serialize( void* data, unsigned size )
{
	ObjectOutput.append((char*)data, size);
}

void WriteSerializer::serializeObject( Object*& object )
{
	unsigned index = ~0;
	vector<Object*>::iterator itor = std::find(ObjectList.begin(), ObjectList.end(), object);

	if(itor == ObjectList.end())
	{
		index = ObjectList.size();
		ObjectList.push_back(object);
	}
	else
	{
		index = itor - ObjectList.begin();
	}

	ObjectOutput.append((char*)&index, sizeof(index));
}

string Serializer::saveObject( Object* object )
{
	return WriteSerializer::saveObject(object);
}
