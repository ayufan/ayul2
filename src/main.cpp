#include "StdAfx.hpp"
#include <cmath>

bool ScriptObject::GenerateCppString = false;

extern FILE* yyin;
int yyparse(void* data);

class SimpleClass {
	DECLARE_CLASS(SimpleClass)

	int Variable;

	virtual ~SimpleClass() {
	}

	SimpleClass() : Variable(5) {
	}

	virtual Int32 Method() {
		return Variable;
	}

	virtual void SetMethod(Int32 value) {
		Variable = value;
	}
};

BEGIN_CLASS(SimpleClass, Object) {
	CLASS_VARIABLE(Variable)
	CLASS_VIRTUAL_METHOD(Method)
	CLASS_VIRTUAL_METHOD(SetMethod)
	CLASS_CONSTRUCTOR()
	CLASS_PROPERTY(Test, Method, SetMethod);
}

void MachineMain(ScriptModule& module);

int parseFileIntoModule(const string& fileName, ScriptModule& module) {
	// Open the file
	yyin = fopen(fileName.c_str(), "r");
	if(!yyin) {
		errorf("couldn't open %s\n", fileName.c_str());
		return -1;
	}

	try {
		// Copy data
		asCurrentLine.Line = 1;
		asCurrentLine.File = String::allocString(fileName);

		// Parse content
		intrusive_ptr<SyntaxContext> context;
		yyparse(&context);

		// Build tree
		if(context)
			context->build(module);
	}
	catch(std::exception& e) {
		errorf("%s\n", e.what());
		fclose(yyin);
		yyin = NULL;
		DebugBreak();
		return -1;
	}

	// Close file
	fclose(yyin);
	yyin = NULL;
	return 0;
}

int main(int argc, char* argv[]) {
	logf("ayul2 test runtime: %s\n", AYUL_VERSION());

	// Init main object
	Object::id$();

	if(argc != 2)
	{
		fprintf(stderr, "usage: %s [script]\n", argv[0]);
		return -1;
	}

	// Create module
	intrusive_ptr<ScriptModule> module(new ScriptModule("MyModule"));
	module->addObject<Core>();
	module->addObject<Script>();

#if 1
	// Parse file
	if(!parseFileIntoModule(argv[1], *module)) {
		//ScriptObject::GenerateCppString = true;
	}
	else {
		errorf("compile error!\n");
		return -1; 
	}

	//logf("%s\n", module->toFullString().c_str());
	module->finalize();
	//module->dump();
	//MachineMain(*module);

	intrusive_ptr<ScriptClassMethod> main$ = module->findObject<ScriptClassMethod>("Main");
	if(main$) 
	{
		FunctionType<void ()> mainFunction(main$->function());
		mainFunction.Function();
		//printf("a=%f\n", a.Value);
	}
	else
	{
		errorf("main not found");
	}
#endif
	// Close file
	//getchar();
	return 0;
}
