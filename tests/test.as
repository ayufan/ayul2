struct MyType 
{
	Float32 x = 1.0, y = 2.0, z = 3.0, w;
	Vec4 vv;

	MyType()
	{
		vv = new Vec4(1.0,2.0,3.0);
	}

	Float32 Sum()
	{
		return x + y + z + w;
	}
};


static float OtherRef(ref Vec4 v, ref Vec4 v2)
{
	return v ^ v2;
}

static void Main1() {
	MyType type = new MyType();
	MyOtherType type2 = new MyOtherType();

	type2.xxx += type.x += 2.0;

	Vec4 v1 = new Vec4(3.0, 2.0, 3.0);
	Vec4 v2 = new Vec4(2.0, 3.0, 4.0);

	type.vv.X = 10.0;

	WriteLine("Hello World!");
	WriteLineFloat(OtherRef(v1, v2));
	WriteLine(type.x.toString() + " " + type2.xxx.toString());
	WriteLine("dot: " + OtherRef(v1, v2).toString());
	WriteLine("type.vv.X: " + type.vv.X.toString());
	WriteLine("dot: " + (v1 ^ v2).toString());

	Main3();
}

static void SimpleFunc(int a, int b) {
	WriteLine(a.toString() + "   " + b.toString());
}

static float Main3() {

	Vec4 v1 = new Vec4(3.0, 2.0, 3.0);
	Vec4 v2 = new Vec4(2.0, 3.0, 4.0);

	WriteLine("Hello World!");
	WriteLine("dot: " + (v1 ^ v2).toString());

	//Main2();

	return 1.0;
}


delegate void MyDelegate(float v);

class MyOtherType
{
	Float32 xxx = 2.0;
	MyDelegate dupaJasia;
	void setXXX(float x) 
	{
		xxx = x;
	}
};

static void DelegateTest() {
	MyOtherType type2 = new MyOtherType();
	type2.dupaJasia = type2.setXXX;
	type2.dupaJasia(3.0);

	int i = 0;

	while(i < 10) {
		type2.dupaJasia(type2.xxx + 2.0);
		i = i + 1;
		WriteLineFloat(type2.xxx);
	}

	type2.dupaJasia = WriteLineFloat;
	type2.dupaJasia(type2.xxx);
}


static void SimpleDelegateTest() {
	MyOtherType type2 = new MyOtherType();
	type2.dupaJasia = type2.setXXX;
	type2.dupaJasia(3.0);
}

static void RTTI() {
	MyOtherType type2 = new MyOtherType();
	ScriptObject script;
	script = type2.id();
	WriteLine(script.ToString());
}

static void Main() {
	RTTI();

	//WriteLineFloat(1.0);

	//DelegateTest();

	//Main3();
	//Main1();
	//Main1();
}
